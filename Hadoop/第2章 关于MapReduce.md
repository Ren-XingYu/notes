MapReduce是一种可用于数据处理的编程模型。该模型比较简单，但要相写出有用的程序却不太容易。Hadoop可以允许各种语言版本的MapReduce程序。

## map和reduce ##
MapReduce任务过程分为两个处理阶段：map阶段和reduce阶段。每阶段都以键-值对作为输入和输出，其类型由程序员来选择。程序员还需要写两个函数：map函数和reduce函数  

MapReduce的逻辑数据流  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Hadoop/02/mapredue-process.jpg)

## Java MapReduce ##

Hadoop本身提供了一套可以优化网络序列化传输的基本类型，而不直接使用Java内嵌的类型。这些类型都在`org.apache.hadoop.io`包中。

	LongWritable-->Long
	Text-->String
	IntWritable-->Integer

map()和redue()函数的Context实例用于输出内容的写入

reduce函数的输入类型必须匹配map函数的输出类型

## 数据流 ##

单个reduce任务：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Hadoop/02/single-reduce.jpg)

如果有好多个reduce任务，每个map任务就会针对输出进行分区（partition），即为每个reduce任务建一个分区。  

多个reduce任务：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Hadoop/02/multi-reduce.jpg)

Hadoop允许用户针对map任务的输出指定一个combiner（就像mapper和reducer一样），combiner函数的输出作为reduce函数的输入。由于combiner属于优化方案，所以不管调用combiner多少次，reducer的输出结果都是一样的。

## Haddop Streaming ##

Hadoop提供了MapReduce的API，允许你使用非Java的其他语言来写自己的map和reduce函数。Hadoop Streaming使用Unix标准流作为Hadoop和应用程序之间的接口，所以我们可以使用任何编程语言通过标准输入/输出来写MapReduce程序。

Streaming天生适合用于文本处理。

