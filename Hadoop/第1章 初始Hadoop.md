# 1.1 数据！数据！ #

机器设备产生的数据可能远远超过我们个人所产生的数据

# 1.2 数据的存储与分析 #

Hadoop为我们提供了一个可靠的且可扩展的存储和分析平台

# 1.3 查询所有数据 #

MapReduce看似采用了一种蛮力方法。每个查询需要处理整个数据集或至少一个数据集的绝大部分。

# 1.4 不仅仅是批处理 #

从MapReduce的所有长处来看，它基本上是一个批处理系统，并不适合交互式分析。

Hadoop有时被用于指代一个更大的、多个项目组成的生态系统，而不仅仅是HDFS和MapReduce。

YARN（Yet Another Resource Negotiator）是一个集群资源管理系统，允许任何一个分布式程序（不仅仅是MapReduce）基于Hadoop集群的数据而运行。

能与Hadoop系统协同工作的处理模式。

	Interactive SQL（交互式SQL）
	Interative Processing（迭代处理）
	Stream processing（流处理）
	Search（搜索）

# 1.5 相较于其他系统的优势 #

## 1.5.1 关系型数据库管理系统 ##

在许多情况下，可以将MapReduce视为关系型数据库管理系统的补充。两个系统之间的差异如下图所示：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Hadoop/01/RDBMS-vs-MapReduce.jpg)

Hadoop对非结构化或半结构化数据非常有效，因为它是在处理数据时才对数据进行解释（即所谓的“读时模式”）。这种模式在提供灵活性的同时避免了RDBMS数据加载阶段带来的高开销，因为在Hadoop中仅仅是一个文件拷贝操作。

关系型数据往往是**规范的**（normalized），以保持其数据的完整性且不含冗余。

MapReduce以及Hadoop中其他的处理模型是可以随着数据规模线性伸缩的。

## 1.5.2 网格计算 ##

**高性能计算**(High performance Computing，HPC)和**网格计算**（Grid Computing）组织多年以来一致在研究大规模数据处理，主要使用类似于消息传递接口（Message Passing Interface，MPI）的API。从广义上来讲，高性能计算采用的方法是将作业任务分散到集群的各台机器上，这些机器访问存储区域网络（SAN）所组成的共享文件系统。这比较适用于计算密集型的作业，但如果节点需要访问的数据流更庞大，很多计算节点就会因为网络带宽的瓶颈问题而不得不闲下来等数据。

Hadoop尽量在计算节点上存储数据，以实现数据的本地快速访问。**数据本地化**（data locality）特性是Hadoop数据处理的核心，并因此而获得良好的性能。

## 1.5.3 志愿计算 ##

志愿计算项目将问题分成很多快，每一块称为一个**工作单元**（work unit），发到世界各地的计算机上进行分析。

# 1.6 Apache Hadoop发展简史 #

Hadoop起源于开源网络搜索引擎Apache Nutch，后者本身也是Lucene项目的一部分。

# 1.7 本书包含的内容 #

1~3部分讲解Hadoop核心  
4部分讲述Hadoop生态系统中的相关项目  
5部分包含Hadoop实例学习

