Google云计算技术包括：Google文件系统GFS、分布式计算编程模型MapReduce、分布式锁服务Chubby、分布式结构化数据表Bigtable、分布式存储系统Megastore、分布式监控系Dapper、海量数据的交互式分析工具Dremel，以及内存大数据分析系统PowerDrill等。

# 2.1 Google文件系统GFS #

GFS将容错的任务交给文件系统完成，利用软件的方法解决系统可靠性问题，使存储的成本成倍下降。

## 2.1.2 系统架构 ##

GFS系统架构：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Cloud%20Computing/02/gfs-architecture.jpg)

GFS将整个系统的节点分为三类角色：Client（客户端）、Master（主服务器）和Chunk Server（数据块服务器）。

GFS实现了控制流和数据流的分离。Client与Master之间只有控制流，而无数据流。

GFS的特点：  
	
	1、采用中心服务器模式
		Master管理分布式文件系统中的元数据
	2、不缓存数据
		对于存储在Master中的元数据，GFS采取了缓存策略。
	3、在用户态下实现
	4、只提供专用接口


## 2.1.2 容错机制 ##

1、Master容错  
Master保存了GFS文件系统的三种元数据。

	（1）、命名空间（Name Space），也就是整个文件系统的目录结构。（通过操作日志来提供容错）
	（2）、Chunk与文件名的映射表（通过操作日志来提供容错）
	（3）、Chunk副本的位置信息，每一个Chunk默认有三个副本。

2、Chunk Server容错  
GFS采用副本的方式实现Chunk Server的容错

## 2.1.3 系统管理技术 ##

1、大规模集群安装技术  
2、故障检测技术  
3、节点动态加入技术  
4、节能技术

# 2.2 分布式数据处理MapReduce #

MapReduce是一个软件架构，是一种处理海量数据的并行编程模型，用于大规模数据集的并行运算。Map（映射）、Reduce（化简）的概念和主要思想，都是从函数式编程语言和大量编程语言借鉴而来，这种编程模式特别适合于非结构化和结构化的海量数据的搜索、挖掘、分析与机器智能学习等。

## 2.2.1 产生背景 ##

MapReduce把对数据集的大规模操作，分发给一个主节点管理下的各分节点共同完成，通过这种方式实现任务的可靠执行与容错机制。在每个时间周期，主节点都会对分节点的工作状态进行标记。一旦分节点状态标记为死亡状态，则这个节点的所有任务都将分配给其他分节点重新执行。

## 2.2.2 编程模型 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Cloud%20Computing/02/mapreduce-model.jpg)






	