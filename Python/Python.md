# 第1章 快速上手：基础知识 #

Python自带图形用户界面应用程序`IDLD`

## 1.1 交互式程序 ##

Python每行末尾无须加分号。因为在Python中，一行就是一行。如果你愿意，也可以加上分号，但不会有任何影响。

在命令窗口可以输入`help()`命令获取帮助

## 1.2 算法是什么 ##

## 1.3 数和表达式 ##

除法（`/`）运算的结构为小数，即浮点数。如果想丢弃小数部分，即执行整除运算，可使用双斜杠(`//`)。整除运算是向下取整的。整数总是向下取整的。

Python提供了`__future__`模块，把下一个新版本的特性导入到当前版本，于是我们就可以在当前版本中测试一些新版本的特性。

求余运算也可以用于浮点数。例如`2.72%0.5=0.25`

`**`代表乘方运算

## 1.4 变量 ##

使用Python变量前必须给它赋值，因为Python变量没有默认值

## 1.5 语句 ##

表达式是一些东西，而语句做一些事情

解释器总是将表达式的值打印出来，打印的是`repr`表示的内容

## 1.6 获取用户输入 ##

	x=input('prompt')
	p=int(x)

使用`int(y)`可以将字符串转化为整数

## 1.7 函数 ##

## 1.8 模块 ##

模块是扩展，可通过导入它们来扩展Python的功能。例如，模块math包含多个很有用的函数。

导入模块的方法：

	import math
	math.floor(32.9)

	from math import sqrt
	sqrt(9)

	import cmath
	cmath.sqrt(-1)
	1j

复数处理模块`cmath`

Python没有专门表示虚数的类型，而将虚数视为实部为零的复数。

## 1.9 保存并执行程序 ##

强大的海归绘图法：

	from turtle import *


在有些情况下，你希望能够像执行其他程序一样执行Python脚本，而无需显式地使用Python解释器。UNIX提供了实现这种目标的标准方式：让脚本的第一行以字符序列`#!`（称为`pound bang`或`shebang`）开始，并在它后面指定用于对脚本进行解释的程序的绝对路径。

	#!/usr/bin/env python3

`#`号开头的为注释

## 1.10 字符串 ##

在大多数情况下，可通过使用长字符串和原始字符串来比卖你使用反斜杠进行转义

Python打印所有的字符串时，都用引号将其括起。这是因为Python打印值时，保留其在代码中的样子，而不是你希望用户看到的样子。如果你使用`print`，结果将不同。

使用str能以合理的方式将值转换为用户能够看懂的字符串。然而，使用repr时，通常会获得值的合法Python表达式表示。

	>>>print(repr("Hello,\nworld!"))
	'Hello,\nworld!'
	>>>print(str("Hello,\nworld!"))
	Hello,
	world!	

有一种独特的语法可用于表示包含换行符或反斜杠的字符串（长字符串和原始字符串）。

在Python3中，所有的字符串都是`Unicode`字符串。

长字符串：表示很长的字符串（跨越多行的字符串）

	'''xxx'''
	"""xxx"""

原始字符串：原始字符串不以特殊方式处理反斜杠。

	>>>print(r'C:\nowhere')
	C:\nowhere

原始字符串用前缀`r`表示。看起来可在原始字符串中包含任何字符，这大致是正确的。

	>>>print(r'Let\'s go!')
	Let\'s go!

原始字符串不能以单个反斜杠结尾。换而言之，原始字符串的最后一个字符不能是反斜杠，除非你对其进行转义（但进行转义时，用于转义的反斜杠也将是字符串的一部分）

	>>>print(r"This is illegal\")
	SyntaxError: EOL while scanning string literal
	>>>print(r'C:\Program Files\foo\bar' '\\')
	C:\Program Files\foo\bar\

每个Unicode字符都用一个码点（code point）表示，而码点是Unicode标准给每个字符指定的数字。这让你能够以任何现代软件都能识别的方式表示129个文字系统中的12万个以上的字符。当然，鉴于计算机键盘不可能包含几十万个键，因此有一种指定Unicode字符的通用机制：使用16或32位的十六进制字面量（分别加上前缀`\u`或`\U`）或者使用字符的Unicode名称（`\N{name}`）。

	>>>"\u00C6"
	'Æ'
	>>>"\U0001F60A"
	'☺'
	>>>"This is a cat: \N{Cat}"
	'This is a cat: (cat图像)'

在诸如C等编程语言中，这些字节完全暴露，而字符串不过是字节序列而已。为与C语言互操作以及将文本写入文件或通过网络套接字发送出去，Python提供了两种类似的类型：不可变的`bytes`和可变的`bytearray`。如果需要，可直接创建bytes对象（而不是字符串），方法是使用前缀`b`：

	>>>b'Hello, world!'
	b'Hello, world!'

encode函数第一个参数是编码，第二个参数是告诉它如何处理错误，这个参数默认为`strict`

	>>> "Hello, world!".encode("ASCII")
	b'Hello, world!'
	>>> "Hello, world!".encode("UTF-8")
	b'Hello, world!'

可不使用方法encode和decode，而直接创建`bytes`和`str`（即字符串）对象

	>>> bytes("Hællå, wørld!", encoding="utf-8")
	b'H\xc3\xa6ll\xc3\xa5, w\xc3\xb8rld!'
	>>> str(b'H\xc3\xa6ll\xc3\xa5, w\xc3\xb8rld!', encoding="utf-8")
	'Hællå, wørld!'	

源代码也将被编码，且默认使用的也是UTF-8编码。如果你想使用其他编码（例如，如果你使用的文本编辑器使用其他编码来存储源代码），可使用特殊的注释来指定。

	# -*- coding: encoding name -*-

Python还提供了`bytearray`，它是`bytes`的可变版。从某种意义上说，它就像是可修改的字符串——常规字符串是不能修改的。然而，bytearray其实是为在幕后使用而设计的，因此作为类字符串使用时对用户并不友好。例如，要替换其中的字符，必须将其指定为0～255的值。因此，要插入字符，必须使用`ord`获取其**序数值**（ordinal value）。

	>>> x = bytearray(b"Hello!")
	>>> x[1] = ord(b"u")
	>>> x
	bytearray(b'Hullo!')


# 第2章 列表和元组 #

**数据结构**是以某种方式（如通过编号）组合起来的数据元素（如数、字符乃至其他数据结构）集合。在Python中，最基本的数据结构为**序列**（sequence）。序列中的每个元素都有编号，即其位置或索引，从0开始。

## 2.1 序列概述 ##

Python内置了多种序列，包括：**列表**、**元组**、**字符串**等

列表和元组的主要不同在于，列表是可以修改的，而元组不可以。内置函数通常返回元组。

列表：所有元素都放在方括号内，并用逗号隔开

	>>> edward = ['Edward Gumby', 42]

**容器**基本上就是可包含其他对象的对象。两种主要的容器是**序列**（如列表和元组）和**映射**（如字典）。在序列中，每个元素都有编号，而在映射中，每个元素都有名称（也叫键）。有一种既不是序列也不是映射的容器，它就是**集合**（set）。

## 2.2 通用的序列操作 ##

有几种操作适用于所有序列，包括**索引**、**切片**、**相加**、**相乘**和**成员资格**检查。

### 2.2.1 索引 ###

序列中的所有元素都有编号——从0开始递增。

	>>> greeting = 'Hello'
	>>> greeting[0]
	'H'

字符串就是由字符组成的序列。。不同于其他一些语言，Python没有专门用于表示字符的类型，因此一个字符就是只包含一个元素的字符串。


### 2.2.2 切片 ###

除使用索引来访问单个元素外，还可使用切片（slicing）来访问**特定范围**内的元素。为此，可使用两个索引，并用冒号分隔。

	>>> tag = '<a href="http://www.python.org">Python web site</a>'
	>>> tag[9:30]
	'http://www.python.org'
	>>> tag[32:-4]
	'Python web site'

第一个索引指定的元素包含在切片内，但第二个索引指定的元素不包含在切片内。（包含头不包含尾）

	>>> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	>>> numbers[7:10]
	[8, 9, 10]
	>>> numbers[-3:-1]
	[8, 9]
	>>> numbers[-3:]
	[8, 9, 10]
	>>> numbers[:3]
	[1, 2, 3]
	>>> numbers[:]     # 复制整个序列，可将两个索引都省略
	[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

	>>> numbers[0:10:1]     # 步长为1
	[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	>>> numbers[0:10:2]     # 步长为2
	[1, 3, 5, 7, 9]
	>>> numbers[::4]
	[1, 5, 9]	
	
	# 当然，步长不能为0，否则无法向前移动，但可以为负数，即从右向左提取元素。
	>>> numbers[8:3:-1]
	[9, 8, 7, 6, 5]
	>>> numbers[10:0:-2]
	[10, 8, 6, 4, 2]
	>>> numbers[0:10:-2]
	[]
	>>> numbers[::-2]
	[10, 8, 6, 4, 2]	
	>>> numbers[5::-2]
	[6, 4, 2]
	>>> numbers[:5:-2]
	[10, 8]

### 2.2.3 序列相加 ###

可使用加法运算符来拼接序列。一般而言，不能拼接不同类型的序列。

	>>> [1, 2, 3] + [4, 5, 6]
	[1, 2, 3, 4, 5, 6]
	>>> 'Hello,' + 'world!'
	'Hello, world!'
	>>> [1, 2, 3] + 'world!'
	Traceback (innermost last):
	File "<pyshell>", line 1, in ?
	[1, 2, 3] + 'world!'
	TypeError: can only concatenate list (not "string") to list

### 2.2.4 乘法 ###

将序列与数x相乘时，将重复这个序列x次来创建一个新序列：

	>>> 'python' * 5
	'pythonpythonpythonpythonpython'
	>>> [42] * 10
	[42, 42, 42, 42, 42, 42, 42, 42, 42, 42]

空列表是使用不包含任何内容的两个方括号（`[]`）表示的，在Python中，None表示什么都没有。因此，要将列表的长度初始化为10，可像下面这样做：

	>>> sequence = [None] * 10
	>>> sequence
	[None, None, None, None, None, None, None, None, None, None]


### 2.2.5 成员资格 ###

要检查特定的值是否包含在序列中，可使用运算符in。它检查是否满足指定的条件，并返回布尔值。

	>>> permissions = 'rw'
	>>> 'w' in permissions
	True
	>>> 'x' in permissions
	False

## 2.3 列表：Python 的主力 ##

### 2.3.1 函数list ###

鉴于不能像修改列表那样修改字符串，因此在有些情况下使用字符串来创建列表很有帮助。

	>>> list('Hello')
	['H', 'e', 'l', 'l', 'o']
	# 可将任何序列（而不仅仅是字符串）作为list的参数。

要将字符列表（如前述代码中的字符列表）转换为字符串，可使用下面的表达式：

	''.join(somelist)

### 2.3.2 基本的列表操作 ###

注意，并非所有列表方法都会修改列表。

	修改列表：给元素赋值
	>>> x = [1, 1, 1]
	>>> x[1] = 2
	>>> x
	[1, 2, 1]

	删除元素
	>>> names = ['Alice', 'Beth', 'Cecil', 'Dee-Dee', 'Earl']
	>>> del names[2]     #除用于删除列表元素外，del语句还可用于删除其他东西。你可将其用于字典乃至变量
	>>> names
	['Alice', 'Beth', 'Dee-Dee', 'Earl']

	给切片赋值
	>>> name = list('Perl')
	>>> name
	['P', 'e', 'r', 'l']
	>>> name[2:] = list('ar')
	>>> name
	['P', 'e', 'a', 'r']
	# 通过使用切片赋值，可将切片替换为长度与其不同的序列。
	>>> name = list('Perl')
	>>> name[1:] = list('ython')
	>>> name
	['P', 'y', 't', 'h', 'o', 'n']

	>>> numbers = [1, 5]
	>>> numbers[1:1] = [2, 3, 4]
	>>> numbers
	[1, 2, 3, 4, 5]
	>>> numbers
	[1, 2, 3, 4, 5]
	>>> numbers[1:4] = []
	>>> numbers
	[1, 5]

### 2.3.3 列表方法 ###

方法是与对象（列表、数、字符串等）联系紧密的函数。通常，像下面这样调用方法：
	
	object.method(arguments)

方法调用与函数调用很像，只是在方法名前加上了对象和句点。

	append
	>>> lst = [1, 2, 3]
	>>> lst.append(4)
	>>> lst
	[1, 2, 3, 4]

	clear
	>>> lst = [1, 2, 3]
	>>> lst.clear()
	>>> lst
	[]
	#相当于：	lst[:] = []

	copy
	>>> a = [1, 2, 3]
	>>> b = a
	>>> b[1] = 4
	>>> a
	[1, 4, 3]
	>>> a = [1, 2, 3]
	>>> b = a.copy()
	>>> b[1] = 4
	>>> a
	[1, 2, 3]
	# 类似于使用a[:]或list(a)，它们也都复制a

	count
	>>> ['to', 'be', 'or', 'not', 'to', 'be'].count('to')
	2
	>>> x = [[1, 2], 1, 1, [2, 1, [1, 2]]]
	>>> x.count(1)
	2
	>>> x.count([1, 2])
	1
	
	extend
	>>> a = [1, 2, 3]
	>>> b = [4, 5, 6]
	>>> a.extend(b)
	>>> a
	[1, 2, 3, 4, 5, 6]
	>>> a = [1, 2, 3]
	>>> b = [4, 5, 6]
	>>> a + b
	[1, 2, 3, 4, 5, 6]     # 使用副本，创建一个新列表
	>>> a
	[1, 2, 3]	

	index
	>>> knights = ['We', 'are', 'the', 'knights', 'who', 'say', 'ni']
	>>> knights.index('who')
	4
		
	insert
	>>> numbers = [1, 2, 3, 5, 6, 7]
	>>> numbers.insert(3, 'four')
	>>> numbers
	[1, 2, 3, 'four', 5, 6, 7]

	pop：pop是唯一既修改列表又返回一个非None值的列表方法。
	>>> x = [1, 2, 3]
	>>> x.pop()
	3
	>>> x
	[1, 2]

	push和pop是大家普遍接受的两种栈操作（加入和取走）的名称。Python没有提供push，但可使用append来替代。
	>>> x = [1, 2, 3]
	>>> x.append(x.pop())
	>>> x
	[1, 2, 3]
	要创建先进先出（FIFO）的队列，可使用insert(0, ...)代替append。另外，也可继续使用append，但用pop(0)替代pop()。

	remove
	>>> x = ['to', 'be', 'or', 'not', 'to', 'be']
	>>> x.remove('be')
	>>> x
	['to', 'or', 'not', 'to', 'be']

	reverse：修改列表，但不返回任何值
	>>> x = [1, 2, 3]
	>>> x.reverse()
	>>> x
	[3, 2, 1]
	如果要按相反的顺序迭代序列，可使用函数reversed。这个函数不返回列表，而是返回一个迭代器。你可使用list将返回的对象转换为列表。
	>>> x = [1, 2, 3]
	>>> list(reversed(x))
	[3, 2, 1]

	sort：对列表就地排序。就地排序意味着对原来的列表进行修改，使其元素按顺序排列，而不是返回排序后的列表的副本。
	>>> x = [4, 6, 2, 1, 7, 9]
	>>> x.sort()
	>>> x
	[1, 2, 4, 6, 7, 9]
	为获取排序后的列表的副本，另一种方式是使用函数sorted。
	>>> x = [4, 6, 2, 1, 7, 9]
	>>> y = sorted(x)
	>>> x
	[4, 6, 2, 1, 7, 9]
	>>> y
	[1, 2, 4, 6, 7, 9]
	
	高级排序
	方法sort接受两个可选参数：key和reverse。这两个参数通常是按名称指定的，称为关键字参数。
	。参数key类似于参数cmp：你将其设置为一个用于排序的函数。然而，不会直接使用这个函数来判断一个元素是否比另一个元素小，	
	而是使用它来为每个元素创建一个键，再根据这些键对元素进行排序。因此，要根据长度对元素进行排序，可将参数key设置为函数len。	
	对于另一个关键字参数reverse，只需将其指定为一个布尔值，以指出是否要按相反的顺序对列表进行排序。
	>>> x = ['aardvark', 'abalone', 'acme', 'add', 'aerate']
	>>> x.sort(key=len)
	>>> x
	['add', 'acme', 'aerate', 'abalone', 'aardvark']
	>>> x = [4, 6, 2, 1, 7, 9]
	>>> x.sort(reverse=True)
	>>> x
	[9, 7, 6, 4, 2, 1]
	函数sorted也接受参数key和reverse。

## 2.4 元组：不可修改的序列 ##

元组语法很简单，只要将一些值用逗号分隔，就能自动创建一个元组。

	>>> 1, 2, 3
	(1, 2, 3)

元组还可用圆括号括起（这也是通常采用的做法）。
	
	>>> (1, 2, 3)
	(1, 2, 3)
	>>> () # 空元组
	()
	表示一个值的元组：虽然只有一个值，也必须在它后面加上逗号。
	>>> 42  
	42     #不是元组
	>>> 42,  
	(42,)     #是元组
	>>> (42,)
	(42,)
	逗号至关重要，仅将值用圆括号括起不管用：(42)与42完全等效。但仅仅加上一个逗号，就能完全改变表达式的值。
	>>> 3 * (40 + 2)
	126
	>>> 3 * (40 + 2,)
	(42, 42, 42)
	函数tuple的工作原理与list很像：它将一个序列作为参数，并将其转换为元组。如果参数已经是元组，就原封不动地返回它。
	>>> tuple([1, 2, 3])
	(1, 2, 3)
	>>> tuple('abc')
	('a', 'b', 'c')
	>>> tuple((1, 2, 3))
	(1, 2, 3)
	元组没有的index和count方法
	
# 第3章 使用字符串 #

## 3.1 字符串基本操作 ##

字符串是不可变的，因此所有的元素赋值和切片赋值都是非法的。

## 3.2 设置字符串的格式：精简版 ##

	>>> format = "Hello, %s. %s enough for ya?"
	>>> values = ('world', 'Hot')
	>>> format % values
	'Hello, world. Hot enough for ya?' # 在%左边指定一个字符串（格式字符串），并在右边指定要设置其格式的值。
	指定要设置其格式的值时，可使用单个值（如字符串或数字），可使用元组（如果要设置多个值的格式），还可使用字典，其中最常见的是元组。

	字符串模板方式
	>>> from string import Template
	>>> tmpl = Template("Hello, $who! $what enough for ya?")
	>>> tmpl.substitute(who="Mars", what="Dusty")
	'Hello, Mars! Dusty enough for ya?'

	使用字符串方法format
	>>> "{}, {} and {}".format("first", "second", "third")
	'first, second and third'
	>>> "{0}, {1} and {2}".format("first", "second", "third")
	'first, second and third'
	>>> "{3} {0} {2} {1} {3} {0}".format("be", "not", "or", "to")
	'to be or not to be
	>>> from math import pi
	>>> "{name} is approximately {value:.2f}.".format(value=pi, name="π")
	'π is approximately 3.14.'

	如果变量与替换字段同名，还可使用一种简写。在这种情况下，可使用f字符串——在字符串前面加上f。
	>>> from math import e
	>>> f"Euler's constant is roughly {e}."
	"Euler's constant is roughly 2.718281828459045."
	等价于
	>>> "Euler's constant is roughly {e}.".format(e=e)
	"Euler's constant is roughly 2.718281828459045."


## 3.3 设置字符串的格式：完整版 ##

字段名：索引或标识符，指出要设置哪个值的格式并使用结果来替换该字段。  
转换标志：跟在叹号后面的单个字符。当前支持的字符包括r（表示repr）、s（表示str）和a（表示ascii）。
格式说明符：跟在冒号后面的表达式（这种表达式是使用微型格式指定语言表示的）。

### 3.3.1 替换字段名 ###

	>>> "{foo} {} {bar} {}".format(1, 2, bar=4, foo=3)
	'3 1 4 2'
	>>> "{foo} {1} {bar} {0}".format(1, 2, bar=4, foo=3)
	'3 2 4 1'
	>>> fullname = ["Alfred", "Smoketoomuch"]
	>>> "Mr {name[1]}".format(name=fullname)
	'Mr Smoketoomuch'
	>>> import math
	>>> tmpl = "The {mod.__name__} module defines the value {mod.pi} for π"
	>>> tmpl.format(mod=math)
	'The math module defines the value 3.141592653589793 for π'	

### 3.3.2 基本转换 ###

	>>> print("{pi!s} {pi!r} {pi!a}".format(pi="π"))
	π 'π' '\u03c0'
	>>> "The number is {num}".format(num=42)
	'The number is 42'
	>>> "The number is {num:f}".format(num=42)
	'The number is 42.000000'
	>>> "The number is {num:b}".format(num=42)
	'The number is 101010'
	
	字符串格式设置中的类型说明符
	b 将整数表示为二进制数
	c 将整数解读为Unicode码点
	d 将整数视为十进制数进行处理，这是整数默认使用的说明符
	e 使用科学表示法来表示小数（用e来表示指数）
	E 与e相同，但使用E来表示指数
	f 将小数表示为定点数
	F 与f相同，但对于特殊值（nan和inf），使用大写表示
	g 自动在定点表示法和科学表示法之间做出选择。这是默认用于小数的说明符，但在默认情况下至少有1位小数
	G 与g相同，但使用大写来表示指数和特殊值
	n 与g相同，但插入随区域而异的数字分隔符
	o 将整数表示为八进制数
	s 保持字符串的格式不变，这是默认用于字符串的说明符
	x 将整数表示为十六进制数并使用小写字母
	X 与x相同，但使用大写字母
	% 将数表示为百分比值（乘以100，按说明符f设置格式，再在后面加上%）	

### 3.3.3 宽度、精度和千位分隔符 ###	

设置浮点数（或其他更具体的小数类型）的格式时，默认在小数点后面显示`6`位小数

	>>> "{num:10}".format(num=3)
	' 3'
	>>> "{name:10}".format(name="Bob")
	'Bob '
	>>> "Pi day is {pi:.2f}".format(pi=pi)
	'Pi day is 3.14'
	>>> "{pi:10.2f}".format(pi=pi)
	' 3.14'
	对于其他类型也可指定精度，但是这样做的情形不太常见
	>>> "{:.5}".format("Guido van Rossum")
	'Guido'	
	最后，可使用逗号来指出你要添加千位分隔符。
	>>> 'One googol is {:,}'.format(10**100)
	'One googol is 10,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,00
	0,000,000,000,000,000,000,000,000,000,000,000,000,000,000'
	同时指定其他格式设置元素时，这个逗号应放在宽度和表示精度的句点之间

### 3.3.4 符号、对齐和用0 填充 ###

	>>> '{:010.2f}'.format(pi)
	'0000003.14'
	>>> print('{0:<10.2f}\n{0:^10.2f}\n{0:>10.2f}'.format(pi))
	3.14
		3.14
			3.14
	>>> print('{0:10.2f}\n{1:10.2f}'.format(pi, -pi))
		3.14
		-3.14
	>>> print('{0:10.2f}\n{1:=10.2f}'.format(pi, -pi))
		3.14
	-   3.14
	>>> print('{0:-.2}\n{1:-.2}'.format(pi, -pi)) #默认设置
	3.1
	-3.1
	>>> print('{0:+.2}\n{1:+.2}'.format(pi, -pi))
	+3.1
	-3.1
	>>> print('{0: .2}\n{1: .2}'.format(pi, -pi))
	 3.1
	-3.1
	>>> "{:b}".format(42)
	'101010'
	>>> "{:#b}".format(42)
	'0b101010'
	>>> "{:g}".format(42)
	'42'
	>>> "{:#g}".format(42)
	'42.0000'

## 3.4 字符串方法 ##

	center:通过在两边添加填充字符（默认为空格）让字符串居中
	>>> "The Middle by Jimmy Eat World".center(39)
	' The Middle by Jimmy Eat World '
	>>> "The Middle by Jimmy Eat World".center(39, "*")
	'*****The Middle by Jimmy Eat World*****'
	
	find:在字符串中查找子串。如果找到，就返回子串的第一个字符的索引，否则返回-1。
	>>> 'With a moo-moo here, and a moo-moo there'.find('moo')
	7
	还可指定搜索的起点和终点
	>>> subject = '$$$ Get rich now!!! $$$'
	>>> subject.find('$$$')
	0
	>>> subject.find('$$$', 1) # 只指定了起点
	20
	
	join：其作用与split相反，用于合并序列的元素
	>>> seq = [1, 2, 3, 4, 5]
	>>> sep = '+'
	>>> sep.join(seq) # 尝试合并一个数字列表
	Traceback (most recent call last):
	File "<stdin>", line 1, in ?
	TypeError: sequence item 0: expected string, int found
	>>> seq = ['1', '2', '3', '4', '5']
	>>> sep.join(seq) # 合并一个字符串列表
	'1+2+3+4+5'
	>>> dirs = '', 'usr', 'bin', 'env'
	>>> '/'.join(dirs)
	'/usr/bin/env'
	>>> print('C:' + '\\'.join(dirs))
	C:\usr\bin\env
	
	lower：返回字符串的小写版本   # 词首大写：title
	>>> 'Trondheim Hammer Dance'.lower()
	'trondheim hammer dance'
	
	replace：将指定子串都替换为另一个字符串
	>>> 'This is a test'.replace('is', 'eez')
	'Theez eez a test'
	
	split：其作用与join相反，用于将字符串拆分为序列。
	>>> '1+2+3+4+5'.split('+')
	['1', '2', '3', '4', '5']
	>>> '/usr/bin/env'.split('/')
	['', 'usr', 'bin', 'env']
	>>> 'Using the default'.split()
	['Using', 'the', 'default']
	
	strip：将字符串开头和末尾的空白（但不包括中间的空白）删除，并返回删除后的结果。
	>>> ' internal whitespace is kept '.strip()
	'internal whitespace is kept'
	>>> '*** SPAM * for * everyone!!! ***'.strip(' *!')  # 指定要就删除哪些字符
	'SPAM * for * everyone'
	
	translate
	方法translate与replace一样替换字符串的特定部分，但不同的是它只能进行单字符替换。
	这个方法的优势在于能够同时替换多个字符，因此效率比replace高。  
	假设你要将一段英语文本转换为带有德国口音的版本，为此必须将字符c和s分别替换为k和z。  
	然而，使用translate前必须创建一个转换表。这个转换表指出了不同Unicode码点之间的转换关系。
	要创建转换表，可对字符串类型str调用方法maketrans，这个方法接受两个参数：两个长度相同的字符串，
	它们指定要将第一个字符串中的每个字符都替换为第二个字符串中的相应字符。  
	调用方法maketrans时，还可提供可选的第三个参数，指定要将哪些字母删除。
	例如，要模仿语速极快的德国口音，可将所有的空格都删除。
	>>> table = str.maketrans('cs', 'kz')	
	>>> table
	{115: 122, 99: 107}
	>>> 'this is an incredible test'.translate(table)
	'thiz iz an inkredible tezt'
	>>> table = str.maketrans('cs', 'kz', ' ')
	>>> 'this is an incredible test'.translate(table)
	'thizizaninkredibletezt'

# 第4章 当索引行不通时 #

字典的键可能是数、字符串或元组。

## 4.1 字典的用途 ##

## 4.2 创建和使用字典 ##

	phonebook = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258'}
	
空字典（没有任何项）用两个花括号表示，类似于下面这样：{}。
  
在字典（以及其他映射类型）中，键必须是独一无二的，而字典中的值无需如此。

### 4.2.1 函数dict ###

	可使用函数dict①从其他映射（如其他字典）或键值对序列创建字典。
	>>> items = [('name', 'Gumby'), ('age', 42)]
	>>> d = dict(items)
	>>> d
	{'age': 42, 'name': 'Gumby'}
	>>> d['name']
	'Gumby'
	还可使用关键字实参来调用这个函数，如下所示：
	>>> d = dict(name='Gumby', age=42)
	>>> d
	{'age': 42, 'name': 'Gumby'}

### 4.2.2 基本的字典操作 ###

**键的类型**：字典中的键可以是任何不可变的类型，如整数、浮点数（实数）、字符串或元组。

**自动添加**：即便是字典中原本没有的键，也可以给它赋值，这将在字典中创建一个新项。

**成员资格**：表达式`k in d`（其中d是一个字典）查找的是键而不是值，而表达式`v in l`（其中l是一个列表）查找的是值而不是索引。

	>>> x = {}
	>>> x[42] = 'Foobar'
	>>> x
	{42: 'Foobar'}

### 4.2.3 将字符串格式设置功能用于字典 ###

必须使用`format_map`来指出你将通过一个映射来提供所需的信息。

	>>> phonebook
	{'Beth': '9102', 'Alice': '2341', 'Cecil': '3258'}
	>>> "Cecil's phone number is {Cecil}.".format_map(phonebook)
	"Cecil's phone number is 3258."

### 4.2.4 字典方法 ###

	clear：方法clear删除所有的字典项（就地执行），什么都不返回（或者说返回None）。
	>>> d = {}
	>>> d['name'] = 'Gumby'
	>>> d['age'] = 42
	>>> d
	{'age': 42, 'name': 'Gumby'}
	>>> returned_value = d.clear()
	>>> d
	{}
	>>> print(returned_value)
	None

	copy：返回一个新字典，其包含的键值对与原来的字典相同（这个方法执行的是浅复制，因为值本身是原件，而非副本）。
	>>> x = {'username': 'admin', 'machines': ['foo', 'bar', 'baz']}
	>>> y = x.copy()
	>>> y['username'] = 'mlh'
	>>> y['machines'].remove('bar')
	>>> y
	{'username': 'mlh', 'machines': ['foo', 'baz']}
	>>> x
	{'username': 'admin', 'machines': ['foo', 'baz']}
	为避免这种问题，一种办法是执行深复制，即同时复制值及其包含的所有值，等等。为此，可使用模块copy中的函数deepcopy。
	>>> from copy import deepcopy
	>>> d = {}
	>>> d['names'] = ['Alfred', 'Bertrand']
	>>> c = d.copy()
	>>> dc = deepcopy(d)
	>>> d['names'].append('Clive')
	>>> c
	{'names': ['Alfred', 'Bertrand', 'Clive']}
	>>> dc
	{'names': ['Alfred', 'Bertrand']}

	fromkeys：创建一个新字典，其中包含指定的键，且每个键对应的值都是None。
	>>> {}.fromkeys(['name', 'age'])
	{'age': None, 'name': None}
	>>> dict.fromkeys(['name', 'age'])     # 直接对dict调用fromkeys（dict是所有字典所属的类型）
	{'age': None, 'name': None}		
	如果你不想使用默认值None，可提供特定的值。
	>>> dict.fromkeys(['name', 'age'], '(unknown)')
	{'age': '(unknown)', 'name': '(unknown)'}	

	get：为访问字典项提供了宽松的环境。通常，如果你试图访问字典中没有的项，将引发错误。
	使用get来访问不存在的键时，没有引发异常，而是返回None。你可指定“默认”值，这样将返回你指定的值而不是None。
	>>> d = {}
	>>> print(d['name'])
	Traceback (most recent call last):
	File "<stdin>", line 1, in ?
	KeyError: 'name'
	>>> print(d.get('name'))
	None
	>>> d.get('name', 'N/A')
	'N/A'

	items：返回一个包含所有字典项的列表，其中每个元素都为(key, value)的形式。字典项在列表中的排列顺序不确定。
	>>> d = {'title': 'Python Web Site', 'url': 'http://www.python.org', 'spam': 0}
	>>> d.items()
	dict_items([('url', 'http://www.python.org'), ('spam', 0), ('title', 'Python Web Site')])
	返回值属于一种名为字典视图的特殊类型。字典视图可用于迭代。另外，你还可确定其长度以及对其执行成员资格检查。视图的一个优点是不复制

	keys：返回一个字典视图，其中包含指定字典中的键。

	pop：可用于获取与指定键相关联的值，并将该键值对从字典中删除。
	>>> d = {'x': 1, 'y': 2}
	>>> d.pop('x')
	1
	>>> d
	{'y': 2}

	popitem：随机地弹出一个字典项
	>>> d = {'url': 'http://www.python.org', 'spam': 0, 'title': 'Python Web Site'}
	>>> d.popitem()
	('url', 'http://www.python.org')
	>>> d
	{'spam': 0, 'title': 'Python Web Site'}	

	setdefault：还在字典不包含指定的键时，在字典中添加指定的键值对
	>>> d = {}
	>>> d.setdefault('name', 'N/A')
	'N/A'
	>>> d
	{'name': 'N/A'}
	>>> d['name'] = 'Gumby'
	>>> d.setdefault('name', 'N/A')
	'Gumby'
	>>> d
	{'name': 'Gumby'}

	update：使用一个字典中的项来更新另一个字典。
	>>> d = {
	... 'title': 'Python Web Site',
	... 'url': 'http://www.python.org',
	... 'changed': 'Mar 14 22:09:15 MET 2016'
	... }
	>>> x = {'title': 'Python Language Website'}
	>>> d.update(x)
	>>> d
	{'url': 'http://www.python.org', 'changed':
	'Mar 14 22:09:15 MET 2016', 'title': 'Python Language Website'}
	
	values：返回一个由字典中的值组成的字典视图。不同于方法keys，方法values返回的视图可能包含重复的值。
	>>> d = {}
	>>> d[1] = 1
	>>> d[2] = 2
	>>> d[3] = 3
	>>> d[4] = 1
	>>> d.values()
	dict_values([1, 2, 3, 1])

# 第5章 条件、循环以及其他语句 #

## 5.1 再谈print 和 import ##

print可同时打印多个表达式，条件是用逗号分隔它们。

	>>> print('Age:', 42)
	Age: 42	
	自定义分隔符
	>>> print("I", "wish", "to", "register", "a", "complaint", sep="_")
	I_wish_to_register_a_complain
	你还可自定义结束字符串，以替换默认的换行符
	print('Hello,', end='')
	print('world!')
	Hello, world!

	>>> import math as foobar
	>>> foobar.sqrt(4)
	2.0
	>>> from math import sqrt as foobar
	>>> foobar(4)
	2.0

## 5.2 赋值魔法 ##

### 5.2.1 序列解包 ###
	
	>>> x, y, z = 1, 2, 3
	>>> print(x, y, z)
	1 2 3
	>>> x, y = y, x
	>>> print(x, y, z)
	2 1 3
	实际上，这里执行的操作称为序列解包（或可迭代对象解包）：将一个序列（或任何可迭代
	对象）解包，并将得到的值存储到一系列变量中。
	>>> values = 1, 2, 3
	>>> values
	(1, 2, 3)
	>>> x, y, z = values
	>>> x
	1
	要解包的序列包含的元素个数必须与你在等号左边列出的目标个数相同，否则Python将引发异常。
	>>> x, y, z = 1, 2
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	ValueError: need more than 2 values to unpack
	>>> x, y, z = 1, 2, 3, 4
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	ValueError: too many values to unpack
	可使用星号运算符（*）来收集多余的值，这样无需确保值和变量的个数相同，如下例所示：
	>>> a, b, *rest = [1, 2, 3, 4]
	>>> rest
	[3, 4]
	还可将带星号的变量放在其他位置。
	>>> name = "Albus Percival Wulfric Brian Dumbledore"
	>>> first, *middle, last = name.split()
	>>> middle
	['Percival', 'Wulfric', 'Brian']
	赋值语句的右边可以是任何类型的序列，但带星号的变量最终包含的总是一个列表。在变量
	和值的个数相同时亦如此。
	>>> a, *b, c = "abc"
	>>> a, b, c
	('a', ['b'], 'c')


### 5.2.2 链式赋值 ###

	x = y = somefunction()
	上述代码与下面的代码等价：
	y = somefunction()
	x = y
	请注意，这两条语句可能与下面的语句不等价：
	x = somefunction()
	y = somefunction()	

### 5.2.3 增强赋值 ###
	
	>>> x = 2
	>>> x += 1
	>>> x *= 2
	>>> x
	6

## 5.3 代码块：缩进的乐趣 ##

代码块是一组语句，代码块是通过缩进代码（即在前面加空格）来创建的。

在Python中，使用冒号（`:`）指出接下来是一个代码块，并将该代码块中的每行代码都缩进相同的程度。发现缩进量与之前相同时，你就知道当前代码块到此结束了。

## 5.4 条件和条件语句 ##

### 5.4.1 这正式布尔值的用武之地 ###

	False None 0 "" () [] {} 都被看作为假
	
	True（1）为真，False（0）为假

### 5.4.2 有条件地执行和if语句 ###

	if condition:
		code

	条件表达式--三目运算符的Python版本
	
	status= "friends" if name.enddWith('Gumby') else "stranger"

### 5.4.3 else子句 ###

	if condition:
		code
	else:
		code

### 5.4.4 elif子句 ###

	if condition:
		code
	elif condition:
		code
	else:
		code

### 5.4.5 代码块嵌套 ###

#### 5.4.6 更复杂的条件 ####

	比较运算符
	x == y 		x 等于y
	x < y 		x小于y
	x > y 		x大于y
	x >= y 		x大于或等于y
	x <= y 		x小于或等于y
	x != y 		x不等于y
	x is y 		x和y是同一个对象
	x is not y 	x和y是不同的对象
	x in y 		x是容器（如序列）y的成员
	x not in y 	x不是容器（如序列）y的成员
	与赋值一样，Python也支持链式比较：可同时使用多个比较运算符，如0 < age < 100。
	==用来检查两个对象是否相等，而is用来检查两个对象是否相同（是同一个对象）。

	布尔运算符：and or not  （短路逻辑）

### 5.4.7 断言 ###

	>>> age = -1
	>>> assert 0 < age < 100, 'The age must be realistic'   # 可在条件后面添加一个字符串，对断言做出说明。
	Traceback (most recent call last):
	File "<stdin>", line 1, in ?
	AssertionError: The age must be realistic

## 5.5 循环 ##

### 5.5.1 while循环 ###

	while condition:
		code

### 5.5.2 for循环 ###

	words = ['this', 'is', 'an', 'ex', 'parrot']
	for word in words:
		print(word)

	numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
	for number in numbers:
		print(number)

	for number in range(1,101):
		print(number)

### 5.5.3 迭代字典 ###

	d = {'x': 1, 'y': 2, 'z': 3}
	for key in d:
		print(key, 'corresponds to', d[key])

	for key, value in d.items():     # 序列解包
		print(key, 'corresponds to', value)
	

### 5.5.4 一些迭代工具 ###

	并行迭代
	names = ['anne', 'beth', 'george', 'damon']
	ages = [12, 45, 32, 102]	
	for i in range(len(names)):
		print(names[i], 'is', ages[i], 'years old')
	函数zip，它将两个序列“缝合”起来，并返回一个由元组组成的序列。
	>>> list(zip(names, ages))
	[('anne', 12), ('beth', 45), ('george', 32), ('damon', 102)]
	for name, age in zip(names, ages):
		print(name, 'is', age, 'years old')
	
	迭代时获取索引:使用内置函数enumerat
	for index, string in enumerate(strings):
		if 'xxx' in string:
			strings[index] = '[censored]'

	反向迭代（reversed）和排序（sorted）后再迭代
	可用于任何序列或可迭代的对象，且不就地修改对象，而是返回反转和排序后的版本。
	
### 5.5.5 跳出循环 ###	

	break
	continue

### 5.5.6 循环中的else 子句 ###

	broke_out = False
	for x in seq:
		do_something(x)
		if condition(x):
			broke_out = True
			break
		do_something_else(x)
	if not broke_out:
		print("I didn't break out!")
	
	一种更简单的办法是在循环中添加一条else子句，它仅在没有调用break时才执行。
	
	from math import sqrt
	for n in range(99, 81, -1):
		root = sqrt(n)
		if root == int(root):
			print(n)
			break
	else:
		print("Didn't find it!")

## 5.6 简单推导 ##
	
	推导并不是语句，而是表达式。它们看起来很像循环，因此我将它们放在循环中讨论。

	列表推导是一种从其他列表创建列表的方式，类似于数学中的集合推导。
	
	>>> [x * x for x in range(10)]
	[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]

	>>> [x*x for x in range(10) if x % 3 == 0]
	[0, 9, 36, 81]
	
	还可添加更多的for部分。
	>>> [(x, y) for x in range(3) for y in range(3)]
	[(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]	

	使用圆括号代替方括号并不能实现元组推导，而是将创建生成器。然而，可使用花括号来执行字典推导。
	>>> squares = {i:"{} squared is {}".format(i, i**2) for i in range(10)}
	>>> squares[8]
	'8 squared is 64'
	
## 5.7 三人行 ##

### 5.7.1 什么都不做 ###

	>>> pass
	>>>

在编写代码时，可将其用作占位符。

### 5.7.2 使用del 删除 ###

	>>> x = 1
	>>> del x
	>>> x
	Traceback (most recent call last):
	File "<pyshell#255>", line 1, in ?
	x
	NameError: name 'x' is not defined

	>>> x = ["Hello", "world"]
	>>> y = x
	>>> y[1] = "Python"
	>>> x
	['Hello', 'Python']
	>>> del x
	>>> y
	['Hello', 'Python']  # x和y指向同一个列表，但删除x对y没有任何影响，因为你只删除名称x，而没有删除列表本身（值）。

	del语句不仅会删除到对象的引用，还会删除名称本身。

	在Python中，根本就没有办法删除值，而且你也不需要这样做，因为对于你不再使用的值，Python解释器会立即将其删除。

### 5.7.3 使用exec 和eval 执行字符串及计算其结果 ###

	函数exec将字符串作为代码执行。
	>>> exec("print('Hello, world!')")
	Hello, world!

	在大多数情况下，还应向它传递一个命名空间——用于放置变量的地方；否则代码将污染你的命名空间，即修改你的变量。
	命名空间类似于一个看不见的字典。
	>>> from math import sqrt
	>>> exec("sqrt = 1")
	>>> sqrt(4)
	Traceback (most recent call last):
	File "<pyshell#18>", line 1, in ?
	sqrt(4)
	TypeError: object is not callable: 1
	
	>>> from math import sqrt
	>>> scope = {}
	>>> exec('sqrt = 1', scope)
	>>> sqrt(4)
	2.0
	>>> scope['sqrt']
	1

	eval是一个类似于exec的内置函数。exec执行一系列Python语句，而eval计算用字符串表示的Python表达式的值，并返回结果（exec什么都不返回，因为它本身是条语句）。
	>>> eval(input("Enter an arithmetic expression: "))
	Enter an arithmetic expression: 6 + 18 * 2
	42	
	与exec一样，也可向eval提供一个命名空间。
	
# 第6章 抽象 #

## 6.1 懒惰是一种美德 ##


## 6.2 抽象和结构 ##

一般而言，要判断某个对象是否可调用，可使用内置函数`callable`

	>>> import math
	>>> x = 1
	>>> y = math.sqrt
	>>> callable(x)
	False
	>>> callable(y)
	True

## 6.3 自定义函数 ##

	def hello(name):
		return 'Hello, ' + name + '!'

放在函数开头的字符串称为文档字符串（docstring），将作为函数的一部分存储起来。

	def square(x):
		'Calculates the square of the number x.'
		return x * x
	可以像下面这样访问文档字符串：
	>>> square.__doc__     # _doc__是函数的一个属性
	'Calculates the square of the number x.'

	特殊的内置函数help很有用。在交互式解释器中，可使用它获取有关函数的信息，其中包含函数的文档字符串。
	>>> help(square)
	Help on function square in module __main__:
	
	square(x)
	Calculates the square of the number x.

什么都不返回的函数不包含return语句，或者包含return语句，但没有在return后面指定值。

所有的函数都返回值。如果你没有告诉它们该返回什么，将返回None。

## 6.4 参数魔法 ##

切片返回的是副本

	def hello_1(greeting, name):
		print('{}, {}!'.format(greeting, name))
	>>> hello_1('Hello', 'world')
	Hello, world!
	为了简化调用工作，可指定参数的名称。
	>>> hello_1(greeting='Hello', name='world')
	Hello, world!
	>>> hello_1(name='world', greeting='Hello')
	Hello, world!
	像这样使用名称指定的参数称为关键字参数，主要优点是有助于澄清各个参数的作用。
	关键字参数最大的优点在于，可以指定默认值。
	def hello_3(greeting='Hello', name='world'):
	print('{}, {}!'.format(greeting, name))
	>>> hello_3()
	Hello, world!
	>>> hello_3('Greetings')
	Greetings, world!
	>>> hello_3('Greetings', 'universe')
	Greetings, universe!

	收集参数：
	def print_params(*params):
		print(params)
	>>> print_params('Testing')
	('Testing',)
	>>> print_params(1, 2, 3)
	(1, 2, 3)
	与赋值时一样，带星号的参数也可放在其他位置（而不是最后），但不同的是，在这种情况下你需要做些额外的工作：使用名称来指定后续参数
	>>> def in_the_middle(x, *y, z):
	... print(x, y, z)
	...
	>>> in_the_middle(1, 2, 3, 4, 5, z=7)
	1 (2, 3, 4, 5) 7
	星号不会收集关键字参数。
	>>> print_params_2('Hmm...', something=42)
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	TypeError: print_params_2() got an unexpected keyword argument 'something'
	要收集关键字参数，可使用两个星号。
	要收集关键字参数，可使用两个星号。
	>>> def print_params_3(**params):
	... print(params)
	...
	>>> print_params_3(x=1, y=2, z=3)
	{'z': 3, 'x': 1, 'y': 2}     # 得到的是字典
	

	分配参数
	def add(x, y):
		return x + y
	params = (1, 2)
	>>> add(*params)
	3
	如果在定义和调用函数时都使用*或**，将只传递元组或字典。因此还不如不使用它们。

## 6.5 作用域 ##

有一个名为vars的内置函数，它返回这个不可见的字典：

	>>> x = 1
	>>> scope = vars()
	>>> scope['x']
	1
	>>> scope['x'] += 1
	>>> x
	2	

这种“看不见的字典”称为**命名空间**或**作用域**。

	globals返回一个包含全局变量的字典。
	locals返回一个包含局部变量的字典。

	>>> def combine(parameter):
	... print(parameter + globals()['parameter'])   # 访问全局变量
	...
	>>> parameter = 'berry'
	>>> combine('Shrub')
	Shrubberry

	>>> x = 1
	>>> def change_global():
	... global x    # 重新关联全局变量
	... x = x + 1
	...
	>>> change_global()
	>>> x
	2

	函数可以潜逃
	def foo():
		def bar():
			print("Hello, world!")
		bar()
	嵌套通常用处不大，但有一个很突出的用途：使用一个函数来创建另一个函数。
	def multiplier(factor):
		def multiplyByFactor(number):
			return number * factor
		return multiplyByFactor
	在这里，一个函数位于另一个函数中，且外面的函数返回里面的函数。也就是返回一个函数，而不是调用它。
	每当外部函数被调用时，都将重新定义内部的函数，而变量factor的值也可能不同。
	>>> double = multiplier(2)
	>>> double(5)
	10
	>>> triple = multiplier(3)
	>>> triple(3)
	9
	>>> multiplier(5)(4)
	20

## 6.6 递归 ##

Python提供了一些有助于进行这种函数式编程的函数：`map`、`filter`和`reduce`。在较新的Python版本中，函数map和filter的用途并不大，应该使用列表推导来替代它们。你可使用map将序列的所有元素传递给函数。

	>>> list(map(str, range(10))) 与 [str(i) for i in range(10)] 等价
	['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
	你可使用filter根据布尔函数的返回值来对元素进行过滤。
	>>> def func(x):
	... return x.isalnum()
	...
	>>> seq = ["foo", "x41", "?!", "***"]
	>>> list(filter(func, seq))
	['foo', 'x41']
	>>> [x for x in seq if x.isalnum()]
	['foo', 'x41']


# 第7章 再谈抽象 #	

## 7.1 对象魔法 ##

多态、封装、继承

## 7.2 类 ##

	__metaclass__ = type # 如果你使用的是Python 2，请包含这行代码（在旧版中本创建新类）
	class Person:
		def set_name(self, name):
			self.name = name
		def get_name(self):
			return self.name
		def greet(self):
			print("Hello, world! I'm {}.".format(self.name))
	self指向对象本身
	
	要让方法或属性成为私有的（不能从外部访问），只需让其名称以两个下划线打头即可

	class Secretive:
	def __inaccessible(self):
		print("Bet you can't see me ...")
	def accessible(self):
		print("The secret message is:")
		self.__inaccessible()
	>>> s._Secretive__inaccessible()    #幕后的处理手法并不标准：在类定义中，对所有以两个下划线打头的名称都进行转换，即在开头加上一个下划线和类名。只要知道这种幕后处理手法，就能从类外访问私有方法，然而不应这样做。
	Bet you can't see me ...

	单继承
	class Filter:
		code
	class SPAMFilter(Filter)::
		code
	多继承
	class Calculator:
		code
	class Talker:
		code
	class TalkingCalculator(Calculator, Talker):
		code
	
	要确定一个类是否是另一个类的子类，可使用内置方法issubclass。
	>>> issubclass(SPAMFilter, Filter)
	True
	>>> issubclass(Filter, SPAMFilter)
	False
	如果你有一个类，并想知道它的基类，可访问其特殊属性__bases__。
	>>> SPAMFilter.__bases__
	(<class __main__.Filter at 0x171e40>,)
	>>> Filter.__bases__
	(<class 'object'>,)
	同样，要确定对象是否是特定类的实例，可使用isinstance。
	>>> s = SPAMFilter()
	>>> isinstance(s, SPAMFilter)
	True
	>>> isinstance(s, Filter)
	True
	>>> isinstance(s, str)
	False
	如果你要获悉对象属于哪个类，可使用属性__class__。
	>>> s.__class__
	<class __main__.SPAMFilter at 0x1707c0>
	可使用type(s)来获悉其所属的类

	要查看对象中存储的所有值，可检查其__dict__属性。如果要确定对象是由什么组成的，应研究模块inspect。
	
	abc模块为所谓的抽象基类提供了支持。
	from abc import ABC, abstractmethod
	class Talker(ABC):
		@abstractmethod
		def talk(self):
			pass	
	class Herring:
		def talk(self):
			print("Blub.")
	>>> h = Herring()
	>>> isinstance(h, Talker)
	False
	>>> Talker.register(Herring)     # 将Herring注册为Talker（而不从Herring和Talker派生出子类），这样所有的Herring对象都将被视为Talker对象。
	<class '__main__.Herring'>
	>>> isinstance(h, Talker)
	True
	>>> issubclass(Herring, Talker)
	True

## 7.3 关于面向对象设计的一些思考 ##	

在Python中，所有的属性都是公有的，但直接访问对象的状态时程序员应谨慎行事，因为这可能在不经意间导致状态不一致。


# 第8章 异常 #

## 8.1 异常是什么 ##

## 8.2 让事情沿你指定的轨道出错 ##

### 8.2.1 raise语句 ###

要引发异常，可使用raise语句，并将一个类（必须是Exception的子类）或实例作为参数。将类作为参数时，将自动创建一个实例。

	>>> raise Exception
	Traceback (most recent call last):
	File "<stdin>", line 1, in ?
	Exception
	>>> raise Exception('hyperdrive overload')
	Traceback (most recent call last):
	File "<stdin>", line 1, in ?
	Exception: hyperdrive overload

	一些内置的异常类
	Exception 			几乎所有的异常类都是从它派生而来的
	AttributeError 		引用属性或给它赋值失败时引发
	OSError 			操作系统不能执行指定的任务（如打开文件）时引发，有多个子类
	IndexError 			使用序列中不存在的索引时引发，为LookupError的子类
	KeyError 			使用映射中不存在的键时引发，为LookupError的子类
	NameError 			找不到名称（变量）时引发
	SyntaxError 		代码不正确时引发
	TypeError 			将内置操作或函数用于类型不正确的对象时引发
	ValueError 			将内置操作或函数用于这样的对象时引发：其类型正确但包含的值不合适
	ZeroDivisionError 	在除法或求模运算的第二个参数为零时引发

### 8.2.2 自定义的异常类 ###

	class SomeCustomException(Exception): pass

## 8.3 捕获异常 ##

	try:
		code
	except Exception:
		code

### 8.3.1 不用提供参数 ###

捕获异常后，如果要重新引发它（即继续向上传播），可调用raise且不提供任何参数。

	try:
		pass
	except Excepion:
		if condition:
			pass
		else:
			raise

	try:
		code
	except ZeroDivisionErrot	
		raise ValueError   # Except子句中的异常讲被作为异常上下文存储起来，并出现在最终的错误消息中
	可使用raise ... from ...语句来提供自己的异常上下文，也可使用None来禁用上下文。
		raise ValueError from None

### 8.3.1 多个except子句 ###

	try:
		pass
	except ZeroDivisionError:
		pass
	except TypeError:
		pass

### 8.3.3 一箭双雕 ###

	try:
		pass
	except (ZeroDivisionError,TypeError):
		pass

### 8.3.4 捕获对象 ###

	try:
		pass
	except (ZeroDivisionError,TypeError) as e:
		print(e)

### 8.3.5 一网打尽 ###
	
	try:
		pass
	except:
		pass

	更好的一种方式
	try:
		pass
	except Exception as e:
		print(e)

### 8.3.6 万事大吉时 ###

在有些情况下，在没有出现异常时执行一个代码块很有用。为此，可像条件语句和循环一样，给try/except语句添加一个else子句。

	try:
		pass
	except:
		pass
	else:
		pass

### 8.3.7 最后 ###

	try:
		1 / 0
	except NameError:
		print("Unknown variable")
	else:
		print("That went well!")
	finally:
		print("Cleaning up.")

## 8.4 异常和函数 ##

如果不处理函数中引发的异常，异常讲继续向上传播

## 8.5 异常之禅 ##

用try/except或try/finally代替if

	try:
		obj.write
	except AttributeError:
		print('The object is not writeable')
	else:
		print('The object is writeable')

## 8.6 不那么异常的情况 ##	

如果你只想发出警告，指出情况偏离了正轨，可使用模块`warnings`中的函数`warn`。

警告类似于异常，但（通常）只打印一条错误消息。你可指定警告类别，它们是Warning的子类。

	>>> from warnings import warn
	>>> warn("I've got a bad feeling about this.")
	__main__:1: UserWarning: I've got a bad feeling about this.
	>>>
	警告只显示一次。如果再次运行最后一行代码，什么事情都不会发生。
	如果其他代码在使用你的模块，可使用模块warnings中的函数filterwarnings来抑制你发出的警告（或特定类型的警告），并指定要采取的措施，如"error"或"ignore"。
	>>> from warnings import filterwarnings
	>>> filterwarnings("ignore")
	>>> warn("Anyone out there?")
	>>> filterwarnings("error")
	>>> warn("Something is very wrong!")
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	UserWarning: Something is very wrong!	
	发出警告时，可指定将引发的异常（即警告类别），但必须是Warning的子类。如果将警告转换为错误，将使用你指定的异常。另外，还可根据异常来过滤掉特定类型的警告。
	>>> filterwarnings("error")
	>>> warn("This function is really old...", DeprecationWarning)
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	DeprecationWarning: This function is really old...
	>>> filterwarnings("ignore", category=DeprecationWarning)
	>>> warn("Another deprecation warning.", DeprecationWarning)
	>>> warn("Something else.")
	Traceback (most recent call last):
	File "<stdin>", line 1, in <module>
	UserWarning: Something else.


# 第9章 魔法方法、特性和迭代器 #

## 9.1 如果你使用的不是Python 3 ##

新类的两种表示方式：

	1、在模块开头包含赋值语句__metaclass__ = type
	2、直接或间接地继承内置类object或其他新式类
		calss NewStyle(object):
			pass
在Python 3中没有旧式类，因此无需显式地继承object或将__metaclass__设置为type。所有的类都将隐式地继承object。如果没有指定超类，将直接继承它，否则将间接地继承它。

## 9.2 构造函数 ##

	class FooBar:
		def __init__(self):  # 构造函数
			self.somevar = 42
	>>> f = FooBar()
	>>> f.somevar
	42

Python提供了魔法方法`__del__`，也称作析构函数（destructor）。这个方法在对象被销毁（作为垃圾被收集）前被调用，但鉴于你无法知道准确的调用时间，建议尽可能不要使用__del__。

### 9.2.1 重写普通方法和特殊的构造函数 ###

重写构造函数时，必须调用超类（继承的类）的构造函数，否则可能无法正确地初始化对象。

有两种调用父类的构造方法：调用未关联的超类构造函数，以及使用函数super。

## 9.2.2 调用未关联的超类构造函数 ##

该方法主要用于解决历史遗留问题。在较新的Python版本中，显然应该使用函数super

	class Bird:
		def _init_(self):
			self.hungry=True
	class SongBird(Bird):
		def _init_(self):
			Bird._init(self):
			self.hungry=True

### 9.2.3 使用函数super ###

调用这个函数时，将`当前类`和`当前实例`作为参数。对其返回的对象调用方法时，调用的将是超类（而不是当前类）的方法。

在SongBird的构造函数中，可不使用Bird，而是使用super(SongBird, self)。在Python 3中调用函数super时，可不提供任何参数。

	class Bird:
		def _init_(self):
			self.hungry=True
	class SongBird(Bird):
		def _init_(self):
			super()._init_()
			self.hungry=True

## 9.3 元素访问 ##

在Python中，协议通常指的是规范行为的规则。协议指定应实现哪些方法以及这些方法应做什么。其他的语言可能要求对象属于特定的类或实现了特定的接口，而Python通常只要求对象遵循特定的协议。因此，要成为序列，只需遵循序列协议即可。

### 9.3.1 基本的序列和映射协议 ###

序列和映射基本上是元素（item）的集合，要实现它们的基本行为（协议），不可变对象需要实现2个方法，而可变对象需要实现4个。

	__len__(self)：这个方法应返回集合包含的项数，对序列来说为元素个数，对映射来说
	为键值对数。如果__len__返回零（且没有实现覆盖这种行为的__nonzero__），对象在布
	尔上下文中将被视为假（就像空的列表、元组、字符串和字典一样）。
	__getitem__(self, key)：这个方法应返回与指定键相关联的值。对序列来说，键应该是
	0~n-1的整数（也可以是负数，这将在后面说明），其中n为序列的长度。对映射来说，
	键可以是任何类型。
	__setitem__(self, key, value)：这个方法应以与键相关联的方式存储值，以便以后能够
	使用__getitem__来获取。当然，仅当对象可变时才需要实现这个方法。
	__delitem__(self, key)：这个方法在对对象的组成部分使用__del__语句时被调用，应
	删除与key相关联的值。同样，仅当对象可变（且允许其项被删除）时，才需要实现这个
	方法。

### 9.3.2 从list、dict 和str 派生 ###

使用继承

## 9.4 其他魔法方法 ##

## 9.5 特性 ##

Python能够替你隐藏存取方法，让所有的属性看起来都一样。通过存取方法定义的属性通常称为**特性**（property）。

### 9.5.1 函数property ###

	class C(object):
	    def __init__(self):
	        self._x = None 
	    def getx(self):
	        return self._x
	    def setx(self, value):
	        self._x = value
	    def delx(self):
	        del self._x
	    x = property(getx, setx, delx, "I'm the 'x' property.")
	property函数的参数：fget、fset、fdel、doc 
	如果 c 是 C 的实例化, c.x 将触发 getter,c.x = value 将触发 setter ， del c.x 触发 deleter。
	如果给定 doc 参数，其将成为这个属性值的 docstring，否则 property 函数就会复制 fget 函数的 docstring（如果有的话）。

### 9.5.2 静态方法和类方法 ###

静态方法和类方法是这样创建的：将它们分别包装在staticmethod和classmethod类的对象中。静态方法的定义中没有参数self，可直接通过类来调用。类方法的定义中包含类似于self的参数，通常被命名为cls。对于类方法，也可通过对象直接调用，但参数cls将自动关联到类。

	class MyClass:
		def smeth():
			print('This is a static method')
		smeth = staticmethod(smeth)
		def cmeth(cls):
			print('This is a class method of', cls)
		cmeth = classmethod(cmeth)

	使用装饰器：
	class MyClass:
		@staticmethod
		def smeth():
			print('This is a static method')
		
		@classmethod
		def cmeth(cls):
			print('This is a class method of', cls)

	>>> MyClass.smeth()
	This is a static method
	>>> MyClass.cmeth()
	This is a class method of <class '__main__.MyClass'>

### 9.5.3 __getattr__、__setattr__等方法 ###

可以拦截对对象属性的所有访问企图，其用途之一是在旧式类中实现特性。要在属性被访问时执行一段代码，必须使用一些魔法方法。

	__getattribute__(self, name)：在属性被访问时自动调用（只适用于新式类）。
	__getattr__(self, name)：在属性被访问而对象没有这样的属性时自动调用。
	__setattr__(self, name, value)：试图给属性赋值时自动调用。
	__delattr__(self, name)：试图删除属性时自动调用。


	class Rectangle:
		def __init__ (self):
			self.width = 0
			self.height = 0
		def __setattr__(self, name, value):
			if name == 'size':
				self.width, self.height = value
			else:
				self. __dict__[name] = value
		def __getattr__(self, name):
			if name == 'size':
				return self.width, self.height
			else:
				raise AttributeError()

## 9.6 迭代器 ##

对于魔法方法，这里只介绍`__iter__`，它是迭代器协议的基础。

### 9.6.1 迭代器协议 ###

迭代（iterate）意味着重复多次，就像循环那样。本书前面只使用for循环迭代过序列和字典，但实际上也可迭代其他对象：实现了方法`__iter__`的对象。

方法__iter__返回一个迭代器，它是包含方法__next__的对象，而调用这个方法时可不提供任何参数。当你调用方法__next__时，迭代器应返回其下一个值。如果迭代器没有可供返回的值，应引发StopIteration异常。你还可使用内置的便利函数next，在这种情况下，next(it)与it.__next__()等效

	class Fibs:
		def __init__(self):
			self.a = 0
			self.b = 1
		def __next__(self):
			self.a, self.b = self.b, self.a + self.b
			return self.a
		def __iter__(self):
			return self

	实现了方法__iter__的对象是可迭代的，而实现了方法__next__的对象是迭代器。

### 9.6.2 从迭代器创建序列 ###

除了对迭代器和可迭代对象进行迭代（通常这样做）之外，还可将它们转换为序列。

## 9.7 生成器 ##

生成器是一个相对较新的Python概念。由于历史原因，它也被称为简单生成器（simple generator）。生

生成器是一种使用普通函数语法定义的迭代器。

### 9.7.1 创建生成器 ###
	
	nested = [[1, 2], [3, 4], [5]]
	def flatten(nested):
		for sublist in nested:
			for element in sublist:
				yield element

	包含yield语句的函数都被称为生成器
	生成器不是使用return返回一个值，而是可以生成多个值，每次一个。每次使用yield生成一个值后，函数都将冻结，即在此停止执行，等待被重新唤醒。被重新唤醒后，函数将从停止的地方开始继续执行。

	为使用所有的值，可对生成器进行迭代。
	>>> nested = [[1, 2], [3, 4], [5]]
	>>> for num in flatten(nested):
	... print(num)
	...
	1
	2
	3
	4
	5
	或
	>>> list(flatten(nested))
	[1, 2, 3, 4, 5]

### 9.7.2 递归式生成器 ###


		
# 第10章 开箱即用 #

## 10.1 模块 ##

## 10.1.1 模块就是程序 ##

	# hello.py
	print("Hello,world")
	这个文件的名称（不包括扩展名.py）将成为模块的名称

	告诉解释器去哪儿找这个模块
	>>> import sys
	>>> sys.path.append('C:/python')

模块并不是用来执行操作（如打印文本）的，而是用于定义变量、函数、类等。鉴于定义只需做一次，因此导入模块多次和导入一次的效果相同。
	
	重新加载hello模块
	>>> import importlib
	>>> hello = importlib.reload(hello)
	Hello, world!
	
## 10.1.2 模块是用来下定义的 ##

模块在首次被导入程序时执行。这看似有点用，但用处不大。让模块值得被创建的原因在于它们像类一样，有自己的作用域。这意味着在模块中定义的类和函数以及对其进行赋值的变量都将成为模块的属性。这看似复杂，但实际上非常简单。

	__name__：如果是在主程序输出__main__，如果是在模块中输出__moduleName__
	>>> __name__
	'__main__'
	>>> hello3.__name__
	'hello3'

### 10.1.3 让模块可用 ###
1. 将模块放在正确的位置:

查看默认模块查找位置：

	import sys,pprint 
	pprint.pprint(sys.path)     # 如果要打印的数据结构太大，一行容纳不下，课使用pprint，pprint是个卓越的打印函数，能够更妥善地打印输出。
		
2. 告诉解释器到哪里去查找

将模块所在的目录包含在环境变量PYTHONPATH中。
	
### 10.1.4 包 ###

为组织模块，可将其编组为包（package）。包其实就是另一种模块，但有趣的是它们可包含其他模块。模块存储在扩展名为.py的文件中，而包则是一个目录。要被Python视为包，目录必须包含文件`__init__.py`。如果像普通模块一样导入包，文件__init__.py的内容就将是包的内容。

	一种简单的包布局
	~/python/ 								PYTHONPATH中的目录
	~/python/drawing/ 						包目录（包drawing）
	~/python/drawing/__init__.py 			包代码（模块drawing）
	~/python/drawing/colors.py 				模块colors
	~/python/drawing/shapes.py 				模块shapes

	import drawing # 				(1) 导入drawing包     # 可使用目录drawing中文件__init__.py的内容，但不能使用模块shapes和colors的内容。
	import drawing.colors # 		(2) 导入drawing包中的模块colors	# 只能通过全限定名drawing.colors来使用
	from drawing import shapes # 	(3) 导入模块shapes		# 可直接使用shape

## 10.2 探索模块 ##	

### 10.2.1 模块包含什么 ###

	使用dir：它列出对象的所有属性（对于模块，它列出所有的函数、类、变量等）
	[n for n in dir(cppy) if not n.startswith('_')]

	变量__all__
	>>> copy.__all__
	['Error', 'copy', 'deepcopy']
	在copy.py中
	__all__ = ["Error", "copy", "deepcopy"]
	如果使用下面代码：
	from copy import *
	将只能得到变量__all__中列出的函数，要导入PyStringMap，必须显示导入
	import copy.PyStringMap 或 from copy import PyStringMap

	如果不设置__all__，则会在以import *方式导入时，导入所有不以下划线打头的全局名称。

### 10.2.2 使用help 获取帮助 ###	
	
	>>> help(copy.copy)
	Help on function copy in module copy:
	
	copy(x)
		Shallow copy operation on arbitrary Python objects.  
		See the module's __doc__ string for more info.	# _doc_字符串：文档字符串
	
	>>> print(copy.copy.__doc__)
	Shallow copy operation on arbitrary Python objects.
		
		See the module's __doc__ string for more info.
	

### 10.2.3 文档 ###	

查看模块的源码位置：
	
	>>> print(copy.__file__)
	C:\Python35\lib\copy.py

## 10.3 标准库：一些受欢迎的模块 ##

### 10.3.1 sys ###

模块sys让你能够访问与Python解释器紧密相关的变量和函数

	模块sys中一些重要的函数和变量
	argv 			命令行参数，包括脚本名
	exit([arg]) 	退出当前程序，可通过可选参数指定返回值或错误消息
	modules 		一个字典，将模块名映射到加载的模块
	path 			一个列表，包含要在其中查找模块的目录的名称
	platform 		一个平台标识符，如sunos5或win32
	stdin 			标准输入流——一个类似于文件的对象
	stdout 			标准输出流——一个类似于文件的对象
	stderr 			标准错误流——一个类似于文件的对象

### 10.3.2 os ###

模块os让你能够访问多个操作系统服务。

	模块os中一些重要的函数和变量
	environ 			包含环境变量的映射
	system(command) 	在子shell中执行操作系统命令
	sep 				路径中使用的分隔符 UNIX / Windows \\
	pathsep 			分隔不同路径的分隔符  UNIX ： Windows ;
	linesep 			行分隔符（'\n'、'\r'或'\r\n'）
	urandom(n) 			返回n个字节的强加密随机数据

	os.environ['PATH']

	让浏览器打开网站
	import webbrowser
	webbrowser.open('http://www.python.org')

### 10.3.3 fileinput ###

模块fileinput让你能够轻松地迭代一系列文本文件中的所有行。

	模块fileinput中一些重要的函数
	input([files[, inplace[, backup]]]) 		帮助迭代多个输入流中的行
	filename() 									返回当前文件的名称
	lineno() 									返回（累计的）当前行号
	filelineno() 								返回在当前文件中的行号
	isfirstline() 								检查当前行是否是文件中的第一行
	isstdin() 									检查最后一行是否来自sys.stdin
	nextfile() 									关闭当前文件并移到下一个文件
	close() 									关闭序列

### 10.3.4 集合、堆和双端队列 ###

1、集合

在较新的版本中，集合是由内置类set实现的，这意味着你可直接创建集合，而无需导入模块sets。

集合主要用于成员资格检查，因此将忽略重复的元素：

	>>> {0, 1, 2, 3, 0, 1, 2, 3, 4, 5}
	{0, 1, 2, 3, 4, 5}

与字典一样，集合中元素的排列顺序是不确定的，因此不能依赖于这一点。

	>>> {'fee', 'fie', 'foe'}
	{'foe', 'fee', 'fie'}

集合是可变的，因此不能用作字典中的键。另一个问题是，集合只能包含不可变（可散列）的值，因此不能包含其他集合。

2、堆

堆是一种优先

Python没有独立的堆类型，而只有一个包含一些堆操作函数的模块。这个模块名为`heapq`（其中的q表示队列）

	模块heapq中一些重要的函数
	heappush(heap, x) 			将x压入堆中
	heappop(heap) 				从堆中弹出最小的元素
	heapify(heap) 				让列表具备堆特征
	heapreplace(heap, x) 		弹出最小的元素，并将x压入堆中
	nlargest(n, iter) 			返回iter中n个最大的元素
	nsmallest(n, iter) 			返回iter中n个最小的元素


3、双端队列（及其他集合）

在模块collections中，包含类型deque以及其他几个集合（collection）类型。

### 10.3.5 time ###

元组`(2008, 1, 21, 12, 2, 56, 0, 21, 0)`表示2008年1月21日12时2分56秒。这一天是星期一，2008年的第21天（不考虑夏时）。

	模块time中一些重要的函数
	asctime([tuple]) 			将时间元组转换为字符串
	localtime([secs]) 			将秒数转换为表示当地时间的日期元组
	mktime(tuple) 				将时间元组转换为当地时间
	sleep(secs) 				休眠（什么都不做）secs秒
	strptime(string[, format]) 	将字符串转换为时间元组
	time() 						当前时间（从新纪元开始后的秒数，以UTC为准）

### 10.3.6 random ###

模块random包含生成伪随机数的函数，有助于编写模拟程序或生成随机输出的程序。

	模块random中一些重要的函数
	random() 							返回一个0~1（含）的随机实数
	getrandbits(n) 						以长整数方式返回n个随机的二进制位
	uniform(a, b) 						返回一个a~b（含）的随机实数
	randrange([start], stop, [step]) 	从range(start, stop, step)中随机地选择一个数
	choice(seq) 						从序列seq中随机地选择一个元素
	shuffle(seq[, random]) 				就地打乱序列seq
	sample(seq, n) 						从序列seq中随机地选择n个值不同的元素

### 10.3.7 shelve 和json ###

用于文件处理

### 10.3.8 re ###

模块re提供了对正则表达式的支持。


## 10.3.9 其它有趣的标准模块 ##
	
	timeit、profile和trace：模块timeit（和配套的命令行脚本）是一个测量代码段执行时
	间的工具。这个模块暗藏玄机，度量性能时你可能应该使用它而不是模块time。模块
	profile（和配套模块pstats）可用于对代码段的效率进行更全面的分析。模块trace可帮
	助你进行覆盖率分析（即代码的哪些部分执行了，哪些部分没有执行），这在编写测试代
	码时很有用。


# 第11章节 文件 #

## 11.1 打开文件 ##

要打开文件，可使用函数open，它位于自动导入的模块`io`中。

文件模式：  
调用函数open时，如果只指定文件名，将获得一个可读取的文件对象。如果要写入文件，必须通过指定模式来显式地指出这一点。

	函数open的参数mode的最常见取值：
	'r' 		读取模式（默认值）
	'w' 		写入模式
	'x' 		独占写入模式
	'a' 		附加模式
	'b' 		二进制模式（与其他模式结合使用）
	't' 		文本模式（默认值，与其他模式结合使用）
	'+' 		读写模式（与其他模式结合使用）

写入模式让你能够写入文件，并在文件不存在时创建它。会覆盖。

r+'和'w+'之间有个重要差别：后者删除文件，而前者不会这样做。

默认模式为'rt'

通常，Python使用通用换行模式。在这种模式下，后面将讨论的`readlines`等方法能够识别所有合法的换行符（'\n'、'\r'和'\r\n'）。如果要使用这种模式，同时禁止自动转换，可将关键字参数newline设置为空字符串，如`open(name, newline='')`。


## 11.2 文件的基本方法 ##

### 11.2.1 读取和写入 ###

	f.write('str')
	
	f.read(n)

### 11.2.2 使用管道重定向输出 ###

随机存取：

	seek()：移动
	tell()：返回当前文件的位置

### 11.2.3 读取和写入行 ###

	readline
	readlines
	writelines

### 11.2.4 关闭文件 ###

对于写入过的文件，一定要将其关闭，因为Python可能缓冲你写入的数据（将数据暂时存储在某个地方，以提高效率）。	

关闭文件的方式：

	# 在这里打开文件
	try:
	# 将数据写入到文件中
	finally:
		file.close()

	实际上，有一条专门为此设计的语句，那就是with语句。
	with open("somefile.txt") as somefile:
		do_something(somefile)
	
	with语句让你能够打开文件并将其赋给一个变量（这里是somefile）。在语句体中，你将数据
	写入文件（还可能做其他事情）。到达该语句末尾时，将自动关闭文件，即便出现异常亦如此。

### 11.3 迭代文件内容 ###

	def process(string):
		print('Processing:', string)

### 11.3.1 每次一个字符（或字节） ###

	with open(filename) as f:
		char = f.read(1)
		while char:
			process(char)
			char = f.read(1)   # 代码重复

	
	with open(filename) as f:
		while True:
			char = f.read(1)
			if not char: break
			process(char)

### 13.3.2 每次一行 ###

	with open(filename) as f:
		while True:
			line = f.readline()
			if not line: break
			process(line)

### 11.3.3 读取所有内容 ###

	with open(filename) as f:
		for char in f.read():
			process(char)
	
	with open(filename) as f:
		for line in f.readlines():
			process(line)

### 11.3.4 使用fileinput实现延迟行执行 ###

	import fileinput
	for line in fileinput.input(filename):
		process(line)

### 11.3.5 文件迭代器 ###

	迭代文件
	with open(filename) as f:
		for line in f:
			process(line)

	在不将文件对象赋给变量的情况下迭代文件
	for line in open(filename):
		process(line)	
	
	import sys
	for line in sys.stdin:
		process(line)

# 第12章 图形用户界面 #

`Tkinter`是事实上的Python标准GUI工具包，包含在Python标准安装中。

## 12.1 创建GUI示例应用程序 ##

### 12.1.1 初探 ###

	import tkinter as tk
	>>> from tkinter import *
	>>> top = Tk()

## 12.2 使用其他GUI 工具包 ##

# 第13章 数据库支持 #

## 13.1 Python 数据库API ##

### 13.1.1 全局变量 ###

所有与DB API2.0兼容的数据库模块都必须包含三个全局变量，它们描述了模块的特征。

	Python DB API的模块属性
	apilevel 			使用的Python DB API版本
	threadsafety 		模块的线程安全程度如何
	paramstyle 			在SQL查询中使用哪种参数风格

### 13.1.2 异常 ###

### 13.1.3 连接和游标 ###

使用`connect`函数

### 13.1.4 类型 ###

## 13.2 SQLite 和PySQLite ##

可用的SQL数据库引擎有很多，它们都有相应的Python模块。

### 13.2.1 起步 ###

	>>> import sqlite3
	>>> conn = sqlite3.connect('somedatabase.db')
	接下来可从连接获得游标。
	>>> curs = conn.cursor()
	>>> conn.commit()
	>>> conn.close()


# 第14章节 网络编程 #

## 14.1 几个网络模块 ##

### 14.1.1 模块socket ###




