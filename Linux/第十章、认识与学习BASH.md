# 10.1 认识BASH 这个Shell #

## 10.1.1 硬体、核心与Shell ##

我们必须要透过『 Shell 』将我们输入的指令与Kernel沟通，好让Kernel可以控制硬体来正确无误的工作！  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/0320bash_1.jpg)

也就是说，只要能够操作应用程式的介面都能够称为壳程式。狭义的壳程式指的是指令列方面的软体，包括本章要介绍的bash 等。广义的壳程式则包括图形介面的软体！因为图形介面其实也能够操作各种应用程式来呼叫核心工作啊！

## 10.1.2 为何要学文字介面的shell？ ##

	文字介面的shell：大家都一样！
	远端管理：文字介面就是比较快！
	Linux 的任督二脉： shell 是也！

## 10.1.3 系统的合法shell 与/etc/shells 功能 ##

常见的shell：Bourne SHell (sh) 、在Sun里头预设的C SHell、商业上常用的K SHell、 ,还有TCSH等等，每一种Shell都各有其特点。至于Linux使用的这一种版本就称为『 Bourne Again SHell (简称bash) 』，这个Shell是Bourne Shell的增强版本，也是基准于GNU的架构下发展出来的呦！

CentOS可用的shell写入`/etc/shells`文件

为什么我们系统上合法的shell要写入/etc/shells这个档案啊？ 这是因为系统某些服务在运作过程中，会去检查使用者能够使用的shells ，而这些shell的查询就是藉由/etc/shells这个档案啰！

## 10.1.4 Bash shell 的功能 ##

	命令编修能力(history)
		~/.bash_history记录的是前一次登入以前所执行过的指令，而至于这一次登入所执行的指令都
		被暂存在记忆体中，当你成功的登出系统后，该指令记忆才会记录到.bash_history当中！
	命令与档案补全功能： ([tab] 按键的好处):
	命令别名设定功能： (alias)
		alias lm='ls -al'
	工作控制、前景背景控制： (job control, foreground, background)
		背景中执行的工作，不能用Ctrl+c杀掉
	程式化脚本： (shell scripts)
	万用字元： (Wildcard)

## 10.1.5 查询指令是否为Bash shell 的内建命令： type ##

	type [-tpa] name
	选项与参数：
		   ：不加任何选项与参数时，type 会显示出name 是外部指令还是bash 内建指令
		-t ：当加入-t 参数时，type 会将name 以底下这些字眼显示出他的意义：
		      file ：表示为外部指令；
		      alias ：表示该指令为命令别名所设定的名称；
		      builtin ：表示该指令为bash 内建的指令功能；
		-p ：如果后面接的name 为外部指令时，才会显示完整档名；
		-a ：会由PATH 变数定义的路径中，将所有含name 的指令都列出来，包含alias

利用type搜寻后面的名称时，如果后面接的名称并不能以执行档的状态被找到，那么该名称是不会被显示出来的。也就是说， type主要在找出『执行档』而不是一般档案档名喔！呵呵！所以，这个type也可以用来作为类似which 指令的用途啦！找指令用的！	
	
## 10.1.6 指令的下达与快速编辑按钮 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/instruction-key.jpg)


# 10.2 Shell 的变数功能 #

## 10.2.1 什么是变数？ ##

- 变数的可变性与方便性  
MAIL变量：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var_01.gif)

- 影响bash 环境操作的变数  
在Linux System下面，所有的执行绪都是需要一个执行码。『真正以shell来跟Linux沟通，是在正确的登入Linux之后！』这个时候你就有一个bash的执行程序，也才可以真正的经由bash来跟系统沟通啰！而在进入shell之前，也正如同上面提到的，由于系统需要一些变数来提供他资料的存取(或者是一些环境的设定参数值，例如是否要显示彩色等等的) ，所以就有一些所谓的『环境变数』需要来读入系统中了！

- 脚本程式设计(shell script) 的好帮手

## 10.2.2 变数的取用与设定：echo, 变数设定规则, unset ##

- 变数的取用: echo：

	echo $variable
	echo ${varible}

- 变数的设定规则

		1、变数与变数内容以一个等号『=』来连结，如下所示： 
		『myname=VBird』
		
		2、等号两边不能直接接空白字元，如下所示为错误： 
		『myname = VBird』或『myname=VBird Tsai』
		
		3、变数名称只能是英文字母与数字，但是开头字元不能是数字，如下为错误： 
		『2myname=VBird』
		
		4、变数内容若有空白字元可使用双引号『"』或单引号『'』将变数内容结合起来，但
			双引号内的特殊字元如$等，可以保有原本的特性，如下所示：
			『var="lang is $LANG"』则『echo $var』可得『lang is zh_TW.UTF-8』
			单引号内的特殊字元则仅为一般字元(纯文字)，如下所示：
			『var='lang is $LANG'』则『echo $var』可得『lang is $LANG』
		
		5、可用跳脱字元『 \ 』将特殊符号(如[Enter], $, \,空白字元, '等)变成一般字元，如：
		『myname=VBird\ Tsai』
		
		6、在一串指令的执行中，还需要藉由其他额外的指令所提供的资讯时，可以使用反单引号『`指令`』或『$(指令)』。
			特别注意，那个`是键盘上方的数字键1左边那个按键，而不是单引号！例如想要取得核心版本的设定：
		『version=$(uname -r)』再『echo $version』可得『3.10.0-229.el7.x86_64』
		
		7、若该变数为扩增变数内容时，则可用"$变数名称"或${变数}累加内容，如下所示：
		『PATH="$PATH":/home/bin』或『PATH=${PATH} :/home/bin』
		
		8、若该变数需要在其他子程序执行，则需要以export来使变数变成环境变数：
		『export PATH』
		
		9、通常大写字元为系统预设变数，自行设定变数可以使用小写字元，方便判断(纯粹依照使用者兴趣与嗜好) ；
		
		10、取消变数的方法为使用unset：『unset变数名称』例如取消myname的设定：
		『unset myname』


## 10.2.3 环境变数的功能 ##

- 用env 观察环境变数与常见环境变数说明

		env
			HOME、SHELL、HISTSIZE、MAIL、PATH、LANG、RANDOM
		大多数的distributions都会有乱数产生器，那就是/dev/random这个档案。

- 用set 观察所有变数(含环境变数与自订变数)  

	一般来说，不论是否为环境变数，只要跟我们目前这个shell的操作介面有关的变数，通常都会被设定为大写字元，也就是说，『基本上，在Linux预设的情况中，使用{大写的字母}来设定的变数一般为系统内定需要的变数』。

	PS1：(提示字元的设定)	 # man bash--> /PS1

		\d ：可显示出『星期月日』的日期格式，如："Mon Feb 2"
		\H ：完整的主机名称。举例来说，鸟哥的练习机为『study.centos.vbird』
		\h ：仅取主机名称在第一个小数点之前的名字，如鸟哥主机则为『study』后面省略
		\t ：显示时间，为24 小时格式的『HH:MM:SS』
		\T ：显示时间，为12 小时格式的『HH:MM:SS』
		\A ：显示时间，为24 小时格式的『HH:MM』
		\@ ：显示时间，为12 小时格式的『am/pm』样式
		\u ：目前使用者的帐号名称，如『dmtsai』；
		\v ：BASH 的版本资讯，如鸟哥的测试主机版本为4.2.46(1)-release，仅取『4.2』显示
		\w ：完整的工作目录名称，由根目录写起的目录名称。但家目录会以~ 取代；
		\W ：利用basename 函数取得工作目录名称，所以仅会列出最后一个目录名。
		\# ：下达的第几个指令。
		\$ ：提示字元，如果是root 时，提示字元为# ，否则就是$ 啰～

	$：(关于本shell 的PID)  
	?：(关于上个执行指令的回传值)  
	OSTYPE, HOSTTYPE, MACHTYPE：(主机硬体与核心的等级)  

			较高阶的硬体通常会向下相容旧有的软体，但较高阶的软体可能无法在旧机器上面安装！例
			如：可以在x86_64的硬体上安装i386的Linux作业系统，但是你无法在i686的硬体上安装x86_64的Linux作业系统！

- export： 自订变数转成环境变数  
程序相关性示意图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/ppid.gif)

子程序仅会继承父程序的环境变数，子程序不会继承父程序的自订变数！

	export 变数名称

## 10.2.4 影响显示结果的语系变数(locale) ##
	
	列出支持的语系：
		locale -a
	列出语系变量设定：	
		locale
			LANG=en_US                    <==主语言的环境 
			LC_CTYPE="en_US"              <==字元(文字)辨识的编码 
			LC_NUMERIC="en_US"            <==数字系统的显示讯息 
			LC_TIME="en_US"               <==时间系统的显示资料 
			LC_COLLATE="en_US"            <==字串的比较与排序等 
			LC_MONETARY="en_US"           <==币值格式的显示等 
			LC_MESSAGES="en_US"           <==讯息显示的内容，如功能表、错误讯息等 
			LC_ALL=                       <==整体语系的环境

	基本上，你可以逐一设定每个与语系有关的变数资料，但事实上，如果其他的语系变数都未设定，且你
	有设定LANG或者是LC_ALL时，则其他的语系变数就会被这两个变数所取代！ 这也是为什么我们在
	Linux当中，通常说明仅设定LANG或LC_ALL这两个变数而已，因为他是最主要的设定变数！	

	语系档案放置目录：/usr/lib/locale/
	整体语系设置档：/etc/locale.conf 

## 10.2.5 变数的有效范围 ##

	环境变数=全域变数
	自订变数=局部变数

	当启动一个shell，作业系统会分配一记忆区块给shell 使用，此记忆体内之变数可让子程序取用
	若在父程序利用export 功能，可以让自订变数的内容写到上述的记忆区块当中(环境变数)；
	当载入另一个shell 时(亦即启动子程序，而离开原本的父程序了)，子shell 可以将父shell 的环境变数所在的记忆区块导入自己的环境变数区块当中。

	『环境变数』与『bash 的操作环境』意思不太一样，举例来说， PS1 并不是环境变数， 但是这个PS1 会影响到bash 的介面(提示字元嘛)！

## 10.2.6 变数键盘读取、阵列与宣告： read, array, declare ##

	read [-pt] variable
	选项与参数：
		-p ：后面可以接提示字元！
		-t ：后面可以接等待的『秒数！』这个比较有趣～不会一直等待使用者啦！

	declare或typeset是一样的功能，就是在『宣告变数的类型』。
	declare [-aixr] variable
	选项与参数：
		-a ：将后面名为variable 的变数定义成为阵列(array) 类型
		-i ：将后面名为variable 的变数定义成为整数数字(integer) 类型
		-x ：用法与export 一样，就是将后面的variable 变成环境变数；
		-r ：将变数设定成为readonly 类型，该变数不可被更改内容，也不能unset
		-p：列出某个变量的类型
	declare +x sum   <==将-变成+可以进行『取消』动作

	变数类型预设为『字串』，所以若不指定变数类型，则1+2 为一个『字串』而不是『计算式』。
	bash 环境中的数值运算，预设最多仅能到达整数形态，所以1/3 结果是0；

	var[index]=content  # 数组类型，建议直接以${数组}的方式来读取，比较正确无误的啦！

## 10.2.7 与档案系统及程序的限制关系： ulimit ##

bash是可以『限制使用者的某些系统资源』的，包括可以开启的档案数量，可以使用的CPU时间，可以使用的记忆体总量等等。

	ulimit [-SHacdfltu] [配额]
	选项与参数：
		-H ：hard limit ，严格的设定，必定不能超过这个设定的数值；
		-S ：soft limit ，警告的设定，可以超过这个设定值，但是若超过则有警告讯息。
		      在设定上，通常soft 会比hard 小，举例来说，soft 可设定为80 而hard 
		      设定为100，那么你可以使用到90 (因为没有超过100)，但介于80~100 之间时，
		      系统会有警告讯息通知你！
		-a ：后面不接任何选项与参数，可列出所有的限制额度；
		-c ：当某些程式发生错误时，系统可能会将该程式在记忆体中的资讯写成档案(除错用)，
		      这种档案就被称为核心档案(core file)。此为限制每个核心档案的最大容量。
		-f ：此shell 可以建立的最大档案容量(一般可能设定为2GB)单位为Kbytes
		-d ：程序可使用的最大断裂记忆体(segment)容量；
		-l ：可用于锁定(lock) 的记忆体量
		-t ：可使用的最大CPU 时间(单位为秒)
		-u ：单一使用者可以使用的最大程序(process)数量。

想要复原ulimit的设定最简单的方法就是登出再登入，否则就是得要重新以ulimit设定才行！不过，要注意的是，一般身份使用者如果以ulimit设定了-f的档案大小，那么他『只能继续减小档案容量，不能增加档案容量喔！』另外，若想要管控使用者的ulimit限值，可以参考第十三章的pam的介绍。

## 10.2.8 变数内容的删除、取代与替换(Optional) ##

- 变数内容的删除与取代  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-del-replace.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-del-%23.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-del-%25.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-replace.jpg)
	
- 变数的测试与内容替换  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-test.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-test-1.jpg)
	
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/var-test-2.jpg)

# 10.3 命令别名与历史命令 #

## 10.3.1 命令别名设定： alias, unalias ##	

『alias的定义规则与变数定义规则几乎相同』，所以你只要在alias后面加上你的{ 『别名』='指令选项...' }

	alias lm='ls -al | more'
	unalias lm

## 10.3.2 历史命令：history ##

	history [n]
	history [-c]
	history [-raw] histfiles
	选项与参数：
		 n ：数字，意思是『要列出最近的n 笔命令列表』的意思！
		-c ：将目前的shell 中的所有history 内容全部消除
		-a ：将目前新增的history 指令新增入histfiles 中，若没有加histfiles ，
		      则预设写入~/.bash_history
		-r ：将histfiles 的内容读到目前这个shell 的history 记忆中；
		-w ：将目前的history 记忆内容写入histfiles 中！

	!number
	!command
	!!
	选项与参数：
		number ：执行第几笔指令的意思；
		command ：由最近的指令向前搜寻『指令串开头为command』的那个指令，并执行；
		!! ：就是执行上一个指令(相当于按↑按键后，按Enter)

	[dmtsai@study ~]$ history
	   66 man rm
	   67 alias
	   68 man history
	   69 history 
	[dmtsai@study ~]$ !66   <==执行第66笔指令 
	[dmtsai@study ~]$ !!   <==执行上一个指令，本例中亦即!66 
	[dmtsai@study ~]$ !al   <==执行最近以al为开头的指令(上头列出的第67个)

历史命令无法记录指令下达的时间，其实可以透过`~/.bash_logout` 来进行history 的记录，并加上date 来增加时间参数。

# 10.4 Bash Shell 的操作环境 #

## 10.4.1 路径与指令搜寻顺序 ##

指令搜寻顺序：

	1、以相对/绝对路径执行指令，例如『 /bin/ls 』或『 ./ls 』；
	2、由alias 找到该指令来执行；
	3、由bash 内建的(builtin) 指令来执行；
	4、透过$PATH 这个变数的顺序搜寻到的第一个指令来执行。

## 10.4.2 bash 的进站与欢迎讯息： /etc/issue, /etc/motd ##
/etc/issues设置：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/issue.jpg)

除了/etc/issue 之外还有个/etc/issue.net 呢！这是啥？这个是提供给telnet 这个远端登入程式用的。当我们使用telnet 连接到主机时，主机的登入画面就会显示/etc/issue.net 而不是/etc/issue 呢！

如果您想要让使用者登入后取得一些讯息，例如您想要让大家都知道的讯息，那么可以将讯息加入`/etc/motd`里面去！

## 10.4.3 bash 的环境设定档 ##

你是否会觉得奇怪，怎么我们什么动作都没有进行，但是一进入bash 就取得一堆有用的变数了？这是因为系统有一些环境设定档案的存在，让bash 在启动时直接读取这些设定档，以规划好bash 的操作环境啦！而这些设定档又可以分为全体系统的设定档以及使用者个人偏好设定档。

- login 与non-login shell

		login shell：取得bash 时需要完整的登入流程的，就称为login shell。举例来说，你要由
		tty1 ~ tty6 登入，需要输入使用者的帐号与密码，此时取得的bash 就称为『 login shell 』啰；
	
		non-login shell：取得bash 介面的方法不需要重复登入的举动，举例来说，(1)你以X window 登入Linux 后， 
		再以X 的图形化介面启动终端机，此时那个终端介面并没有需要再次的输入帐号与密码，那个bash 的环境就称为
		non-login shell了。(2)你在原本的bash 环境下再次下达bash 这个指令，同样的也没有输入帐号密码， 那第二个bash (子程序) 也是non-login shell 。

	login, non-login shell读取的设定档资料并不一样所致。一般来说，login shell 其实只会读取这两个设定档：

		1、/etc/profile：这是系统整体的设定，你最好不要修改这个档案；
		2、~/.bash_profile 或~/.bash_login 或~/.profile：属于使用者个人设定，你要改自己的资料，就写入这里！

- etc/profile (login shell 才会读)

	这个档案设定的变数主要有：

		PATH：会依据UID 决定PATH 变数要不要含有sbin 的系统指令目录；
		MAIL：依据帐号设定好使用者的mailbox 到/var/spool/mail/帐号名；
		USER：根据使用者的帐号设定此一变数内容；
		HOSTNAME：依据主机的hostname 指令决定此一变数内容；
		HISTSIZE：历史命令记录笔数。CentOS 7.x 设定为1000 ；
		umask：包括root 预设为022 而一般用户为002 等！

		/etc/profile
			/etc/profile.d/*.sh
				其实这是个目录内的众多档案！只要在/etc/profile.d/ 这个目录内且副档名为.sh ，另外，使用者能够具有r 的权限， 那么该档案就会被/etc/profile 呼叫进
				来。在CentOS 7.x 中，这个目录底下的档案规范了bash 操作介面的颜色、 语系、ll 与ls 指令的命令别名、vi 的命令别名、which 的命令别名等等。如果你需要帮所有使
				用者设定一些共用的命令别名时， 可以在这个目录底下自行建立副档名为.sh 的档案，并将所需要的资料写入即可喔！
				/etc/profile.d/lang.h-->/etc/locale.conf
				/etc/profile.d/bash_completion.sh-->/usr/share/bash-completion/completions/*
		
- ~/.bash_profile (login shell 才会读)

	在login shell 的bash 环境中，所读取的个人偏好设定档其实主要有三个，依序分别是：

		~/.bash_profile
		~/.bash_login
		~/.profile		
			
	其实bash的login shell设定只会读取上面三个档案的其中一个，而读取的顺序则是依照上面的顺序。

	
		~/.bash_profile
			~/.bashrc
				/etc/bashrc
					/etc/profile.d/*.sh
		这个档案内有设定PATH 这个变数喔！而且还使用了export 将PATH 变成环境变数呢！由于PATH
		在/etc/profile 当中已经设定过，所以在这里就以累加的方式增加使用者家目录下的~/bin/ 为额外的执行档放置目录。
				

	login shell 的设定档读取流程：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/centos7_bashrc_1.gif)

- source ：读入环境设定档的指令

		source 	设定档档名
		.		设定档档名

	利用source 或小数点(.) 都可以将修改的设定档的内容读进来目前的shell 环境中！而不用重新登录。


- ~/.bashrc (non-login shell 会读)

		~/.bashrc
			/etc/bashrc
				/etc/profile.d/*.sh

		[root@study ~]# cat ~/.bashrc
		# .bashrc
		
		# User specific aliases and functions
		alias rm='rm -i'              <==使用者的个人设定
		alias cp='cp -i'
		alias mv='mv -i'
		
		# Source global definitions
		if [ -f /etc/bashrc ]; then   <==整体的环境设定
		        . /etc/bashrc
		fi

	为什么需要呼叫/etc/bashrc呢？因为/etc/bashrc帮我们的bash定义出底下的资料：
		
		依据不同的UID规范出umask的值；
		依据不同的UID 规范出提示字元(就是PS1 变数)；
		呼叫/etc/profile.d/*.sh 的设定

	要注意的是，这个/etc/bashrc 是CentOS 特有的(其实是Red Hat 系统特有的)，其他不同的distributions 可能会放置在不同的档名就是了。由于这个~/.bashrc 会呼叫/etc/bashrc 及/etc/profile.d/*.sh ， 所以，万一你没有~/.bashrc (可能自己不小心将他删除了)，那么你会发现你的bash 提示字元可能会变成这个样子：

		-bash-4.2$ 
	
	不要太担心啦！这是正常的，因为你并没有呼叫/etc/bashrc 来规范PS1 变数啦！而且这样的情况也不会影响你的bash 使用。如果你想要将命令提示字元捉回来，那么可以复制``/etc/skel/.bashrc`` 到你的家目录，再修订一下你所想要的内容， 并使用source 去呼叫~/.bashrc ，那你的命令提示字元就会回来啦

- 其他相关设定档

	/etc/man_db.conf
		
		规范了使用 man的时候， man page的路径到哪里去寻找！

	~/.bash_history

		我们的历史命令就记录在这里啊！

	~/.bash_logout

		这个档案则记录了『当我登出bash后，系统再帮我做完什么动作后才离开』的意思。你可以去读取
		一下这个档案的内容，预设的情况下，登出时， bash只是帮我们清掉萤幕的讯息而已。不过，你
		也可以将一些备份或者是其他你认为重要的工作写在这个档案中(例如清空暂存档)，那么当你离开Linux的时候，就可以解决一些烦人的事情啰！


----------

	profile:对应登陆的,登陆的时候设定好环境变量
		profile 是某个用户唯一的用来设置环境变量的地方, 因为用户可以有多个 shell 比如 bash, sh, zsh 之类的, 但像环境变量这种其实只需要在统一的一个地方初始化就可以了, 而这就是 profile.
	bashrc:对应打开shell的
		是专门用来给 bash 做初始化的比如用来初始化 bash 的设置, bash 的代码补全, bash 的别名, bash 的颜色. 以此类推也就还会有 shrc, zshrc 这样的文件存在了, 只是 bash 太常用了而已.


## 10.4.4 终端机的环境设定： stty, set ##

	stty [-a]  		(setting tty)
	选项与参数：
		-a ：将目前所有的stty 参数列出来

可以利用stty -a来列出目前环境中所有的按键列表：

	intr : 送出一个interrupt (中断) 的讯号给目前正在run 的程序(就是终止啰！)；
	quit : 送出一个quit 的讯号给目前正在run 的程序；
	erase : 向后删除字元，
	kill : 删除在目前指令列上的所有文字；
	eof : End of file 的意思，代表『结束输入』。
	start : 在某个程序停止后，重新启动他的output
	stop : 停止目前萤幕的输出；
	susp : 送出一个terminal stop 的讯号给正在run 的程序。

	stty erase ^h


除了stty 之外，其实我们的bash 还有自己的一些终端机设定值呢！那就是利用set 来设定的！我们之前提到一些变数时，可以利用set 来显示，除此之外，其实set 还可以帮我们设定整个指令输出/输入的环境。例如记录历史命令、显示错误内容等等。

	set [-uvCHhmBx]
	选项与参数：
		-u ：预设不启用。若启用后，当使用未设定变数时，会显示错误讯息；
		-v ：预设不启用。若启用后，在讯息被输出前，会先显示讯息的原始内容；
		-x ：预设不启用。若启用后，在指令被执行前，会显示指令内容(前面有++ 符号)
		-h ：预设启用。与历史命令有关；
		-H ：预设启用。与历史命令有关；
		-m ：预设启用。与工作管理有关；
		-B ：预设启用。与刮号[] 的作用有关；
		-C ：预设不启用。若使用> 等，则若档案存在时，该档案不会被覆盖。

	echo $-
	# 那个$- 变数内容就是set 的所有设定啦！bash 预设是himBH 喔！

按键设定档：`/etc/inputrc`  
还有例如`/etc/DIR_COLORS* 与/usr/share/terminfo/* `等，也都是与终端机有关的环境设定档案呢！

bash预设组合键：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/stty-key-hot.jpg)

## 10.4.5 万用字元与特殊符号 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/bash-wildcard.jpg)
 
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/bash-symbol.jpg)

# 10.5 资料流重导向 #

## 10.5.1 什么是资料流重导向 ##

指令执行过程的资料传输情况：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/centos7_redirection.jpg)

- standard output 与standard error output  
标准输出指的是『指令执行所回传的正确的讯息』，而标准错误输出可理解为『指令执行失败后，所回传的错误讯息』。

		标准输入(stdin) ：代码为0 ，使用< 或<< ；
		标准输出(stdout)：代码为1 ，使用> 或>> ；
		标准错误输出(stderr)：代码为2 ，使用2> 或2>> ；

		1> ：以覆盖的方法将『正确的资料』输出到指定的档案或装置上；
		1>>：以累加的方法将『正确的资料』输出到指定的档案或装置上；
		2> ：以覆盖的方法将『错误的资料』输出到指定的档案或装置上；
		2>>：以累加的方法将『错误的资料』输出到指定的档案或装置上；

- /dev/null 垃圾桶黑洞装置与特殊写法  
	
	这个/dev/null 可以吃掉任何导向这个装置的资讯喔！	
	
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/special-writting.jpg)
		
		第一行错误的原因是，由于两股资料同时写入一个档案，又没有使用特殊的语法，此时两股资料可
		能会交叉写入该档案内，造成次序的错乱。所以虽然最终list档案还是会产生，但是里面的资料排
		列就会怪怪的，而不是原本萤幕上的输出排序。

- standard input ： < 与<<  
将原本需要由键盘输入的资料，改由档案内容来取代。	
			
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/stdin-1.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/stdin-2.jpg)

	``<<``代表的是『结束的输入字元』的意思！

## 10.5.2 命令执行的判断依据： ; , &&, || ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/cmd-and-or.jpg)

由于指令是一个接着一个去执行的，因此，如果真要使用判断，那么这个&&与||的顺序就不能搞错。一般来说，假设判断式有三个，也就是：

	command1 && command2 || command3

而且顺序通常不会变，因为一般来说， command2 与command3 会放置肯定可以执行成功的指令。

# 10.6 管线命令(pipe) #

管线命令『 | 』仅能处理经由前面一个指令传来的正确资讯，也就是standard output的资讯，对于stdandard error并没有直接处理的能力。

管线命令的处理示意图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/0320bash_3.png)
	
在每个管线后面接的第一个资料必定是『指令』喔！而且这个指令必须要能够接受standard input的资料才行，这样的指令才可以是为『管线命令』，例如less, more, head, tail等都是可以接受standard input的管线命令啦。至于例如ls, cp, mv等就不是管线命令了！因为ls, cp, mv并不会接受来自stdin的资料。也就是说，管线命令主要有两个比较需要注意的地方：

	管线命令仅会处理standard output，对于standard error output 会予以忽略
	管线命令必须要能够接受来自前一个指令的资料成为standard input 继续处理才行。

如果你硬要让standard error 可以被管线命令所使用，那该如何处理？其实就是透过上一小节的资料流重导向即可！让2>&1 加入指令中～就可以让2> 变成1> 啰！

## 10.6.1 撷取命令： cut, grep ##

一般来说，撷取讯息通常是针对『一行一行』来分析的，并不是整篇讯息分析的喔

	cut -d'分隔字元' -f fields  <==用于有特定分隔字元 
	cut -c字元区间             <==用于排列整齐的讯息
	选项与参数：
		-d ：后面接分隔字元。与-f 一起使用；
		-f ：依据-d 的分隔字元将一段讯息分割成为数段，用-f 取出第几段的意思；
		-c ：以字元(characters) 的单位取出固定字元区间；

	grep [-acinv] [--color=auto] '搜寻字串' filename 
	选项与参数：
		-a ：将binary 档案以text 档案的方式搜寻资料
		-c ：计算找到'搜寻字串' 的次数
		-i ：忽略大小写的不同，所以大小写视为相同
		-n ：顺便输出行号
		-v ：反向选择，亦即显示出没有'搜寻字串' 内容的那一行！
		--color=auto ：可以将找到的关键字部分加上颜色的显示喔！

## 10.6.2 排序命令： sort, wc, uniq ##

	sort [-fbMnrtuk] [file or stdin]
	选项与参数：
		-f ：忽略大小写的差异，例如A 与a 视为编码相同；
		-b ：忽略最前面的空白字元部分；
		-M ：以月份的名字来排序，例如JAN, DEC 等等的排序方法；
		-n ：使用『纯数字』进行排序(预设是以文字型态来排序的)；
		-r ：反向排序；
		-u ：就是uniq ，相同的资料中，仅出现一行代表；
		-t ：分隔符号，预设是用[tab] 键来分隔；
		-k ：以那个区间(field) 来进行排序的意思

	uniq [-ic]
	选项与参数：
		-i ：忽略大小写字元的不同；
		-c ：进行计数

	wc [-lwm]
	选项与参数：
		-l ：仅列出行；
		-w ：仅列出多少字(英文单字)；
		-m ：多少字元；

## 10.6.3 双向重导向： tee ##

tee 的工作流程示意图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/0320bash_3.png)

tee 会同时将资料流分送到档案去与萤幕(screen)；而输出到萤幕的，其实就是stdout ，那就可以让下个指令继续处理喔！
	
	tee [-a] file
	选项与参数：
		-a ：以累加(append) 的方式，将资料加入file 当中！


## 10.6.4 字元转换命令： tr, col, join, paste, expand ##

	tr [-ds] SET1 ...  # tr 可以用来删除一段讯息当中的文字，或者是进行文字讯息的替换！
	选项与参数：
		-d ：删除讯息当中的SET1 这个字串；
		-s ：取代掉重复的字元！

	col [-xb]
	选项与参数：
		-x ：将tab 键转换成对等的空白键

	join [-ti12] file1 file2  # 主要是在处理『两个档案当中，有"相同资料" 的那一行，才将他加在一起』
	选项与参数：
		-t ：join 预设以空白字元分隔资料，并且比对『第一个栏位』的资料，
		      如果两个档案相同，则将两笔资料联成一行，且第一个栏位放在第一个！
		-i ：忽略大小写的差异；
		-1 ：这个是数字的1 ，代表『第一个档案要用那个栏位来分析』的意思；
		-2 ：代表『第二个档案要用那个栏位来分析』的意思。


	需要特别注意的是，在使用join之前，你所需要处理的档案应该要事先经过排序(sort)处理！否则有些比对的项目会被略过呢！

	paste [-d] file1 file2		# 直接『将两行贴在一起，且中间以[tab]键隔开』而已！
	选项与参数：
		-d ：后面可以接分隔字元。预设是以[tab] 来分隔的！
		- ：如果file 部分写成- ，表示来自standard input 的资料的意思。

	expand [-t] file    # unexpand 这个将空白转成[tab] 
	选项与参数：
		-t ：后面可以接数字。一般来说，一个tab 按键可以用8 个空白键取代。
      		我们也可以自行定义一个[tab] 按键代表多少个字元呢！

## 10.6.5 分割命令： split ##

	split [-bl] file PREFIX 
	选项与参数：
		-b ：后面可接欲分割成的档案大小，可加单位，例如b, k, m 等；
		-l ：以行数来进行分割。
		PREFIX ：代表前置字元的意思，可作为分割档案的前导文字。

## 10.6.6 参数代换： xargs ##

这个指令就是在产生某个指令的参数的意思！ xargs可以读入stdin的资料，并且以`空白字元或断行字元`作为分辨，将stdin的资料分隔成为arguments 。	

	xargs [-0epn] command
	选项与参数：
		-0 ：如果输入的stdin 含有特殊字元，例如`, \, 空白键等等字元时，这个-0 参数
		      可以将他还原成一般字元。这个参数可以用于特殊状态喔！
		-e ：这个是EOF (end of file) 的意思。后面可以接一个字串，当xargs 分析到这个字串时，
		      就会停止继续工作！
		-p ：在执行每个指令的argument 时，都会询问使用者的意思；
		-n ：后面接次数，每次command 指令执行时，要使用几个参数的意思。
		当xargs 后面没有接任何的指令时，预设是以echo 来进行输出喔！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/10/xargs.jpg)

会使用xargs的原因是， 很多指令其实并不支援管线命令，因此我们可以透过xargs来提供该指令引用standard input之用！

	find /usr/sbin -perm /7000 | xargs ls -l
	等效于：
	ls -l $(find /usr/sbin -perm /7000)

## 10.6.7 关于减号- 的用途 ##

在管线命令当中，常常会使用到前一个指令的stdout 作为这次的stdin ， `某些指令需要用到档案名称(例如tar) 来进行处理时`，该stdin 与stdout 可以利用减号"-" 来替代。例如：
	
	mkdir /tmp/homeback 
	tar -cvf - /home | tar -xvf - -C /tmp/homeback

上面这个例子是说：将/home 里面的档案给他打包，但打包的资料不是纪录到档案，而是传送到stdout； 经过管线后，将tar -cvf - /home 传送给后面的tar - xvf - 。

----------	
	
	本章介绍的管线命令主要有：cut, grep, sort, wc, uniq, tee, tr, col, join, paste, expand, split, xargs 等。

	在这样的练习中『A=B』且『B=C』，若我下达『unset $A』，则取消的变数是A 还是B？
		被取消的是B 喔，因为unset $A 相当于unset B 所以取消的是B ，A 会继续存在！
	