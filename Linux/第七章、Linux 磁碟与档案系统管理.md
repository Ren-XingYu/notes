# 7.1 认识Linux 档案系统 #

## 7.1.1 磁碟组成与分割的复习 ##

	早期的分割主要以磁柱为最小分割单位，现在的分割通常使用磁区为最小分割单位(每个磁区都有其号码喔，就好像座位一样)。

	/dev/sd[ap][1-128]：为实体磁碟的磁碟档名；
	/dev/vd[ad][1-128]：为虚拟磁碟的磁碟档名；
	/dev/VGNAME/LVNAME：使用的是LVM

## 7.1.2 档案系统特性 ##

为什么需要进行『格式化』呢？这是因为每种作业系统所设定的档案属性/权限并不相同， 为了存放这些档案所需的资料，因此就需要将分割槽进行格式化，以成为作业系统能够利用的『档案系统格式(filesystem )』。

	Ext2 (Linux second extended file system , ext2fs )	

传统的磁碟与档案系统之应用中，一个分割槽就是只能够被格式化成为一个档案系统，所以我们可以说一个filesystem就是一个partition。但是由于新技术的利用，例如我们常听到的LVM与软体磁碟阵列(software raid)，这些技术可以将一个分割槽格式化为多个档案系统(例如LVM)，也能够将多个分割槽合成一个档案系统(LVM, RAID)！所以说，目前我们在格式化时已经不再说成针对partition来格式化了，通常我们可以称呼`一个可被挂载的资料为一个档案系统而不是一个分割槽`喔！

档案系统通常会将这两部份的资料分别存放在不同的区块，权限与属性放置到inode中，至于实际资料则放置到data block区块中。另外，还有一个超级区块(superblock)会记录整个档案系统的整体资讯，包括inode与block的总量、使用量、剩余量等。

	superblock：记录此filesystem 的整体资讯，包括inode/block的总量、使用量、剩余量， 以及档案系统的格式与相关资讯等；
	inode：记录档案的属性，一个档案占用一个inode，同时记录此档案的资料所在的block 号码；
	block：实际记录档案的内容，若档案太大时，会占用多个block 。

inode/block 资料存取示意图：`索引式档案系统(indexed allocation)`    
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/filesystem-1.jpg)

FAT这种格式的档案系统并没有inode存在，所以FAT没有办法将这个档案的所有block在一开始就读取出来。每个block号码都记录在前一个block当中，他的读取方式有点像底下这样：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/filesystem-2.jpg)

需要磁碟重组的原因就是档案写入的block太过于离散了，此时档案读取的效能将会变的很差所致。 这个时候可以透过磁碟重组将同一个档案所属的blocks汇整在一起，这样资料的读取会比较容易啊！ 想当然尔，FAT的档案系统需要三不五时的磁碟重组一下，Ext2不需要。

## 7.1.3 Linux 的EXT2 档案系统(inode) ##

档案系统一开始就将inode与block规划好了，除非重新格式化(或者利用resize2fs等指令变更档案系统大小)，否则inode与block固定后就不再变动。Ext2 档案系统在格式化的时候基本上是区分为多个区块群组(block group) 的，每个区块群组都有独立的 inode/block/superblock 系统。

ext2档案系统示意图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/ext2_filesystem.jpg)

档案系统最前面有一个开机磁区(boot sector)，这个开机磁区可以安装开机管理程式。

- data block (资料区块)  
在Ext2档案系统中所支援的block大小有`1K, 2K及4K`三种而已。

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/block-size.jpg)

		原则上，block 的大小与数量在格式化完就不能够再改变了(除非重新格式化)；
		每个block 内最多只能够放置一个档案的资料；
		承上，如果档案大于block 的大小，则一个档案会占用多个block 数量；
		承上，若档案小于block ，则该block 的剩余容量就不能够再被使用了(磁碟空间会浪费)。

- inode table (inode 表格)  

		该档案的存取模式(read/write/excute)；
		该档案的拥有者与群组(owner/group)；
		该档案的容量；
		该档案建立或状态改变的时间(ctime)；
		最近一次的读取时间(atime)；
		最近修改的时间(mtime)；
		定义档案特性的旗标(flag)，如SetUID...；
		该档案真正内容的指向(pointer)；

		每个inode 大小均固定为128 bytes (新的ext4 与xfs 可设定到256 bytes)；
		每个档案都仅会占用一个inode 而已；
		承上，因此档案系统能够建立的档案数量与inode 的数量有关；
		系统读取档案时需要先找到inode，并分析inode 所记录的权限与使用者是否符合，若符合才能够开始实际读取 block 的内容。

		inode 记录一个block 号码要花掉4byte

		inode 记录block 号码的区域定义为12个直接，一个间接, 一个双间接与一个三间接记录区

	inode 结构示意图: 

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/inode.jpg)

	以1K block来说明：

		12个直接指向： 12*1K=12K 
		由于是直接指向，所以总共可记录12笔记录，因此总额大小为如上所示；
		
		间接： 256*1K=256K 
		每笔block号码的记录会花去4bytes，因此1K的大小能够记录256笔记录，因此一个间接可以记录的档案大小如上； 
		
		双间接： 256*256*1K=256 2 K 
		第一层block会指定256个第二层，每个第二层可以指定256个号码，因此总额大小如上；
		
		三间接： 256*256*256*1K=256 3 K 
		第一层block会指定256个第二层，每个第二层可以指定256个第三层，每个第三层可以指定256个号码，因此总额大小如上；
		
		总额：将直接、间接、双间接、三间接加总，得到12 + 256 + 256*256 + 256*256*256 (K) = 16GB 

	这个方法不能用在2K及4K block大小的计算中，因为大于2K的block将会受到Ext2档案系统本身的限制，所以计算的结果会不太符合之故。

	
- Superblock (超级区块)

		block 与inode 的总量；
		未使用与已使用的inode / block 数量；
		block 与inode 的大小(block 为1, 2, 4K，inode 为128bytes 或256bytes)；
		filesystem 的挂载时间、最近一次写入资料的时间、最近一次检验磁碟(fsck) 的时间等档案系统的相关资讯；
		一个valid bit 数值，若此档案系统已被挂载，则valid bit 为0 ，若未被挂载，则valid bit 为1 。

	superblock的大小为`1024bytes`

	此外，每个block group 都可能含有superblock 喔！但是我们也说一个档案系统应该仅有一个superblock 而已，那是怎么回事啊？事实上除了第一个block group 内会含有superblock 之外，后续的block group 不一定含有superblock ， 而若含有superblock 则该superblock 主要是做为第一个block group 内superblock 的备份咯，这样可以进行superblock的救援呢！
	
- Filesystem Description (档案系统描述说明)

	这个区段可以描述每个block group的开始与结束的block号码，以及说明每个区段(superblock, bitmap, inodemap, data block)分别介于哪一个block号码之间。这部份也能够用dumpe2fs来观察的。

- block bitmap (区块对照表)

	block bitmap记录哪些block是空的，哪些正在被使用

- inode bitmap (inode 对照表)	

	inode bitmap记录哪些inode是空的，哪些正在被使用

- dumpe2fs： 查询Ext 家族superblock 资讯的指令	
	
		dumpe2fs [-bh]装置档名
		选项与参数：
			-b ：列出保留为坏轨的部分(一般用不到吧！？)
			-h ：仅列出superblock 的资料，不会列出其他的区段内容！

## 7.1.4 与目录树的关系 ##

- 目录

	当我们在Linux下的档案系统建立一个目录时，档案系统会分配一个inode与至少一块block给该目录。其中，inode记录该目录的相关权限与属性，并可记录分配到的那块block号码；而block则是记录在这个目录下的档名与该档名占用的inode号码资料。也就是说目录所占用的block内容在记录如下的资讯：

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/centos7_dir_block.jpg)

	block大小为4K ，因此每个目录几乎都是4K的倍数。

	inode本身并不记录档名，档名的记录是在目录的block当中。

	例如dmtsai要读取passwd

     	2 dr-xr-xr-x. 19 root root 4096 Oct  3 11:09 /
		262147 drwxr-xr-x. 85 root root 4096 Oct  2 13:56 /etc
		287350 -rw-r--r--   1 root root 1141 Oct  1 10:07 /etc/passwd

		1、/的inode：
		透过挂载点的资讯找到inode号码为2的根目录inode，且inode规范的权限让我们可以读取该block的内容(有r与x) ；
		
		2、/的block：
		经过上个步骤取得block的号码，并找到该内容有etc/目录的inode号码(262147)； 
		
		3、etc/的inode：
		读取262147号inode得知dmtsai具有r与x的权限，因此可以读取etc/的block内容； 
		
		4、etc/的block：
		经过上个步骤取得block号码，并找到该内容有passwd档案的inode号码(287350)； 
		
		5、passwd的inode：
		读取287350号inode得知dmtsai具有r的权限，因此可以读取passwd的block内容； 
		
		6、passwd的block：
		最后将该block内容的资料读出来。

## 7.1.5 EXT2/EXT3/EXT4 档案的存取与日志式档案系统的功能 ##

新建一个档案或目录时流程：

	1、先确定使用者对于欲新增档案的目录是否具有w 与x 的权限，若有的话才能新增；
	2、根据inode bitmap 找到没有使用的inode 号码，并将新档案的权限/属性写入；
	3、根据block bitmap 找到没有使用中的block 号码，并将实际的资料写入block 中，且更新inode 的block 指向资料；
	4、将刚刚写入的inode 与block 资料同步更新inode bitmap 与block bitmap，并更新superblock 的内容。

一般来说，我们将inode table与data block称为资料存放区域，至于其他例如superblock、 block bitmap与inode bitmap等区段就被称为metadata (中介资料)啰，因为superblock, inode bitmap及block bitmap的资料是经常变动的，每次新增、移除、编辑时都可能会影响到这三个部分的资料，因此才被称为中介资料的啦。

日志式档案系统(Journaling filesystem)流程：
  
	1、预备：当系统要写入一个档案时，会先在日志记录区块中纪录某个档案准备要写入的资讯；
	2、实际写入：开始写入档案的权限与资料；开始更新metadata 的资料；
	3、结束：完成资料与metadata 的更新后，在日志记录区块当中完成该档案的纪录。

## 7.1.6 Linux 档案系统的运作 ##

当系统载入一个档案到记忆体后，如果该档案没有被更动过，则在记忆体区段的档案资料会被设定为干净(clean)的。 但如果记忆体中的档案资料被更改过了(例如你用nano去编辑过这个档案)，此时该记忆体中的资料会被设定为脏的(Dirty)。此时所有的动作都还在记忆体中执行，并没有写入到磁碟中！系统会不定时的将记忆体中设定为『Dirty』的资料写回磁碟，以保持磁碟与记忆体资料的一致性。

	系统会将常用的​​档案资料放置到主记忆体的缓冲区，以加速档案系统的读/写；
	承上，因此Linux 的实体记忆体最后都会被用光！这是正常的情况！可加速系统效能；
	你可以手动使用sync 来强迫记忆体中设定为Dirty 的档案回写到磁碟中；
	若正常关机时，关机指令会主动呼叫sync 来将记忆体的资料回写入磁碟内；
	但若不正常关机(如跳电、当机或其他不明原因)，由于资料尚未回写到磁碟内， 因此重新开机后可能会花很多时间在进行磁碟检验，甚至可能导致档案系统的损毁(非磁碟损毁)

## 7.1.7 挂载点的意义(mount point) ##

每个filesystem都有独立的inode / block / superblock等资讯，这个档案系统要能够连结到目录树才能被我们使用。将档案系统与目录树结合的动作我们称为『挂载』。挂载点一定是目录，该目录为进入该档案系统的入口。

/ /boot /home为三个分区，每个分区为一个filesystem，每个分区最顶层目录之inode号是一样的，如下图所示:  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/mount-partition.jpg)

同一个filesystem的某个inode只会对应到一个档案内容而已(因为一个档案占用一个inode之故)。如下图所示：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/root-partition.jpg)

上面的资讯中由于挂载点均为/ ，因此三个档案(/, /., /..) 均在同一个filesystem 内，而这三个档案的inode 号码均为64 号，因此这三个档名都指向同一个inode 号码，当然这三个档案的内容也就完全一模一样了！也就是说，根目录的上层(/..) 就是他自己！

## 7.1.8 其他Linux 支援的档案系统与VFS ##

想要知道你的Linux 支援的档案系统有哪些，可以察看底下这个目录：  

	ls -l /lib/modules/$(uname -r)/kernel/fs

系统目前已载入到记忆体中支援的档案系统则有：  

	cat /proc/filesystems

整个Linux 的系统都是透过一个名为Virtual Filesystem Switch 的核心功能去读取filesystem 的。  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/centos7_vfs.gif)

## 7.1.9 XFS 档案系统简介 ##

xfs 档案系统在资料的分布上，主要规划为三个部份，一个资料区(data section)、一个档案系统活动登录区(log section)以及一个即时运作区(realtime section)。

- 资料区(data section)  

	资料区就跟我们之前谈到的ext 家族一样，包括inode/data block/superblock 等资料，都放置在这个区块。这个资料区与ext 家族的block group 类似，也是分为多个储存区群组(allocation groups) 来分别放置档案系统所需要的资料。

	另外，与ext 家族不同的是， xfs 的block 与inode 有多种不同的容量可供设定，block 容量可由512bytes ~ 64K 调配，不过，Linux 的环境下， 由于记忆体控制的关系(分页档pagesize 的容量之故)，因此最高可以使用的block 大小为4K 而已！(鸟哥尝试格式化block 成为16K 是没问题的，不过，Linux 核心不给挂载！ 所以格式化完成后也无法使用啦！) 至于inode 容量可由256bytes 到2M 这么大！不过，大概还是保留256bytes 的预设值就很够用了！

- 档案系统活动登录区(log section)

	在登录区这个区域主要被用来纪录档案系统的变化，其实有点像是日志区啦！

- 即时运作区(realtime section)

	当有档案要被建立时，xfs 会在这个区段里面找一个到数个的extent 区块，将档案放置在这个区块内，等到分配完毕后，再写入到data section 的inode 与block 去！这个extent 区块的大小得要在格式化的时候就先指定，最小值是4K 最大可到1G。一般非磁碟阵列的磁碟预设为64K 容量，而具有类似磁碟阵列的stripe 情况下，则建议extent 设定为与stripe 一样大较佳。这个extent 最好不要乱动，因为可能会影响到实体磁碟的效能喔。

XFS 档案系统的描述资料观察

	xfs_info挂载点|装置档名

# 7.2 档案系统的简单操作 #

## 7.2.1 磁碟与目录的容量 ##

	df：列出档案系统的整体磁碟使用量；
	du：评估档案系统的磁碟使用量(常用在推估目录所占容量)

	df [-ahikHTm] [目录或档名]
	选项与参数：
		-a ：列出所有的档案系统，包括系统特有的/proc 等档案系统；
		-k ：以KBytes 的容量显示各档案系统；
		-m ：以MBytes 的容量显示各档案系统；
		-h ：以人们较易阅读的GBytes, MBytes, KBytes 等格式自行显示；
		-H ：以M=1000K 取代M=1024K 的进位方式；
		-T ：连同该partition 的filesystem 名称(例如xfs) 也列出；
		-i ：不用磁碟容量，而以inode 的数量来显示

	/dev/shm/目录，其实是利用记忆体虚拟出来的磁碟空间，通常是总实体记忆体的一半！ 由于是透过
	记忆体模拟出来的磁碟，因此你在这个目录底下建立任何资料档案时，存取速度是非常快速的！

	du [-ahskm]档案或目录名称
	选项与参数：
		-a ：列出所有的档案与目录容量，因为预设仅统计目录底下的档案量而已。
		-h ：以人们较易读的容量格式(G/M) 显示；
		-s ：列出总量而已，而不列出每个各别的目录占用容量；
		-S ：不包括子目录下的总计，与-s 有点差别。
		-k ：以KBytes 列出容量显示；
		-m ：以MBytes 列出容量显示；

	# 直接输入du没有加任何选项时，则du会分析『目前所在目录』
	# 的档案与目录所占用的磁碟空间。但是，实际显示时，仅会显示目录容量(不含档案)，
	# 因此. 目录有很多档案没有被列出来，所以全部的目录相加不会等于. 的容量喔！
	# 此外，输出的数值资料为1K 大小的容量单位。

## 7.2.2 实体连结与符号连结： ln ##

- Hard Link (实体连结, 硬式连结或实际连结)  
hard link只是在某个目录下新增一笔档名连结到某inode号码的关连记录而已。

	实体连结的档案读取示意图:  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/hard_link1.gif)


		hard link 是有限制的:
			不能跨Filesystem；
			不能link 目录。
	
		/some/aapdir/aadir/
		/some/aapdir/aadir/.
		/some/aapdir/aadir/..
		/some/bbpdir/bbdir/
		/some/bbpdir/bbdir/.
		/some/bbpdir/bbdir/..

		你在aadir 看到的资料应该是要跟bbdir 看到的一样，但是aadir 的.. 会是aapdir ，不过
		bbdir 的.. 却变成bbpdir， 明明是互为连结的hard link 目录，里面的.. 却指向不同的地方
		～这就伤脑筋了！为了避免许多这方面的困扰，所以才建议不要使用hard link 在目录上的！


- Symbolic Link (符号连结，亦即是捷径)  
Symbolic link就是在建立一个独立的档案，而这个档案会让资料的读取指向他link的那个档案的档名！由于只是利用档案来做为指向的动作，所以，当来源档被删除之后，symbolic link的档案会『开不了』，会一直说『无法开启某档案！』。实际上就是找不到原始『档名』而已啦！

	符号连结的档案读取示意图:  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/symbolic_link1.gif)

	这个Symbolic Link与Windows的捷径可以给他划上等号，由Symbolic link所建立的档案为一个独立的新的档案，所以会占用掉inode与block喔！

要制作连结档就必须要使用 ln 这个指令：

	ln [-sf]来源档目标档
	选项与参数：
		-s ：如果不加任何参数就进行连结，那就是hard link，至于-s就是symbolic link 
		-f ：如果目标档存在时，就主动的将目标档直接移除后再建立！

- 关于目录的link 数量：  
	当我们建立一个新的目录时， 『新的目录的link数为2 ，而上层目录的link数则会增加1』


# 7.3 磁碟的分割、格式化、检验与挂载 #

## 7.3.1 观察磁碟分割状态 ##

	lsblk [-dfimpt] [device]   list block device
	选项与参数：
		-d ：仅列出磁碟本身，并不会列出该磁碟的分割资料
		-f ：同时列出该磁碟内的档案系统名称
		-i ：使用ASCII 的线段输出，不要使用复杂的编码(再某些环境下很有用)
		-m ：同时输出该装置在/dev 底下的权限资料(rwx 的资料)
		-p ：列出该装置的完整档名！而不是仅列出最后的名字而已。
		-t ：列出该磁碟装置的详细资料，包括磁碟伫列机制、预读写的资料量大小等

	UUID：universally unique identifier

	blkid：列出装置的UUID 等参数

	parted device_name print		列出磁碟的分割表类型与分割资讯
		parted /dev/vda print

## 7.3.2 磁碟分割： gdisk/fdisk ##

MBR分割表请使用fdisk分割， GPT分割表请使用gdisk分割！

	gdisk 装置名称
		gdisk /dev/vda

	使用的『装置档名』请不要加上数字，因为partition是针对『整个磁碟装置』而不是某个partition呢！
	
	更新Linux 核心的分割表资讯
	partprobe [-s]   #你可以不要加-s ！那么萤幕不会出现讯息！	

	
## 7.3.3 磁碟格式化(建置档案系统) ##

- XFS 档案系统mkfs.xfs	

		mkfs.xfs [-b bsize] [-d parms] [-i parms] [-l parms] [-L label] [-f] \
                         [-r parms]装置名称

		选项与参数：
		关于单位：底下只要谈到『数值』时，没有加单位则为bytes 值，可以用k,m,g,t,p (小写)等来解释
		          比较特殊的是s 这个单位，它指的是sector 的『个数』喔！
			-b ：后面接的是block 容量，可由512 到64k，不过最大容量限制为Linux 的4k 喔！
			-d ：后面接的是重要的data section 的相关参数值，主要的值有：
			      agcount=数值：设定需要几个储存群组的意思(AG)，通常与CPU 有关
			      agsize=数值：每个AG 设定为多少容量的意思，通常agcount/agsize 只选一个设定即可
			      file ：指的是『格式化的装置是个档案而不是个装置』的意思！(例如虚拟磁碟)
			      size=数值：data section 的容量，亦即你可以不将全部的装置容量用完的意思
			      su=数值：当有RAID 时，那个stripe 数值的意思，与底下的sw 搭配使用
			      sw=数值：当有RAID 时，用于储存资料的磁碟数量(须扣除备份碟与备用碟)
			      sunit=数值：与su 相当，不过单位使用的是『几个sector(512bytes大小)』的意思
			      swidth=数值：就是su*sw 的数值，但是以『几个sector(512bytes大小)』来设定
			-f ：如果装置内已经有档案系统，则需要使用这个-f 来强制格式化才行！
			-i ：与inode 有较相关的设定，主要的设定值有：
			      size=数值：最小是256bytes 最大是2k，一般保留256 就足够使用了！
			      internal=[0|1]：log 装置是否为内建？预设为1 内建，如果要用外部装置，使用底下设定
			      logdev=device ：log 装置为后面接的那个装置上头的意思，需设定internal=0 才可！
			      size=数值：指定这块登录区的容量，通常最小得要有512 个block，大约2M 以上才行！
			-L ：后面接这个档案系统的标头名称Label name 的意思！
			-r ：指定realtime section 的相关设定值，常见的有：
			      extsize=数值：就是那个重要的extent 数值，一般不须设定，但有RAID 时，
			                      最好设定与swidth 的数值相同较佳！最小为4K 最大为1G 。


- EXT4 档案系统mkfs.ext4

		mkfs.ext4 [-b size] [-L label]装置名称
		选项与参数：
			-b ：设定block 的大小，有1K, 2K, 4K 的容量，
			-L ：后面接这个装置的标头名称。
		
		ext4大部分的预设值写入于系统的/etc/mke2fs.conf 这个档案中

		mkfs -t xxx 装置名称

## 7.3.4 档案系统检验 ##

- xfs_repair 处理XFS 档案系统

		xfs_repair [-fnd]装置名称
		选项与参数：
			-f ：后面的装置其实是个档案而不是实体装置
			-n ：单纯检查并不修改档案系统的任何资料(检查而已)
			-d ：通常用在单人维护模式底下，针对根目录(/) 进行检查与修复的动作！很危险！不要随便使用

		xfs_repair 可以检查/修复档案系统，不过，因为修复档案系统是个很庞大的任务！因此，修复时该档案系统不能被挂载！

- fsck.ext4 处理EXT4 档案系统

		fsck.ext4 [-pf] [-b superblock]装置名称
		选项与参数：
			-p ：当档案系统在修复时，若有需要回覆y 的动作时，自动回覆y 来继续进行修复动作。
			-f ：强制检查！一般来说，如果fsck 没有发现任何unclean 的旗标，不会主动进入
			      细部检查的，如果您想要强制fsck 进入细部检查，就得加上-f 旗标啰！
			-D ：针对档案系统下的目录进行最佳化配置。
			-b ：后面接superblock 的位置！一般来说这个选项用不到。但是如果你的superblock 因故损毁时，
			      透过这个参数即可利用档案系统内备份的superblock 来尝试救援。一般来说，superblock 备份在：
			      1K block 放在8193, 2K block 放在16384, 4K block 放在32768

	执行xfs_repair/fsck.ext4时，被检查的partition务必不可挂载到系统上！亦即是需要在卸载的状态喔！

## 7.3.5 档案系统挂载与卸载 ##

	单一档案系统不应该被重复挂载在不同的挂载点(目录)中；
	单一目录不应该重复挂载多个档案系统；
	要作为挂载点的目录，理论上应该都是空目录才是。

如果你要用来挂载的目录里面并不是空的，那么挂载了档案系统之后，原目录下的东西就会暂时的消失。

	mount -a 
	mount [-l] 
	mount [-t档案系统] LABEL=''挂载点 
	mount [-t档案系统] UUID=''挂载点  #建议用这种方式喔！
	mount [-t档案系统]装置档名挂载点

	选项与参数：
		-a ：依照设定档/etc/fstab的资料将所有未挂载的磁碟都挂载上来
		-l ：单纯的输入mount 会显示目前挂载的资讯。加上-l 可增列Label 名称！
		-t ：可以加上档案系统种类来指定欲挂载的类型。常见的Linux 支援类型有：xfs, ext3, ext4,
		      reiserfs, vfat, iso9660(光碟格式), nfs, cifs, smbfs (后三种为网路档案系统类型)
		-n ：在预设的情况下，系统会将实际挂载的情况即时写入/etc/mtab 中，以利其他程式的运作。
		      但在某些情况下(例如单人维护模式)为了避免问题会刻意不写入。此时就得要使用-n 选项。
		-o ：后面可以接一些挂载时额外加上的参数！比方说帐号、密码、读写权限等：
		      async, sync: 此档案系统是否使用同步写入(sync) 或非同步(async) 的
		                     记忆体机制，请参考档案系统运作方式。预设为async。
		      atime,noatime: 是否修订档案的读取时间(atime)。为了效能，某些时刻可使用noatime
		      ro, rw: 挂载档案系统成为唯读(ro) 或可读写(rw)
		      auto, noauto: 允许此filesystem 被以mount -a 自动挂载(auto)
		      dev, nodev: 是否允许此filesystem 上，可建立装置档案？dev 为可允许
		      suid, nosuid: 是否允许此filesystem 含有suid/sgid 的档案格式？
		      exec, noexec: 是否允许此filesystem 上拥有可执行binary 档案？
		      user, nouser: 是否允许此filesystem 让任何使用者执行mount ？一般来说，
		                     mount 仅有root 可以进行，但下达user 参数，则可让
		                     一般user 也能够对此partition 进行mount 。
		      defaults: 预设值为：rw, suid, dev, exec, auto, nouser, and async
		      remount: 重新挂载，这在系统出错，或重新更新参数时，很有用！


系统进行挂载测试参考文档：

	/etc/filesystems：系统指定的测试挂载档案系统类型的优先顺序；
	/proc/filesystems：Linux系统已经载入的档案系统类型。

Linux支援的档案系统之驱动程式放置目录：

	/lib/modules/$(uname -r)/kernel/fs/

重新挂载根目录

	mount -o remount,rw,auto /

将某个目录挂载到另外一个目录（相当于符号链接）：

	mkdir /data/var 
	mount --bind /var /data/var 

装置档案卸载：

	umount [-fn]装置档名或挂载点
	选项与参数：
		-f ：强制卸载！可用在类似网路档案系统(NFS) 无法读取到的情况下；
		-l ：立刻卸载档案系统，比-f 还强！
		-n ：不更新/etc/mtab 情况下卸载。	

## 7.3.6 磁碟/档案系统参数修订 ##

- mknod

	Linux底下所有的装置都以档案来代表吧！但是那个档案如何代表该装置呢？很简单！就是透过档案的`major`与`minor`数值来替代的～

	常见的磁碟档名/dev/sda 与/dev/loop0 装置代码如下所示：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/mknod.jpg)

		mknod 装置档名 [bcp] [Major] [Minor]
		选项与参数：
		装置种类：
		   b ：设定装置名称成为一个周边储存设备档案，例如磁碟等；
		   c ：设定装置名称成为一个周边输入设备档案，例如滑鼠/键盘等；
		   p ：设定装置名称成为一个FIFO 档案；
		Major ：主要装置代码；
		Minor ：次要装置代码；

- xfs_admin 修改XFS 档案系统的UUID 与Label name

		xfs_admin [-lu] [-L label] [-U uuid]装置档名
		选项与参数：
			-l ：列出这个装置的label name
			-u ：列出这个装置的UUID
			-L ：设定这个装置的Label name
			-U ：设定这个装置的UUID 喔！


- tune2fs 修改ext4 的label name 与UUID

		tune2fs [-l] [-L Label] [-U uuid]装置档名
		选项与参数：
			-l ：类似dumpe2fs -h 的功能～将superblock 内的资料读出来～
			-L ：修改LABEL name
			-U ：修改UUID 啰！

# 7.4 设定开机挂载 #

## 7.4.1 开机挂载/etc/fstab 及/etc/mtab ##

	根目录/ 是必须挂载的﹐而且一定要先于其它mount point 被挂载进来。
	其它mount point 必须为已建立的目录﹐可任意指定﹐但一定要遵守必须的系统目录架构原则(FHS)
	所有mount point 在同一时间之内﹐只能挂载一次。
	所有partition 在同一时间之内﹐只能挂载一次。
	如若进行卸载﹐您必须先将工作目录移到mount point(及其子目录) 之外。

/etc/fstab文件格式：

	装置/UUID等] [挂载点] [档案系统] [档案系统参数] [dump] [fsck]
	第一栏：磁碟装置档名/UUID/LABEL name
		档案系统或磁碟的装置档名，如/dev/vda2 等
		档案系统的UUID 名称，如UUID=xxx
		档案系统的LABEL 名称，例如LABEL=xxx
	第二栏：挂载点(mount point)
	第三栏：磁碟分割槽的档案系统
	第四栏：档案系统参数
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/07/fstab-params.jpg)

	第五栏：能否被dump 备份指令作用
		dump 是一个用来做为备份的指令，不过现在有太多的备份方案了，所以这个项目可以不要理会啦！直接输入0 就好了！
	第六栏：是否以fsck 检验磁区
		早期开机的流程中，会有一段时间去检验本机的档案系统，看看档案系统是否完整(clean)。不过
		这个方式使用的主要是透过fsck 去做的，我们现在用的xfs 档案系统就没有办法适用，因为xfs
		会自己进行检验，不需要额外进行这个动作！所以直接填0 就好了。

/etc/fstab是开机时的设定档，不过，实际filesystem的挂载是记录到/etc/mtab与/proc/mounts这两个档案当中的。每次我们在更动filesystem的挂载时，也会同时更动这两个档案喔！

/etc/fstab这个档案如果写错了，则你的Linux很可能将无法顺利开机完成！

## 7.4.2 特殊装置loop 挂载(映象档不烧录就挂载使用) ##

- 挂载光碟/DVD映象档

		mount -o loop /tmp/CentOS-7.0-1406-x86_64-DVD.iso /data/centos_dvd

- 建立大档案以制作loop 装置档案！

		建立大型档案
			dd if=/dev/zero of=/srv/loopdev bs=1M count=512
		大型档案的格式化
			mkfs.xfs -f /srv/loopdev
			blkid /srv/loopdev
		挂载
			mount -o loop UUID="7dd97bd2-4446-48fd-9d23-a8b03ffdd5ee" /mnt


# 7.5 记忆体置换空间(swap)之建置 #

由于swap是用磁碟来暂时放置记忆体中的资讯，所以用到swap时，你的主机磁碟灯就会开始闪个不停啊！

## 7.5.1 使用实体分割槽建置swap ##

开始建置swap 格式

	mkswap /dev/vda6

将该swap 装置启动

	swapon /dev/vda6 

写入设定档：nano /etc/fstab

## 7.5.2 使用档案建置swap ##

	dd if=/dev/zero of=/tmp/swap bs=1M count=128
	mkswap /tmp/swap		# 这个档案格式化为swap 的档案格式
	swapon /tmp/swap		# 将/tmp/swap 启动
	nano /etc/fstab 
		/tmp/swap swap swap defaults 0 0 
	# 为何这里不要使用UUID呢？这是因为系统仅会查询区块装置(block device)不会查询档案！
	# 所以，这里千万不要使用UUID，不然系统会查不到喔！

	swapoff /tmp/swap /dev/vda6   # 使用swapoff 关掉swap file

如果你的主机支援电源管理模式， 也就是说，你的Linux 主机系统可以进入『休眠』模式的话，那么， 运作当中的程式状态则会被纪录到swap 去，以作为『唤醒』主机的状态依据！

# 7.6 档案系统的特殊观察与操作 #

## 7.6.1 磁碟空间之浪费问题 ##

当使用ls -l 去查询某个目录下的资料时，第一行都会出现一个『total』的字样！那是啥东西？其实那就是该目录下的所有资料所耗用的实际block 数量* block 大小的值。

使用ls -l去查询时，目录的容量为block大小的整数倍，文件的大小为实际大小。

## 7.6.2 利用GNU 的parted 进行分割行为(Optional) ##

	parted [装置] [指令[参数]]
	选项与参数：
	指令功能：
          新增分割：mkpart [primary|logical|extended] [ext4|vfat|xfs] 开始结束
          显示分割：print
          删除分割：rm [partition]

	以使用『 man parted 』，或者是『 parted /dev/vda help mkpart 』去查询更详细的资料。

	partition 可以直接进行分区而不需要跟用户互动，多用在shell script中

开机自动挂载可参考`/etc/fstab`之设定，设定完毕务必使用mount -a 测试语法正确否。因为/etc/fstab文件里面如果写错，会导致无法开机。







