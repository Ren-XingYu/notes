# 第十四章、磁碟配额(Quota)与进阶档案系统管理 #

## 14.1 磁碟配额(Quota) 的应用与实作 ##

### 14.1.1 什么是Quota ###

- Quota的一般用途

	针对网路服务：

		针对WWW server ，例如：每个人的网页空间的容量限制！
		针对mail server，例如：每个人的邮件空间限制。
		针对file server，例如：每个人最大的可用网路硬碟空间(教学环境中最常见！)	

	针对Linux 系统主机：

		限制某一群组所能使用的最大磁碟配额(使用群组限制)
		限制某一使用者的最大磁碟配额(使用使用者限制)
		限制某一目录(directory, project)的最大磁碟配额

- Quota 的使用限制

		在EXT档案系统家族仅能针对整个filesystem：
		核心必须支援quota
		只对一般身份使用者有效（root就不能设定quota ，因为整个系统所有的资料几乎都是他的啊！）
		若启用SELinux，非所有目录均可设定quota（启用SELinux时，此预设的情况下， quota似乎仅能针对/home进行设定而已）

- Quota 的规范设定项目

		分别针对使用者、群组或个别目录(user, group & project)
		容量限制或档案数量限制(block 或inode)
			限制inode 用量：可以管理使用者可以建立的『档案数量』
			限制block 用量：管理使用者磁碟容量的限制，较常见为这种方式
		柔性劝导与硬性规定(soft/hard)
			hard：表示使用者的用量绝对不会超过这个限制值，以上面的设定为例， 
			使用者所能使用的磁碟容量绝对不会超过500Mbytes ，若超过这个值则系统会锁住该用户的磁碟使用权
			soft：表示使用者在低于soft 限值时(此例中为400Mbytes)，可以正常使用磁碟，
			但若超过soft 且低于hard 的限值(介于400~500Mbytes 之间时)，每次使用者登入系统时，系统会主动发出磁碟即将爆满的警告讯息， 
			且会给予一个宽限时间(grace time)。不过，若使用者在宽限时间倒数期间就将容量再次降低于soft 限值之下， 则宽限时间会停止
		会倒数计时的宽限时间(grace time)
			一般预设的宽限时间为七天，如果七天内你都不进行任何磁碟管理，那么soft限制值会即刻取代hard限值来作为quota的限制。

	quota限定示意图：  
		![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/soft_hard.gif)

### 14.1.2 一个XFS 档案系统的Quota 实作范例 ###

	群组(group)的限制与目录(directory/project)无法同时并存喔！

### 14.1.3 实作Quota 流程-1：档案系统的支援与观察 ###

	df -hT /home     # 检查采用的文件系统
	VFAT 档案系统不支援Linux Quota 功能
	quota除了核心要支援外，还要启动档案系统的支援
	XFS 档案系统的quota 的挂载要写入/etc/fstab中（启动档案系统的支援）

	quota 限制的项目主要有三项：
		uquota/usrquota/quota：针对使用者帐号的设定
		gquota/grpquota：针对群组的设定
		pquota/prjquota：针对单一目录的设定，但是不可与grpquota 同时存在！

### 14.1.4 实作Quota 流程-2：观察Quota 报告资料 ###

	xfs_quota -x -c "指令" [挂载点]
	选项与参数：
		-x ：专家模式，后续才能够加入-c 的指令参数喔！
		-c ：后面加的就是指令，这个小节我们先来谈谈数据回报的指令
		指令：
		      print ：单纯的列出目前主机内的档案系统参数等资料
		      df ：与原本的df 一样的功能，可以加上-b (block) -i (inode) -h (加上单位) 等
		      report：列出目前的quota 项目，有-ugr (user/group/project) 及-bi 等资料
		      state ：说明目前支援quota 的档案系统的资讯，有没有起动相关项目等

	xfs_quota -x -c "print"  
	xfs_quota -x -c "df -h" /home
	xfs_quota -x -c "report -ubih" /home     # soft/hard 若为0，代表没限制
	xfs_quota -x -c "state"

### 14.1.5 实作Quota 流程-3：限制值设定方式 ###
	
	xfs_quota -x -c "limit [-ug] b[soft|hard]=N i[soft|hard]=N name"
	xfs_quota -x -c "timer [-ug] [-bir] Ndays"	
	选项与参数：
		limit ：实际限制的项目，可以针对user/group 来限制，限制的项目有
		        bsoft/bhard : block 的soft/hard 限制值，可以加单位
		        isoft/ihard : inode 的soft/hard 限制值
		        name : 就是用户/群组的名称啊！
		timer ：用来设定grace time 的项目喔，也是可以针对user/group 以及block/inode 设定	

	xfs_quota -x -c "limit -u bsoft=250M bhard=300M myquota1" /home 
	xfs_quota -x -c "limit -g bsoft=950M bhard=1G myquotagrp" /home
	xfs_quota -x -c "timer -ug -b 14days" /home

### 14.1.6 实作Quota 流程-4：project 的限制(针对目录限制) (Optional) ###

	修改/etc/fstab 内的档案系统支援参数
		要将grpquota 的参数取消，然后加入prjquota ，并且卸载/home 再重新挂载才行
	规范目录、专案名称(project)与专案ID
		目录的设定比较奇怪，他必须要指定一个所谓的『专案名称、专案识别码』来规范才行！而且还需要用到两个设定档！
		
		指定专案识别码与目录的对应在/etc/projects
			echo "11:/home/myquota" >> /etc/projects
		规范专案名称与识别码的对应在/etc/projid
			echo "myquotaproject:11" >> /etc/projid
		初始化专案名称
			xfs_quota -x -c "project -s myquotaproject"
		设定
			xfs_quota -x -c "limit -p bsoft=450M bhard=500M myquotaproject" /home 
			 xfs_quota -x -c "report -pbih " /home
	
	有了这个project之后，就能够针对不同的目录做容量限制！而不用管在里头建立档案的档案拥有者！	
		
### 14.1.7 XFS quota 的管理与额外指令对照表 ###

	disable：暂时取消quota的限制，但其实系统还是在计算quota中，只是没有管制而已！应该算最有用的功能啰！
	enable：就是回复到正常管制的状态中，与disable可以互相取消、启用！
	off：完全关闭quota的限制，使用了这个状态后，你只有卸载再重新挂载才能够再次的启动quota喔！也就是说，
	用了off状态后，你无法使用enable再次复原quota的管制喔！注意不要乱用这个状态！一般建议用disable即可，除非你需要执行remove的动作！
	remove：必须要在off的状态下才能够执行的指令～这个remove可以『移除』quota的限制设定，例如要取消project的设定，无须重新设定为0喔！只要remove -p就可以了！
	
	remove -p 是『移除所有的project 控制列表』的意思！也就是说，如果你有在/home 设定多个project 的限制， 
	那么remove 会删的一个也不留喔！如果想要回复设定值，那...只能一个一个重新设定回去了！
	
	xfs_quota -x -c "disable -up" /home 
	xfs_quota -x -c "enable -up" /home
	xfs_quota -x -c "off -up" /home
	xfs_quota -x -c "remove -p" /home

quota xfs和ext指令对照图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/quota-xfs-ext.jpg)

### 14.1.8 不更动既有系统的quota 实例 ###

	/var/spool/mail/并不是一个独立的partition，想要对它进行quota限制，解决方法如下：
		1、将/var/spool/mail 这个目录完整的移动到/home 底下；
		2、利用ln -s /home/mail /var/spool/mail 来建立连结资料；
		3、将/home 进行quota 限额设定

## 14.2 软体磁碟阵列(Software RAID) ##

### 14.2.1 什么是RAID ###

磁碟阵列全名是『 Redundant Arrays of Independent Disks, RAID 』，RAID可以透过一个技术(软体或硬体)，将多个较小的磁碟整合成为一个较大的磁碟装置。

- RAID-0 (等量模式, stripe)：效能最佳

	RAID-0示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/raid0.gif)

	举例来说，你有两颗磁碟组成RAID-0 ， 当你有100MB 的资料要写入时，每个磁碟会各被分配到50MB 的储存量。

	在组成RAID-0时，每颗磁碟(Disk A与Disk B)都会先被区隔成为小区块(chunk)。当有资料要写入RAID时，资料会先被切割成符合小区块的大小，然后再依序一个一个的放置到不同的磁碟去。

- RAID-1 (映射模式, mirror)：完整备份

	RAID-1示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/raid1.gif)

	让同一份资料，完整的保存在两颗磁碟上头。
	
	举例来说，如果我有一个100MB的档案，且我仅有两颗磁碟组成RAID-1时，那么这两颗磁碟将会同步写入100MB到他们的储存空间去。因此，整体RAID的容量几乎少了50%。

- RAID 1+0，RAID 0+1

	RAID-1+0示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/raid1%2B0.jpg)

- RAID 5：效能与资料备份的均衡考量

	RAID-5 至少需要三颗以上的磁碟才能够组成这种类型的磁碟阵列。这种磁碟阵列的资料写入有点类似RAID-0 ， 不过每个循环的写入过程中(striping)，在每颗磁碟还加入一个同位检查资料(Parity) ，这个资料会记录其他磁碟的备份资料， 用于当有磁碟损毁时的救援。

	RAID-5示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/raid5.gif)

	RAID 5的总容量会是整体磁碟数量减一颗。以上图为例，原本的3颗磁碟只会剩下(3-1)=2颗磁碟的容量。而且当损毁的磁碟数量大于等于两颗时，这整组RAID 5的资料就损毁了。因为RAID 5预设仅能支援一颗磁碟的损毁情况。

- Spare Disk：预备磁碟的功能

	为了让系统可以即时的在坏掉硬碟时主动的重建，因此就需要预备磁碟(spare disk) 的辅助。所谓的spare disk 就是一颗或多颗没有包含在原本磁碟阵列等级中的磁碟，这颗磁碟平时并不会被磁碟阵列所使用， 当磁碟阵列有任何磁碟损毁时，则这颗spare disk 会被主动的拉进磁碟阵列中，并将坏掉的那颗硬碟移出磁碟阵列！然后立即重建资料系统。如此你的系统则可以永保安康啊！若你的磁碟阵列有支援热拔插那就更完美了！直接将坏掉的那颗磁碟拔除换一颗新的，再将那颗新的设定成为spare disk ，就完成了！

- 磁碟阵列的优点

		1、资料安全与可靠性：指的并非网路资讯安全，而是当硬体(指磁碟) 损毁时，资料是否还能够安全的救援或使用之意；
		2、读写效能：例如RAID 0 可以加强读写效能，让你的系统I/O 部分得以改善；
		3、容量：可以让多颗磁碟组合起来，故单一档案系统可以有相当大的容量。

	RAID对比图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/compare-of-raid.jpg)

### 14.2.2 software, hardware RAID ###

所谓的硬体磁碟阵列(hardware RAID) 是透过磁碟阵列卡来达成阵列的目的。磁碟阵列卡上面有一块专门的晶片在处理RAID 的任务，因此在效能方面会比较好。在很多任务(例如RAID 5 的同位检查码计算) 磁碟阵列并不会重复消耗原本系统的I/O 汇流排，理论上效能会较佳。

软体磁碟阵列(software RAID)主要是透过软体来模拟阵列的任务， 因此会损耗较多的系统资源，比如说CPU 的运算与I/O 汇流排的资源等。

CentOS提供的软体磁碟阵列为`mdadm`这套软体，这套软体会以`partition`或`disk`为磁碟的单位，也就是说，你不需要两颗以上的磁碟，只要有两个以上的分割槽(partition)就能够设计你的磁碟阵列了。

	硬体磁碟阵列的装置档名为/dev/sd[ap] 
	软体磁碟阵列则是系统模拟的，因此使用的装置档名是系统的装置档，档名为/dev/md0, /dev/md1...

### 14.2.3 软体磁碟阵列的设定 ###

	mdadm --detail /dev/md0 
	mdadm --create /dev/md[0-9] --auto=yes --level=[015] --chunk =NK --raid-devices=N --spare-devices=N /dev/sdx /dev/hdx...
	选项与参数：
		--create ：为建立RAID 的选项；
		--auto=yes ：决定建立后面接的软体磁碟阵列装置，亦即/dev/md0, /dev/md1...
		--chunk=Nk ：决定这个装置的chunk 大小，也可以当成stripe 大小，一般是64K 或512K。
		--raid-devices=N ：使用几个磁碟(partition) 作为磁碟阵列的装置
		--spare-devices=N ：使用几个磁碟作为备用(spare) 装置
		--level=[015] ：设定这组磁碟阵列的等级。支援很多，不过建议只要用0, 1, 5 即可
		--detail ：后面所接的那个磁碟阵列装置的详细资讯

	mdadm --create /dev/md0 --auto=yes --level=5 --chunk=256K --raid-devices=4 --spare-devices=1 /dev/vda {5,6,7,8,9}  # 透过{} 将重复的项目简化！
	cat /proc/mdstat # 查阅系统软体磁碟阵列的情况
	mkfs.xfs -f -d su=256k,sw=3 -r extsize=768k /dev/md0 # 格式化

### 14.2.4 模拟RAID 错误的救援模式 ###
	
	mdadm --manage /dev/md[0-9] [--add装置] [--remove装置] [--fail装置]
	选项与参数：
		--add ：会将后面的装置加入到这个md 中！
		--remove ：会将后面的装置由这个md 中移除
		--fail ：会将后面的装置设定成为出错的状态	

	mdadm --manage /dev/md0 --fail /dev/vda7 
	mdadm --manage /dev/md0 --remove /dev/vda7
	mdadm --manage /dev/md0 --add /dev/vda7

### 14.2.5 开机自动启动RAID 并自动挂载 ###

Software RAID 也是有设定档的，这个设定档在`/etc/mdadm.conf `

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/boot-on-starup.jpg)

### 14.2.6 关闭软体RAID(重要！) ###

如果你只是将/dev/md0 卸载，然后忘记将RAID 关闭， 结果就是....未来你在重新分割/dev/vdaX 时可能会出现一些莫名的错误状况啦！所以才需要关闭software RAID 的步骤！

移除RAID：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/remove-raid.jpg)

需要数个dd指令时因为RAID 的相关资料其实也会存一份在磁碟当中，因此，如果你只是将设定档移除， 同时关闭了RAID，但是分割槽并没有重新规划过，那么重新开机过后，系统还是会将这颗磁碟阵列建立起来，只是名称可能会变成/dev/md127 就是了！因此，移除掉Software RAID 时，上述的dd 指令不要忘记！

## 14.3 逻辑卷轴管理员(Logical Volume Manager) ##

LVM的重点在于『可以弹性的调整filesystem的容量！』而并非在于效能与资料保全上面。需要档案的读写效能或者是资料的可靠性，请参考前面的RAID小节。 LVM可以整合多个实体partition在一起，让这些partitions看起来就像是一个磁碟一样！而且，还可以在未来新增或移除其他的实体partition到这个LVM管理的磁碟当中。如此一来，整个磁碟空间的使用上，实在是相当的具有弹性啊！

### 14.3.1 什么是LVM： PV, PE, VG, LV 的意义 ###

LVM 的全名是Logical Volume Manager，中文可以翻译作逻辑卷轴管理员。LVM 的作法是将几个实体的partitions (或disk) 透过软体组合成为一块看起来是独立的大磁碟(VG) ，然后将这块大磁碟再经过分割成为可使用分割槽(LV)，最终就能够挂载使用了。

- Physical Volume, PV, 实体卷轴

	我们实际的partition (或Disk)需要调整系统识别码(system ID)成为8e (LVM的识别码)，然后再经过pvcreate的指令将他转成LVM最底层的实体卷轴(PV) ，之后才能够将这些PV加以利用！调整system ID的方是就是透过gdisk啦！

- Volume Group, VG, 卷轴群组

	所谓的LVM 大磁碟就是将许多PV 整合成这个VG 的东西就是啦！所以VG 就是LVM 组合起来的大磁碟！这么想就好了。那么这个大磁碟最大可以到多少容量呢？这与底下要说明的PE 以及LVM 的格式版本有关喔～在预设的情况下， 使用32位元的Linux 系统时，基本上LV 最大仅能支援到65534 个PE 而已，若使用预设的PE为4MB 的情况下， 最大容量则仅能达到约256GB 而已～不过，这个问题在64位元的Linux 系统上面已经不存在了！LV 几乎没有啥容量限制了！

- Physical Extent, PE, 实体范围区块

	LVM预设使用4MB的PE区块，而LVM的LV在32位元系统上最多仅能含有65534个PE (lvm1的格式)，因此预设的LVM的LV会有4M*65534/(1024M/G )=256G。这个PE很有趣喔！他是整个LVM最小的储存区块，也就是说，其实我们的档案资料都是藉由写入PE来处理的。简单的说，这个PE就有点像档案系统里面的block大小啦。这样说应该就比较好理解了吧？所以调整PE会影响到LVM的最大容量喔！不过，在CentOS 6.x以后，由于直接使用lvm2的各项格式功能，以及系统转为64位元，因此这个限制已经不存在了。

- Logical Volume, LV, 逻辑卷轴

	最终的VG还会被切成LV，这个LV就是最后可以被格式化使用的类似分割槽的咚咚了！那么LV是否可以随意指定大小呢？当然不可以！既然PE是整个LVM的最小储存单位，那么LV的大小就与在此LV内的PE总数有关。为了方便使用者利用LVM来管理其系统，因此LV的装置档名通常指定为『 /dev/vgname/lvname』的样式！

	此外，我们刚刚有谈到LVM 可弹性的变更filesystem 的容量，那是如何办到的？其实他就是透过『交换PE 』来进行资料转换， 将原本LV 内的PE 移转到其他装置中以降低LV 容量，或将其他装置的PE 加到此LV 中以加大容量！VG、LV 与PE 的关系有点像下图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/pe_vg.gif)

	如上图所示，VG 内的PE 会分给虚线部分的LV，如果未来这个VG 要扩充的话，加上其他的PV 即可。而最重要的LV 如果要扩充的话，也是透过加入VG 内没有使用到的PE 来扩充的！

- 实作流程

	透过PV, VG, LV的规划之后，再利用mkfs 就可以将你的LV格式化成为可以利用的档案系统了！而且这个档案系统的容量在未来还能够进行扩充或减少，而且里面的资料还不会被影响！实做流程如下图所示：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/centos7_lvm.jpg)

	LV资料写入的两种方式：  
	- 线性模式(linear)：假如我将/dev/vda1, /dev/vdb1 这两个partition 加入到VG 当中，并且整个VG 只有一个LV 时，那么所谓的线性模式就是：当/dev/vda1 的容量用完之后，/dev/vdb1 的硬碟才会被使用到， 这也是我们所建议的模式。
	- 交错模式(triped)：那什么是交错模式？很简单啊，就是我将一笔资料拆成两部分，分别写入/dev/vda1 与/dev/vdb1 的意思，感觉上有点像RAID 0 啦！如此一来，一份资料用两颗硬碟来写入，理论上，读写的效能会比较好。

	基本上，LVM最主要的用处是在实现一个可以弹性调整容量的档案系统上，而不是在建立一个效能为主的磁碟上，所以，我们应该利用的是LVM可以弹性管理整个partition大小的用途上，而不是著眼在效能上的。因此， LVM预设的读写模式是线性模式啦！如果你使用triped模式，要注意，当任何一个partition 『归天』时，所有的资料都会『损毁』的！所以啦，不是很适合使用这种模式啦！如果要强调效能与备份，那么就直接使用RAID即可，不需要用到LVM啊！

### 14.3.2 LVM 实作流程 ###

LVM 必需要核心有支援且需要安装lvm2 这个软体，Linux预设已经支援该软件。partition或disk的system ID 改为`8e00`

- 0.Disk 阶段(实际的磁碟)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_disk.jpg)

- 1.PV 阶段

		pvcreate ：将实体partition 建立成为PV ；
		pvscan ：搜寻目前系统里面任何具有PV 的磁碟；
		pvdisplay ：显示出目前系统上面的PV 状态；
		pvremove ：将PV 属性移除，让该partition 不具有PV 属性。

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_pv.jpg)

- 2.VG 阶段

		vgcreate ：就是主要建立VG 的指令啦！他的参数比较多，等一下介绍。
		vgscan ：搜寻系统上面是否有VG 存在？
		vgdisplay ：显示目前系统上面的VG 状态；
		vgextend ：在VG 内增加额外的PV ；
		vgreduce ：在VG 内移除PV；
		vgchange ：设定VG 是否启动(active)；
		vgremove ：删除一个VG 啊！

	PV 的名称其实就是partition 的装置档名，与PV 不同的是， VG 的名称是自订的！

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_vg.jpg)
	
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_vg_add.jpg)

- 3.LV 阶段

		lvcreate ：建立LV 啦！
		lvscan ：查询系统上面的LV ；
		lvdisplay ：显示系统上面的LV 状态啊！
		lvextend ：在LV 里面增加容量！
		lvreduce ：在LV 里面减少容量；
		lvremove ：删除一个LV ！
		lvresize ：对LV 进行容量大小的调整！	

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_lv.jpg)

	要特别注意的是， VG的名称为vbirdvg ，但是LV的名称必须使用全名！亦即是/dev/vbirdvg/vbirdlv才对喔

- 档案系统阶段

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_fs.jpg)

### 14.3.3 放大LV 容量 ###

放大档案系统时，需要底下这些流程的：

1. VG阶段需要有剩余的容量：因为需要放大档案系统，所以需要放大LV，但是若没有多的VG容量，那么更上层的LV与档案系统就无法放大的。因此，你得要用尽各种方法来产生多的VG容量才行。一般来说，如果VG容量不足，最简单的方法就是再加硬碟！然后将该硬碟使用上面讲过的pvcreate及vgextend增加到该VG内即可！
2. LV阶段产生更多的可用容量：如果VG的剩余容量足够了，此时就可以利用lvresize这个指令来将剩余容量加入到所需要增加的LV装置内！过程相当简单！
3. 档案系统阶段的放大：我们的Linux实际使用的其实不是LV啊！而是LV这个装置内的档案系统！所以一切最终还是要以档案系统为依归！目前在Linux环境下，鸟哥测试过可以放大的档案系统有XFS以及EXT家族！至于缩小仅有EXT家族，目前XFS档案系统并不支援档案系统的容量缩小喔！要注意！要注意！XFS放大档案系统透过简单的xfs_growfs指令即可！

其中最后一个步骤最重要！我们在第七章当中知道，整个档案系统在最初格式化的时候就建立了inode/block/superblock等资讯，要改变这些资讯是很难的！不过因为档案系统格式化的时候建置的是多个block group ，因此我们可以透过在档案系统当中增加block group的方式来增减档案系统的量！而增减block group就是利用xfs_growfs啰！所以最后一步是针对档案系统来处理的，前面几步则是针对LVM的实际容量大小！

因此，严格说起来，放大档案系统并不是没有进行『格式化』喔！放大档案系统时，格式化的位置在于该装置后来新增的部份，装置的前面已经存在的档案系统则没有变化。而新增的格式化过的资料，再反馈回原本的supberblock 这样而已！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/extend_lv_1.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/extend_lv_2.jpg)

档案系统的放大可以在On-line 的环境下实作。

### 14.3.4 使用LVM thin Volume 让LVM 动态自动调整磁碟使用率 ###

什么是LVM thin Volume 呢？这东西其实挺好玩的，他的概念是：先建立一个可以实支实付、用多少容量才分配实际写入多少容量的磁碟容量储存池(thin pool)， 然后再由这个thin pool 去产生一个『指定要固定容量大小的LV 装置』，这个LV 就有趣了！虽然你会看到『宣告上，他的容量可能有10GB ，但实际上， 该装置用到多少容量时，才会从thin pool 去实际取得所需要的容量』！就如同上面的环境说的，可能我们的thin pool 仅有1GB 的容量， 但是可以分配给一个10GB 的LV 装置！而该装置实际使用到500M 时，整个thin pool 才分配500M 给该LV 的意思！当然啦！在所有由thin pool 所分配出来的LV 装置中，总实际使用量绝不能超过thin pool 的最大实际容量啊！如这个案例说的， thin pool 仅有1GB， 那所有的由这个thin pool 建置出来的LV 装置内的实际用量，就绝不能超过1GB 啊！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_thin_volume.jpg)

实际上，真的可用容量就是实际的磁碟储存池内的容量！如果突破该容量，这个thin pool 可是会爆炸而让资料损毁的！要注意！要注意！

### 14.3.5 LVM 的LV 磁碟快照 ###

快照就是将当时的系统资讯记录下来，就好像照相记录一般！未来若有任何资料更动了，则原始资料会被搬移到快照区，没有被更动的区域则由快照区与档案系统共享。

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/snapshot.gif)

左图为最初建置LV磁碟快照区的状况，LVM会预留一个区域(左图的左侧三个PE区块)作为资料存放处。此时快照区内并没有任何资料，而`快照区与系统区共享所有的PE资料，因此你会看到快照区的内容与档案系统是一模一样的`。等到系统运作一阵子后，假设A区域的资料被更动了(上面右图所示)，则更动前系统会将该区域的资料移动到快照区，所以在右图的快照区被占用了一块PE成为A，而其他B到I的区块则还是与档案系统共用！

照这样的情况来看，LVM 的磁碟快照是非常棒的『备份工具』，因为他只有备份有被更动到的资料， 档案系统内没有被变更的资料依旧保持在原本的区块内，但是LVM 快照功能会知道那些资料放置在哪里， 因此『快照』当时的档案系统就得以『备份』下来，且快照所占用的容量又非常小！所以您说，这不是很棒的工具又是什么？

那么快照区要如何建立与使用呢？首先，由于快照区与原本的LV共用很多PE区块，因此`快照区与被快照的LV必须要在同一个VG上头。`

- 传统快照区的建立

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/create_snapshot.jpg)

	您看看！这个/dev/vbirdvg/vbirdsnap1 快照区就被建立起来了！而且他的VG 量竟然与原本的/dev/vbirdvg/vbirdlv 相同！也就是说，如果你真的挂载这个装置时，看到的资料会跟原本的vbirdlv 相同喔！我们就来测试看看：

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/create_snapshot_test.jpg)

	因为XFS 不允许相同的UUID 档案系统的挂载，因此我们得要加上那个nouuid 的参数，让档案系统忽略相同的UUID 所造成的问题！没办法啊！因为快照出来的档案系统当然是会一模一样的！

- 利用快照区复原系统

	注意：你要复原的资料量不能够高于快照区所能负载的实际容量。由于原始资料会被搬移到快照区，如果你的快照区不够大，若原始资料被更动的实际资料量比快照区大，那么快照区当然容纳不了，这时候快照功能会失效喔！

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/snapshot_restore_1.jpg)

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/snapshot_restore_2.jpg)

- 利用快照区进行各项练习与测试的任务，再以原系统还原快照

	换个角度来想想，我们将原本的vbirdlv当作备份资料，然后将vbirdsnap1当作实际在运作中的资料，任何测试的动作都在vbirdsnap1这个快照区当中测试，那么当测试完毕要将测试的资料删除时，只要将快照区删去即可！而要复制一个vbirdlv的系统，再作另外一个快照区即可！

### 14.3.6 LVM 相关指令汇整与LVM 的关闭 ###

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_instruction.jpg)

	LVM移除流程：
		1、先卸载系统上面的LVM 档案系统(包括快照与所有LV)；
		2、使用lvremove 移除LV ；
		3、使用vgchange -an VGname 让VGname 这个VG 不具有Active 的标志；
		4、使用vgremove 移除VG：
		5、使用pvremove 移除PV；
		6、最后，使用fdisk 修改ID 回来啊！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/lvm_remove.jpg)

## 习题：在RAID上面建置LVM ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/task_step_1.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/task_step_2.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/task_step_3.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/14/task_step_4.jpg)











