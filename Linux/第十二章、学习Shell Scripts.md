# 12.1 什么是Shell scripts #

shell script是利用shell的功能所写的一个『程式(program)』，这个程式是使用纯文字档，将一些shell的语法与指令(含外部指令)写在里面，搭配正规表示法、管线命令与资料流重导向等功能，以达到我们所想要的处理目的。

## 12.1.1 干嘛学习shell scripts ##

shell script用在系统管理上面是很好的一项工具，但是用在处理大量数值运算上，就不够好了，因为Shell scripts的速度较慢，且使用的CPU资源较多，造成主机资源的分配不良。

## 12.1.2 第一支script 的撰写与执行 ##

shell script撰写注意事项：

	指令的执行是从上而下、从左而右的分析与执行；
	指令的下达就如同第四章内提到的：指令、选项与参数间的多个空白都会被忽略掉；
	空白行也将被忽略掉，并且[tab] 按键所推开的空白同样视为空白键；
	如果读取到一个Enter 符号(CR) ，就尝试开始执行该行(或该串) 命令；
	至于如果一行的内容太多，则可以使用『 \[Enter] 』来延伸至下一行；
	『 # 』可做为注解！任何加在# 后面的资料将全部被视为注解文字而被忽略！

shell script执行方式：以/home/dmtsai/shell.sh为例

	直接指令下达： shell.sh 档案必须要具备可读与可执行(rx) 的权限，然后：
		绝对路径：使用/home/dmtsai/shell.sh 来下达指令；
		相对路径：假设工作目录在/home/dmtsai/ ，则使用./shell.sh 来执行
		变数『PATH』功能：将shell.sh 放在PATH 指定的目录内，例如： ~/bin/

	以bash 程式来执行：透过『 bash shell.sh 』或『 sh shell.sh 』来执行
		/bin/sh其实就是/bin/bash (连结档)
		使用bash来执行script的时候，shell.sh只需要r 权限即可被执行。

shell script的第一行`#!/bin/bash`在宣告这个script使用的shell名称。当这个程式被执行时，他就能够载入bash的相关环境设定档(一般来说就是non-login shell的~/.bashrc )，并且执行bash来使我们底下的指令能够执行！

## 12.1.3 撰写shell script 的良好习惯建立 ##

在每个script 的档头处记录好：

	script 的功能；
	script 的版本资讯；
	script 的作者与联络方式；
	script 的版权宣告方式；
	script 的History (历史纪录)；
	script 内较特殊的指令，使用『绝对路径』的方式来下达；
	script 运作时需要的环境变数预先宣告与设定。

# 12.2 简单的shell script 练习 #

## 12.2.1 简单范例 ##

数值计算的两种格式（bash预设仅支持到整数）
	
	var=$((${firstnu}*${secnu}))
	declare -i total=${firstnu}*${secnu}

计算PI：

	time echo "scale=${num}; 4*a(1)" | bc -lq

## 12.2.2 script 的执行方式差异(source, sh script, ./script) ##

脚本的执行方式除了直接下达外还有直接下达和使用bash外，还可以利用`source 或小数点(.)`来执行。

- 利用直接执行的方式来执行script  

		直接指令下达(不论是绝对路径/相对路径还是${PATH}内)，或者是利用bash (或sh)来下达脚本
		时，该script都会使用一个新的bash环境来执行脚本内的指令！也就是说，使用这种执行方式
		时，其实script是在子程序的bash内执行的！当子程序完成后，在子程序内的各项变数或动作将会结束而不会传回到父程序中。

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/centos7_non-source.gif)
	
- 利用source 来执行脚本：在父程序中执行

		脚本会在父程序中执行的，因此各项动作都会在原本的bash 内生效！这也是为啥你不登出系统而
		要让某些写入~/.bashrc 的设定生效时，需要使用『 source ~/.bashrc 』而不能使用『 bash ~/.bashrc 』是一样的啊！

	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/centos7_source.gif)

# 12.3 善用判断式 #

## 12.3.1 利用test 指令的测试功能 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/test.jpg)

## 12.3.2 利用判断符号[ ] ##

使用中括号必须要特别注意，因为中括号用在很多地方，包括万用字元与正规表示法等等，所以如果要在bash的语法当中使用中括号作为shell的判断式时，必须要注意中括号的两端需要有空白字元来分隔喔！假设我空白键使用『□』符号来表示，那么，在这些地方你都需要有空白键：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/medium-syntax.jpg)

	在中括号[] 内的每个元件都需要有空白键来分隔；
	在中括号内的变数，最好都以双引号括号起来；
	在中括号内的常数，最好都以单或双引号括号起来。

## 12.3.3 Shell script 的预设变数($0, $1...) ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/script-buildin-var.jpg)

	$# ：代表后接的参数『个数』，以上表为例这里显示为『 4 』；
	"$@" ：代表『 "$1" "$2" "$3" "$4" 』之意，每个变数是独立的(用双引号括起来)；
	"$*" ：代表『 "$1 c $2 c $3 c $4" 』，其中c为分隔字元，预设为空白键，所以本例中代表『 "$1 $2 $3 $4" 』之意。

有点怪异的是， `$@` 与`"$@"` 的结果并不一样喔！当你输入的参数内带有双引号(") 时，建议还是得要使用"$@" 来带入脚本中， 否则双引号会被取消，这样执行结果的差异会相当大喔！尤其是像『 ./script one "a to b" 』这种仅有两个参数，但是参数内还有空白字元的， 最容易出现莫名的问题喔！

shift：造成参数变数号码偏移  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/shift-1.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/shift-2.jpg)

# 12.4 条件判断式 #

## 12.4.1 利用if .... then ##

- 单层、简单条件判断式

		if [条件判断式]; then
			当条件判断式成立时，可以进行的指令工作内容；
		fi    <==将if反过来写，就成为fi啦！结束if之意！

	有多个条件要判别时，除了『将多个条件写入一个中括号内的情况』之外，我还可以有多个中括号来隔开喔！而括号与括号之间，则以&&或||来隔开。

		&& 代表AND ；
		|| 代表or ；

		[ "${yn}" == "Y" -o "${yn}" == "y" ] 
		上式可替换为
		[ "${yn}" == "Y" ] || [ "${ yn}" == "y" ]

- 多重、复杂条件判断式
	
		#一个条件判断，分成功进行与失败进行(else) 
		if [条件判断式]; then
			当条件判断式成立时，可以进行的指令工作内容；
		else
			当条件判断式不成立时，可以进行的指令工作内容；
		fi


		#多个条件判断(if ... elif ... elif ... else)分多种不同情况执行
		if [条件判断式一]; then
			当条件判断式一成立时，可以进行的指令工作内容；
		elif [条件判断式二]; then
			当条件判断式二成立时，可以进行的指令工作内容；
		else
			当条件判断式一与二均不成立时，可以进行的指令工作内容；
		fi


	IP说明的是该服务位于那个介面上，`127.0.0.1` 则是仅针对本机开放，若是`0.0.0.0 或:::` 则代表对整个Internet 开放

		将日期转换为秒数：
			date --date="YYYYMMDD" +%s

	
## 12.4.2 利用case ..... esac 判断 ##

	case $变数名称in    <==关键字为case ，还有变数前有钱字号 
	  "第一个变数内容" )    <==每个变数内容建议用双引号括起来，关键字则为小括号)
		程式段
		;;             <==每个类别结尾使用两个连续的分号来处理！
	  "第二个变数内容" )
		程式段
		;; 
	  * )                   <==最后一个变数内容都会用*来代表所有其他值
		不包含第一个变数内容与第二个变数内容的其他程式执行段
		exit 1
		;; 
	esac                   <==最终的case结尾！『反过来写』思考一下！

一般来说，使用『 case $变数in 』这个语法中，当中的那个『 $变数』大致有两种取得的方式：

	直接下达式：例如上面提到的，利用『 script.sh variable 』的方式来直接给予$1这个变数的内容，这也是在/etc/init.d目录下大多数程式的设计方式。

	互动式：透过read这个指令来让使用者输入变数的内容。

## 12.4.3 利用function 功能 ##

因为shell script的执行方式是由上而下，由左而右，因此在shell script当中的function的设定一定要在程式的最前面。

	function fname () {
		程式段
	}

function也是拥有内建变数的～他的内建变数与shell script很类似，函数名称代表示`$0` ，而后续接的变数也是以`$1, $2...`来取代的。例如：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/12/function.jpg)


# 12.5 回圈(loop) #

## 12.5.1 while do done, until do done (不定回圈) ##

	while [ condition ]   <==中括号内的状态就是判断式 
	do             <==do是回圈的开始！
		程式段落
	done           <==done是回圈的结束
	当condition条件成立时，就进行回圈，直到condition的条件不成立才停止。

	until [ condition ]
	do
		程式段落
	done
	当condition条件成立时，就终止回圈，否则就持续进行回圈的程式段。

## 12.5.2 for...do...done (固定回圈) ##

	for var in con1 con2 con3 ...
	 do
		程式段
	done

	生成连续序列的两种方式：
		{1..100}	bash内建
		$(seq 1 100)

## 12.5.3 for...do...done 的数值处理 ##	

	for ((初始值;限制值;执行步阶))
	do
		程式段
	done
	初始值：某个变数在回圈当中的起始值，直接以类似i=1 设定好；
	限制值：当变数的值在这个限制值的范围内，就继续进行回圈。例如i<=100；
	执行步阶：每作一次回圈时，变数的变化量。例如i=i+1

## 12.5.4 搭配乱数与阵列的实验 ##	

shell script可以搭配数组使用


# 12.6 shell script 的追踪与debug #

	sh [-nvx] scripts.sh
	选项与参数：
		-n ：不要执行script，仅查询语法的问题；
		-v ：再执行sccript 前，先将scripts 的内容输出到萤幕上；
		-x ：将使用到的script 内容显示到萤幕上，这是很有用的参数！（相当于显示执行过程）


