## 1.1 Linux是什么 ##
## 1.1.1 Linux是什么？作业系统/应用程式？ ##
Linux就是一套作业系统！

## 1.1.2 Linux之前，Unix的历史 ##
- 1969年以前：一个伟大的梦想--Bell,MIT与GE的『Multics』系统  
1960年代初期麻省理工学院(MIT)发展了所谓的： 『相容分时系统(Compatible Time-Sharing System, CTSS)』，它可以让大型主机透过提供数个终端机(terminal)以连线进入主机，来利用主机的资源进行运算工作。架构有点像这样：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/01/0110whatislinux_2.jpg)  
比较先进的主机大概也只能提供30个不到的终端机而已  
1965年前后，由贝尔实验室(Bell)、麻省理工学院(MIT)及奇异公司(GE,或称为通用电器)共同发起了Multics的计画，Multics计画的目的是想要让大型主机可以达成提供300个以上的终端机连线使用的目标。

- 1969年：Ken Thompson的小型file server system

		Thompson的这个档案系统有两个重要的概念，分别是：  
			所有的程式或系统装置都是档案  
			不管建构编辑器还是附属档案，所写的程式只有一个目的，且要有效的完成目标。

- 1973年：Unix的正式诞生，Ritchie等人以C语言写出第一个正式Unix核心
  
- 1977年：重要的Unix分支--BSD的诞生  
Berkeley Software Distribution ( BSD)

- 1979年：重要的System V 架构与版权宣告  
1979年时，AT&T推出System V第七版Unix。这一版最重要的特色是可以支援x86架构的个人电脑系统，也就是说System V可以在个人电脑上面安装与运作了。  
不过因为AT&T由于商业的考量，以及在当时现实环境下的思考，于是想将Unix的版权收回去。  
目前被称为纯种的Unix指的就是System V以及BSD这两套啰！

- 1984年之一：x86架构的Minix作业系统开始撰写并于两年后诞生  
之所以称为Minix的原因，是因为他是个Mini (微小的) 的Unix系统啰！

- 1984年之二：GNU计画与FSF基金会的成立  
GNU是GNU is Not Unix 的简写  
GNU C Compiler(gcc)  
自由软体基金会(FSF, Free Software Foundation)  
通用公共许可证(General Public License, GPL)，称呼他为copyleft (相对于专利软体copyright！)
  
		GNU所开发的几个重要软件：  
			Emacs  
			GNU C (GCC)  
			GNU C Library (glibc)  
			Emacs

- 1988年：图形介面XFree86计画  
XFree86：X Window System + Free + x86

- 1991年：芬兰大学生Linus Torvalds的一则简讯

## 1.1.3 关于GNU计画、自由软体与开放原始码 ##

free software：自由软件  

    "Free software" is a matter of liberty, not price. To understand the concept, 
	you should think of "free speech", not "free beer". "Free software" refers to 
	the users' freedom to run, copy, distribute, study, change, and improve the software.

自由软体的重点并不是指『免费』的，而是指具有『自由度, freedom』的软体。自由度的意义是： 使用者可以自由的执行、复制、再发行、学习、修改与强化自由软体。  
1998年成立的『开放原始码促进会(Open Source Initiative)』提出了开放原始码(Open Source，亦可简称开源软体)这一名词！  

GPL自由软体也可以算是开源软体的一个，只是对于商业应用的限止稍微多一些而已。与GPL自由软体相比，其他开源软体的授权可能比较轻松喔！比较轻松的部份包括：再发布的授权可以跟原本的软体不同；另外，开源软体的全部或部份可作为其他软体的一部分，且其他软体无须使用与开源软体相同的授权来发布！这跟GPL自由软体差异就大了！自由软体的GPL授权规定，任何软体只要用了GPL的全部或部份程式码，那么该软体就得要使用GPL的授权！这对于自由软体的保障相当大！

	常见的开放原始码授权：
		Apache License 2.0
		BSD 3-Clause "New" or "Revised" license
		BSD 2-Clause "Simplified" or "FreeBSD" license
		GNU General Public License (GPL)
		GNU Library or "Lesser" General Public License (LGPL)
		MIT license
		Mozilla Public License 2.0
		Common Development and Distribution License

相对于Open Source的软体会释出原始码，Close source（专属软体/专利软体）的程式则仅推出可执行的二进位程式(binary program)而已。 
 	
	免费的专利软体代表的授权模式有：
	Freeware：不同于Free software，Freeware为『免费软体』而非『自由软体！』虽然它是免费的软体，但是不见得要公布其原始码。  
	Shareware：共享软件，Shareware在使用初期，它也是免费的，但是，到了所谓的『试用期限』之后，你就必须要选择『付费后继续使用』或者『将它移除』的宿命～

# 1.2 Torvalds的Linux发展 #

POSIX是可携式作业系统介面(Portable Operating System Interface)的缩写，重点在规范核心与应用程式之间的介面， 这是由美国电器与电子工程师学会(IEEE)所发布的一项标准喔！

    Linx核心版本号：  
       	3.10.0-123.el7.x86_64  
    	主版本.次版本.释出版本-修改版本  
释出版本则是在主、次版本架构不变的情况下，新增的功能累积到一定的程度后所新释出的核心版本。  
如果你有针对某个版本的核心修改过部分的程式码， 那么那个被修改过的新的核心版本就可以加上所谓的修改版本了。  

在2.6.x以前，主、次版本未奇数为发展中版本(development) ，主、次版本为偶数为稳定版本(stable) 

从3.0 版开始，核心主要依据主线版本(MainLine) 来开发，开发完毕后会往下一个主线版本进行。而旧的版本在新的主线版本出现之后，会有两种机制来处理，一种机制为结束开发(End of Live, EOL)，亦即该程式码已经结束，不会有继续维护的状态。另外一种机制为保持该版本的持续维护，亦即为长期维护版本(Longterm)！

『Kernel + Softwares + Tools +可完整安装程序』的咚咚，我们称之为Linux distribution。  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/01/distribution.gif)

为了让所有的Linux distributions开发不致于差异太大，且让这些开发商在开发的时候有所依据，还有Linux Standard Base (LSB)等标准来规范开发者，以及目录架构的File system Hierarchy Standard (FHS)标准规范！

distributions分类：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/01/categories.jpg)

此外，如果只想看看Linux的话，还可以选择所谓的可光碟开机进入Linux的Live CD版本， 亦即是KNOPPIX这个Linux distributions呢！


----------

有用的FAQ与How-To网站：  
Linux自己的文件资料： /usr/share/doc (在你的Linux系统中)  
The Linux Documentation Project：http://www.tldp.org/  


作为一个使用者，人要迁就机器；做为一个开发者，要机器迁就人。  
在Windows里面，设定不好设备，您可以骂它；在Linux里面，如果设定好设备了，您得要感激它﹗

	Linux是一个多人(Multi-user)多任务系统(Multitask):  
		Multiuser指的是Linux允许多人同时连上主机之外，每个使用者皆有其各人的使用环境，并且可以同时使用系统的资源！  
		Multitask指的是多工环境，在Linux系统下， CPU与其他例如网路资源可以同时进行多项工作， Linux最大的特色之一即在于其多工时，资源分配较为平均！

