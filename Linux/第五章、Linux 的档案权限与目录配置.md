# 5.1 使用者与群组 #
预设的情况下，所有的系统上的帐号与一般身份使用者，还有那个root的相关资讯， 都是记录在`/etc/passwd`这个档案内的。至于个人的密码则是记录在`/etc/shadow`这个档案下。此外，Linux所有的群组名称都纪录在`/etc/group`内！

# 5.2 Linux 档案权限概念 #

## 5.2.1 Linux档案属性 ##

档案属性的示意图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/centos7_0210filepermission_2.gif)

文件类型：
	
	d：目录
	-：文件
	l：链接档
	b：装置档里面的可供储存的周边设备(可随机存取装置)
	c：装置档里面的序列埠设备，例如键盘、滑鼠(一次性读取装置)

请你特别注意喔！不论是那一组权限，基本上，都是`『针对某些帐号来设计的权限』`喔！以群组来说，他规范的是『加入这个群组的帐号具有什么样的权限』之意， 以学校社团为例，假设学校有个童军社的社团办公室，『加入童军社的同学就可以进出社办』，主角是『学生(帐号)』而不是童军社本身喔！

每个档案都会将他的权限与属性记录到档案系统的i-node中，不过，我们使用的目录树却是使用档名来记录，因此每个档名就会连结到一个i-node啰！

使用ls显示的时候，档案容量的大小默认为`bytes`;

可以修改系统设定档`/etc/locale.conf`，来改变系统预设语系。

## 5.2.2 如何改变档案属性与权限 ##

	chgrp ：改变档案所属群组
	chown ：改变档案拥有者
	chmod ：改变档案的权限, SUID, SGID, SBIT等等的特性

- 改变所属群组, chgrp  
要被改变的群组名称必须要在`/etc/group`档案内存在才行，否则就会显示错误！

		chgrp [-R] dirname/filename ... 
		选项与参数：
			-R : 进行递回(recursive)的持续变更，亦即连同次目录下的所有档案、
			目录都更新成为这个群组之意。常常用在变更某一目录内所有的档案之况。

- 改变档案拥有者, chown
使用者必须是已经存在系统中的帐号，也就是在/etc/passwd 这个档案中有纪录的使用者名称才能改变。

		chown [-R]帐号名称档案或目录 
		chown [-R]帐号名称:群组名称档案或目录
		选项与参数：
		-R : 进行递回(recursive)的持续变更，亦即连同次目录下的所有档案都变更
		
- 改变权限, chmod
权限的设定方法有两种， 分别可以使用数字或者是符号来进行权限的变更。

		数字类型改变档案权限：
			f:4 w:2 x:1
			chmod [-R] xyz档案或目录
				选项与参数：
					xyz : 就是刚刚提到的数字类型的权限属性，为rwx 属性数值的相加。
					-R : 进行递回(recursive)的持续变更，亦即连同次目录下的所有档案都会变更
		符号类型改变档案权限:
			chmod u=rwx,go=rx .bashrc 
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/chmod-character.jpg)

## 5.2.3 目录与档案之权限意义 ##

- 权限对档案的重要性

		r (read)：可读取此一档案的实际内容，如读取文字档的文字内容等；
		w (write)：可以编辑、新增或者是修改该档案的内容(但不含删除该档案)；
		x (eXecute)：该档案具有可以被系统执行的权限。

	对于档案的rwx来说，主要都是针对『档案的内容』而言，与档案档名的存在与否没有关系喔！因为档案记录的是实际的资料嘛！

- 权限对目录的重要性  
目录主要的内容在记录档名清单，档名与目录有强烈的关连啦！

		r (read contents in directory)：
			表示具有读取目录结构清单的权限，所以当你具有读取(r)一个目录的权限时，表示你可以查
			询该目录下的档名资料。所以你就可以利用ls这个指令将该目录的内容列表显示出来！
		w (modify contents of directory)：
			这个可写入的权限对目录来说，是很了不起的！ 因为他表示你具有异动该目录结构清单的权限，也就是底下这些权限：
				建立新的档案与目录；
				删除已经存在的档案与目录(不论该档案的权限为何！)
				将已存在的档案或目录进行更名；
				搬移该目录内的档案、目录位置。
			总之，目录的w权限就与该目录底下的档名异动有关就对了啦！
		x (access directory)：
			目录的x代表的是使用者能否进入该目录成为工作目录的用途！所谓的工作目录(work directory)
			就是你目前所在的目录啦！举例来说，当你登入Linux时，你所在的家目录就是你当下的工作目录。
			而变换目录的指令是『cd』(change directory)啰！

	文件目录权限汇总：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/file-directory.jpg)

对一般档案来说，rwx 主要是针对『档案的内容』来设计权限，对目录来说，rwx则是针对『目录内的档名列表』来设计权限。

目录相当于抽屉，抽屉的r 代表『这个抽屉里面有灯光』， 所以你能看到的抽屉内的所有资料夹名称(非内容)。r 权限对于目录来说是非必须的，只是，没有r 的话，使用[tab] 时，他就无法自动帮你补齐档名了！

## 5.2.4 Linux档案种类与副档名 ##

档案种类：

	正规档案(regular file )：-
		纯文字档(ASCII)
		二进位档(binary)
		资料格式档(data)：
			有些程式在运作的过程当中会读取某些特定格式的档案，那些特定格式的档案可以被称为
			资料档(data file)。举例来说，我们的Linux在使用者登入时，都会将登录的资料记录在
			/var/log/wtmp那个档案内，该档案是一个data file，他能够透过last这个指令读来！
			但是使用cat时，会读出乱码～因为他是属于一种特殊格式的档案。
	
	目录(directory)：d
	连结档(link)：l
	设备与装置档(device)：
		与系统周边及储存等相关的一些档案，通常都集中在/dev这个目录之下！通常又分为两种：
			区块(block)设备档：
				就是一些储存资料，以提供系统随机存取的周边设备，举例来说，硬碟与软碟等就是啦！
				你可以随机的在硬碟的不同区块读写，这种装置就是区块装置啰！你可以自行查一下
				/dev/sda看看，会发现第一个属性为[ b ]喔！
			字元(character)设备档：
				亦即是一些序列埠的周边设备，例如键盘、滑鼠等等！这些设备的特色就是『一次性读
				取』的，不能够截断输出。举例来说，你不可能让滑鼠『跳到』另一个画面，而是『连续
				性滑动』到另一个地方啊！第一个属性为[ c ]。
	资料接口档(sockets)：
		既然被称为资料接口档，想当然尔，这种类型的档案通常被用在网路上的资料承接了。我们可以启动
		一个程式来监听用户端的要求，而用户端就可以透过这个socket来进行资料的沟通了。第一个属性
		为[ s ]，最常在/run或/tmp这些个目录中看到这种档案类型了。
	资料输送档(FIFO, pipe)：
		FIFO也是一种特殊的档案类型，他主要的目的在解决多个程序同时存取一个档案所造成的错误问题。
		FIFO是first-in-first-out的缩写。第一个属性为[p] 。

Linux档案长度限制：单一档案或目录的最大容许档名为255bytes，以一个ASCII 英文占用一个bytes 来说，则大约可达255 个字元长度。若是以每个中文字占用2bytes 来说， 最大档名就是大约在128 个中文字之谱！

Linux档案名称的限制，档名不要出现如下符号：* ? > < ; & ! [ ] | \ ' " ` ( ) { }

# 5.3 Linux目录配置 #

## 5.3.1 Linux目录配置的依据--FHS ##

Filesystem Hierarchy Standard (FHS)：FHS的重点在于规范每个特定的目录下应该要放置什么样子的资料而已。

FHS四种交互状态：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/fhs-directory-1.jpg)

FHS定义的三层目录：

	/ (root, 根目录)：与开机系统有关；
	/usr (unix software resource)：与软体安装/执行有关；
	/var (variable)：与系统运作过程有关。

根目录与开机/还原/系统修复等动作有关。因此FHS标准建议：根目录(/)所在分割槽应该越小越好，且应用程式所安装的软体最好不要与根目录放在同一个分割槽内，保持根目录越小越好。如此不但效能较佳，根目录所在的档案系统也较不容易发生问题。

CentOS7目录结构

	bin-->usr/bin
	boot
		config-*
		efi
		grub
		grub2
			device.map
			grub.cfg
			i386-pc
				boot.img
				core.img
				*.mod
			locale
				*.mo
		initramfs-*
		vmlinuz-*
	data
	dev
		null
		tty[0-9]
		zero
	etc
		cron.d
		firewalld
		rc.d
	home
	lib-->usr/lib
	lib64-->usr/bin64
	lost+found
	media
	mnt
	opt
	proc
	root
	run
	sbin-->usr/sbin
	srv
	sys
	tmp
	usr
		bin
		etc
		games
		include
			*.h
		lib
		lib64
		libexec
		local
			bin
			etc
			games
			include
			lib
			lib64
			qcloud
			sbin
			share
			src
		sbin
		share
		src
			man
			info
		tmp-->../var/tmp
	var
	

FHS推荐目录：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/fhs-directory-2.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/fhs-directory-3.jpg)

/usr 的意义与内容（usr是Unix Software Resource的缩写）：    
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/fhs-directory-4.jpg) 

/var 的意义与内容：  
如果/usr是安装时会占用较大硬碟容量的目录，那么/var就是在系统运作后才会渐渐占用硬碟容量的目录。    
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/fhs-directory-5.jpg)

## 5.3.2 目录树(directory tree) ##

目录树的特性：

	目录树的启始点为根目录(/, root)；
	每一个目录不止能使用本地端的partition 的档案系统，也可以使用网路上的filesystem 。举例来说， 可以利用Network File System (NFS) 伺服器挂载某特定目录等。
	每一个档案在此目录树中的档名(包含完整路径)都是独一无二的。

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/05/centos7_0210filepermission_4.jpg)

## 5.3.3 绝对路径与相对路径 ##

	. ：代表当前的目录，也可以使用./ 来表示；
	.. ：代表上一层目录，也可以../ 来代表。

