# 9.1 vi 与vim #

常常听到的文书编辑器： emacs , pico , nano , joe ,与vim等等

## 9.1.1 为何要学vim ##

	所有的Unix Like 系统都会内建vi 文书编辑器，其他的文书编辑器则不一定会存在；
	很多个别软体的编辑介面都会主动呼叫vi (例如未来会谈到的crontab , visudo , edquota等指令)；
	vim 具有程式编辑的能力，可以主动的以字体颜色辨别语法的正确性，方便程式设计；
	因为程式简单，编辑速度相当快速。

# 9.2 vi 的使用 #

vi共分为三种模式，分别是『一般指令模式』、『编辑模式』与『指令列命令模式』。

	一般指令模式(command mode)：
		以vi 打开一个档案就直接进入一般指令模式了(这是预设的模式，也简称为一般模式)。在这个模
		式中， 你可以使用『上下左右』按键来移动游标，你可以使用『删除字元』或『删除整列』来处
		理档案内容， 也可以使用『复制、贴上』来处理你的文件资料。
	编辑模式(insert mode)：
		在一般指令模式中可以进行删除、复制、贴上等等的动作，但是却无法编辑文件内容的！要等到你
		按下『i, I, o, O, a, A, r, R』等任何一个字母之后才会进入编辑模式。注意了！通常在
		Linux中，按下这些按键时，在画面的左下方会出现『INSERT或REPLACE』的字样，此时才可以进
		行编辑。而如果要回到一般指令模式时，则必须要按下『Esc』这个按键即可退出编辑模式。
	指令列命令模式(command-line mode)：
		在一般模式当中，输入『 : / ?』三个中的任何一个按钮，就可以将游标移动到最底下那一列。
		在这个模式当中，可以提供你『搜寻资料』的动作，而读取、存档、大量取代字元、离开vi 、显
		示行号等等的动作则是在此模式中达成的！

vi三种模式的相互关系：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/centos7_vi-mode.gif)

## 9.2.1 简易执行范例 ##

『强制写入』是在『你的权限可以改变』的情况下才能成立的！

## 9.2.2 按键说明 ##

- 第一部份：一般指令模式可用的按钮说明，游标移动、复制贴上、搜寻取代等
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/key-part-1.jpg)
	
- 第二部份：一般指令模式切换到编辑模式的可用的按钮说明
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/key-part-2.jpg)

- 第三部份：一般指令模式切换到指令列模式的可用按钮说明
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/key-part-3.jpg)

在vi中，『数字』是很有意义的！数字通常代表重复做几次的意思！也有可能是代表去到第几个什么什么的意思。

## 9.2.3 一个案例练习 ##

...

## 9.2.4 vim 的暂存档、救援回复与开启时的警告讯息 ##

当我们在使用vim编辑时， vim会在与被编辑的档案的目录下，再建立一个名为.filename.swp的档案。你对文件所做的所有操作都会记录到该文件中。

vim的工作被不正常的中断，导致暂存档无法藉由正常流程来结束，暂存档就不会消失，而继续保留下来。

vim并不会主动删除暂存档。

# 9.3 vim 的额外功能 #

## 9.3.1 区块选择(Visual Block) ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/visual-block.jpg)

## 9.3.2 多档案编辑 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/multi-file.jpg)

## 9.3.3 多视窗功能 ##

在指令列模式输入『:sp {filename}』即可！那个filename可有可无， 如果想要在新视窗启动另一个档案，就加入档名，否则仅输入:sp时，出现的则是同一个档案在两个视窗间！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/multi-window.jpg)

## 9.3.4 vim 的挑字补全功能 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/content-complete.jpg)

## 9.3.5 vim 环境设定与记录： ~/.vimrc, ~/.viminfo ##

vim会主动的将你曾经做过的行为登录下来，好让你下次可以轻松的作业啊！那个记录动作的档案就是： ~/.viminfo！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/vim-env-set.jpg)

 整体vim的设定值一般是放置在/etc/vimrc这个档案，不过，不建议你修改他！你可以修改~/.vimrc这个档案 (预设不存在，请你自行手动建立！)，将你所希望的设定值写入！

	[dmtsai@study ~]$ vim ~/.vimrc 
	"这个档案的双引号(")是注解
	set hlsearch "高亮度反白
	set backspace=2 "可随时用倒退键删除
	set autoindent "自动缩排
	set ruler "可显示最后一列的状态
	set showmode "左下角那一列的状态
	set nu "可以在每一列的最前面显示行号啦！
	set bg=dark "显示不同的底色色调
	syntax on "进行语法检验，颜色显示。

在这个档案中，使用『 set hlsearch 』或『 :set hlsearch 』，亦即最前面有没有冒号『 : 』效果都是一样的！至于双引号则是注解符号！

## 9.3.6 vim 常用指令示意图 ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/09/vim-commands.jpg)

# 9.4 其他vim 使用注意事项 #

## 9.4.1 中文编码的问题 ##

编码需要考虑的问题：
	
	1、你的Linux 系统预设支援的语系资料：这与/etc/locale.conf 有关；
	2、你的终端介面(bash) 的语系： 这与LANG, LC_ALL 这几个变数有关；
	3、你的档案原本的编码；
	4、开启终端机的软体，例如在GNOME 底下的视窗介面。

事实上最重要的是上头的第三与第四点，只要这两点的编码一致，你就能够正确的看到与编辑你的中文档案。否则就会看到一堆乱码啦！

## 9.4.2 DOS 与Linux 的断行字元 ##

DOS使用的断行字元为^M$ ，我们称为CR与LF两个符号。而在Linux底下，则是仅有LF ($)这个断行符号。

	dos2unix [-kn] file [newfile]
	unix2dos [-kn] file [newfile]
	选项与参数：
		-k ：保留该档案原本的mtime 时间格式(不更新档案上次内容经过修订的时间)
		-n ：保留原本的旧档，将转换后的内容输出到新档案，如： dos2unix -n old new

## 9.4.3 语系编码转换 ##
	
	iconv --list
	iconv -f原本编码-t新编码filename [-o newfile]
	选项与参数：
		--list ：列出iconv 支援的语系资料
		-f ：from ，亦即来源之意，后接原本的编码格式；
		-t ：to ，亦即后来的新编码要是什么格式；
		-o file：如果要保留原本的档案，那么使用-o 新档名，可以建立新编码档案。

从繁体utf8转化成简体utf8:

	[dmtsai@study vitest]$ iconv -f utf8 -t big5 vi.utf8 | \ 
	> iconv -f big5 -t gb2312 | iconv -f gb2312 -t utf8 -o vi.gb.utf8


	