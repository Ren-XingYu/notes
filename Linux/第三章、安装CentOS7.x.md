## 安装CentO
 ##
强制使用GPT分割表的安装参数：`inst.gpt`  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/centos7_02.jpg)

装置类型：

	标准分割区：就是我们一直谈的分割槽啊！类似/dev/vda1之类的分割就是了。
	LVM：这是一种可以弹性增加/削减档案系统容量的装置设定，我们会在后面的章节持续介绍LVM这个有趣的东西！
	LVM紧张供应：这个名词翻译的超奇怪的！其实这个是LVM的进阶版！与传统LVM直接分配固定的容量不
	同，这个『 LVM紧张供应』的项目，可以让你在使用多少容量才分配磁碟多少容量给你，所以如果LVM
	装置内的资料量较少，那么你的磁碟其实还可以作更多的资料储存！而不会被平白无故的占用！这部份
	我们也在后续谈到LVM的时候再来强调！

文件系统（实际格式化的时候，可以格式化成什么文件系统）：

	ext2/ext3/ext4：Linux早期适用的档案系统类型。由于ext3/ext4档案系统多了日志的记录，对于
	系统的复原比较快速。不过由于磁碟容量越来越大，ext家族似乎有点挡不住了～所以除非你有特殊的
	设定需求，否则近来比较少使用ext4项目了！
	swap：当有资料被存放在实体记忆体里面，但是这些资料又不是常被CPU所取用时， 那么这些不常被使
	用的程序将会被丢到硬碟的swap置换空间当中， 而将速度较快的实体记忆体空间释放出来给真正需要
	的程序使用！就是磁碟模拟成为记忆体，由于swap并不会使用到目录树的挂载，所以用swap就不需要指
	定挂载点喔。
	BIOS Boot：就是GPT分割表可能会使用到的项目，若你使用MBR分割，那就不需要这个项目了！
	xfs：这个是目前CentOS预设的档案系统，最早是由大型伺服器所开发出来的！他对于大容量的磁碟管
	理非常好，而且格式化的时候速度相当快，很适合当今动不动就是好几个TB的磁碟的环境喔！因此我们
	主要用这玩意儿！
	vfat：同时被Linux与Windows所支援的档案系统类型。如果你的主机硬碟内同时存在Windows与
	Linux作业系统，为了资料的交换，确实可以建置一个vfat的档案系统喔！

KDUMP项目，这个项目主要在处理，当Linux系统因为核心问题导致的当机事件时，会将该当机事件的记忆体内资料储存出来的一项特色！不过，这个特色似乎比较偏向核心开发者在除错之用～如果你有需要的话，也可以启动它！

系统安装时候所设定的项目，包括root 的密码等等，通通都会被纪录到`/root/anaconda-ks.cfg`这个档案内喔！这个档案可以提醒与协助你未来想要重建一个一模一样的系统时，就可以参考该档案来制作啰!

## 安装笔记型电脑或其他类PC电脑的参数 ##

由于笔记型电脑加入了非常多的省电机制或者是其他硬体的管理机制，包括显示卡常常是整合型的， 因此在笔记型电脑上面的硬体常常与一般桌上型电脑不怎么相同。所以当你使用适合于一般桌上型电脑的DVD来安装Linux时， 可能常常会出现一些问题，导致无法顺利的安装Linux到你的笔记型电脑中啊！可以在安装的时候加入如下参数：

	nofb apm=off acpi=off pci=noacpi

apm(Advanced Power Management)是早期的电源管理模组，acpi(Advanced Configuration and Power Interface)则是近期的电源管理模组。这两者都是硬体本身就有支援的，但是笔记型电脑可能不是使用这些机制， 因此，当安装时启动这些机制将会造成一些错误，导致无法顺利安装。

nofb则是取消显示卡上面的缓冲记忆体侦测。因为笔记型电脑的显示卡常常是整合型的， Linux安装程式本身可能就不是很能够侦测到该显示卡模组。此时加入nofb将可能使得你的安装过程顺利一些。

对于这些在开机的时候所加入的参数，我们称为『核心参数』，这些核心参数是有意义的！

在图形安装界面可以按`ctrl+alt+f2`来进入安装过程中的shell环境。再按`ctrl+alt+f6`可以回到原来的安装流程。

parted分区方法如下：  

	parted /dev/vda mklabel msdos             	#建立MBR分割 
	parted /dev/vda mkpart primary 1M 2G      	#建立/boot 
	parted / dev/vda mkpart primary 2G 52G     	#建立/ 
	parted /dev/vda mkpart primary 52G 152G   	#建立C 
	parted /dev/vda mkpart extended 152G 100% 	#建立延伸分割 
	parted /dev/vda mkpart logical 152G 100% 	#建立逻辑分割 
	parted /dev/vda print                     	#显示分割结果

## 安装多系统救援回Linux 的开机管理程式 ##

放入光盘-->Troubleshooting-->Rescue a CentOS system-->Continue-->OK-->修改开机选单

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/multi_boot_mbr_4.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/multi_boot_mbr_5.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/rescue-instruction.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/boot-menu.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/03/multi_boot_mbr_6.jpg)