# 13.1 Linux 的帐号与群组 #

## 13.1.1 使用者识别码： UID 与GID ##

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/change-uid.jpg)

如上图所示，当改变了linux账户的uid时，uid由1001变成2000。由于家目录的记录的uid还是原先的1001，会导致linux账户无法进入家目录。

## 13.1.2 使用者帐号 ##
login登录流程:

	1、先找寻/etc/passwd 里面是否有你输入的帐号？如果没有则跳出，如果有的话则将该帐号对应的 UID 与GID (在/etc/group 中) 读出来，另外，该帐号的家目录与shell 设定也一并读出；
	
	2、再来则是核对密码表啦！这时Linux 会进入/etc/shadow 里面找出对应的帐号与 UID，然后核对一下你刚刚输入的密码与里头的密码是否相符？
	
	3、如果一切都OK 的话，就进入Shell 控管的阶段啰！

详细的说明可以参考man 5 passwd及man 5 shadow 

- /etc/passwd 档案结构

	每一行都代表一个帐号，有几行就代表有几个帐号在你的系统中！不过需要特别留意的是，里头很多帐号本来就是系统正常运作所必须要的，我们可以简称他为系统帐号，例如bin, daemon, adm, nobody等等，这些帐号请不要随意的杀掉他呢！

		账号名称：密码：UID：GID：使用者资讯说明栏：家目录：Shell
	
	密码：这个档案的特性是所有的程序都能够读取，这样一来很容易造成密码资料被窃取，因此后来就将这个栏位的密码资料给他改放到/etc/shadow 中了。所以这里你会看到一个『 x 』。	

	UID说明：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/uid.jpg)

	使用者资讯说明栏：这个栏位基本上并没有什么重要用途，只是用来解释这个帐号的意义而已！不过，如果您提供使用finger的功能时，这个栏位可以提供很多的讯息呢！本章后面的chfn指令会来解释这里的说明。

- /etc/shadow 档案结构

	各程式需要读取/etc/passwd来了解不同帐号的权限。 因此/etc/passwd的权限需设定为-rw-r--r--。

		帐号名称：密码：最近更动密码的日期：密码不可被更动的天数：密码需要重新变更的天数：密码需要变更期限前的警告天数：密码过期后的帐号宽限时间(密码失效日)：帐号失效日期：保留

	密码：由于固定的编码系统产生的密码长度必须一致，因此『当你让这个栏位的长度改变后，该密码就会失效(算不出来)』。很多软体透过这个功能，在此栏位前加上!或*改变密码栏位长度，就会让密码『暂时失效』了。
	
	最近更动密码的日期：以1970年1月1日作为1而累加的日期，以天为单位。

		可以通过：echo $(($(date --date="2015/05/04" +%s)/86400+1)) 计算

	密码不可被更动的天数(与第3栏位相比)：这个帐号的密码在最近一次被更改后需要经过几天才可以再被变更！如果是0的话，表示密码随时可以更动的意思。

	密码需要重新变更的天数(与第3栏位相比)：经常变更密码是个好习惯！为了强制要求使用者变更密码，这个栏位可以指定在最近一次更改密码后，在多少天数内需要再次的变更密码才行。你必须要在这个天数内重新设定你的密码，否则这个帐号的密码将会『变为过期特性』。

	密码需要变更期限前的警告天数(与第5栏位相比)：当帐号的密码有效期限快要到的时候(第5栏位)，系统会依据这个栏位的设定，发出『警告』言论给这个帐号，提醒他『再过n天你的密码就要过期了，请尽快重新设定你的密码呦！』。

	密码过期后的帐号宽限时间(密码失效日)(与第5栏位相比)：如果密码过期了，那当你登入系统时，系统会强制要求你必须要重新设定密码才能登入继续使用喔，这就是密码过期特性。该栏是在密码过期几天后，如果使用者还是没有登入更改密码，那么这个帐号的密码将会『失效』，亦即该帐号再也无法使用该密码登入了。要注意密码过期与密码失效并不相同。

	帐号失效日期：使用1970年以来的总日数设定。这个栏位表示： 这个帐号在此栏位规定的日期之后，将无法再使用。就是所谓的『帐号失效』，此时不论你的密码是否有过期，这个『帐号』都不能再被使用！这个栏位会被使用通常应该是在『收费服务』的系统中，你可以规定一个日期让该帐号不能再使用啦！

		dmtsai:$6$M4IphgNP2TmlXaSS$B418YFroYxxmm....:16559:5:60:7:5:16679:
		最近一次更改密码时间：2015/05/04 (16559)
		2015/05/09以前dmtsai不能修改自己的密码（5）
		使用者必须要在2015/05/09 (前5天不能改)到2015/07/03(60)之间的60天限制内去修改自己的密码，若2015/07/03之后还是没有变更密码时，该密码就宣告为过期
		警告日期为：2015/06/26 ~ 2015/07/03（7）
		在2015/07/08（5）前都还可以使用旧密码登入主机，登录后会被强制更改密码
		2015/09/01（16679）账号失效

	可以使用如下指令查看shadow采用哪种加密机制：
		
		authconfig --test | grep hashing

## 13.1.3 关于群组： 有效与初始群组、groups, newgrp ##

- /etc/group 档案结构

		群组名称：群组密码：GID：此群组支援的帐号名称

	群组密码：通常不需要设定，这个设定通常是给『群组管理员』使用的，目前很少有这个机会设定群组管理员啦！同样的，密码已经移动到/etc/gshadow去，因此这个栏位只会存在一个『x』而已。
	
	此群组支援的帐号名称：新版的Linux 中，初始群组的用户群已经不会加入这里。

	帐号相关档案之间的UID/GID 与密码相关性示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/centos7_id_link.jpg)

- 有效群组(effective group)与初始群组(initial group)
	
	/etc/passwd 里面的第四栏的GID就是所谓的『初始群组 (initial group) 』！也就是说，当使用者一登入系统，立刻就拥有这个群组的相关权限的意思。	
		
	建立一个新的档案或者是新的目录，是参考当时的有效群组(effective group)。

- groups: 有效与支援群组的观察
	
		groups
		第一个输出的为有效群组。通常有效群组的作用是在新建档案啦。

- newgrp: 有效群组的切换

	想要切换的群组必须是你已经有支援的群组。

		newgrp users
		
	newgrp可以变更目前使用者的有效群组，而且是另外以一个shell来提供这个功能的喔。由于是新取得一个shell ，因此如果你想要回到原本的环境中，请输入exit 回到原本的shell 喔！

	newgrp 的运作示意图：  
	![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/newgrp.gif)

	加入群组的两种方式：

		1、透过系统管理员(root)利用 usermod帮你加入。
		2、过群组管理员以 gpasswd帮你加入他所管理的群组中！


- /etc/gshadow  
	四个栏位的意义为：
		
		1、群组名称
		2、密码栏，同样的，开头为! 或为空表示无合法密码，所以无群组管理员
		3、群组管理员的帐号(相关资讯在gpasswd中介绍)
		4、有加入该群组支援的所属帐号(与/etc/group 内容相同！)
	
	以系统管理员的角度来说，这个gshadow最大的功能就是建立群组管理员啦！


## 13.2 帐号管理 ##

### 13.2.1 新增与移除使用者： useradd, 相关设定档, passwd, usermod, userdel ###

- useradd

		useradd [-u UID] [-g初始群组] [-G次要群组] [-mM] [-c说明栏] [-d家目录绝对路径] [- s shell]使用者帐号名
		选项与参数：
			-u ：后面接的是UID ，是一组数字。直接指定一个特定的UID 给这个帐号；
			-g ：后面接的那个群组名称就是我们上面提到的initial group 啦～
			      该群组的GID 会被放置到/etc/passwd 的第四个栏位内。
			-G ：后面接的群组名称则是这个帐号还可以加入的群组。
			      这个选项与参数会修改/etc/group 内的相关资料喔！
			-M ：强制！不要建立使用者家目录！(系统帐号预设值)
			-m ：强制！要建立使用者家目录！(一般帐号预设值)
			-c ：这个就是/etc/passwd 的第五栏的说明内容啦～可以随便我们设定的啦～
			-d ：指定某个目录成为家目录，而不要使用预设值。务必使用绝对路径！
			-r ：建立一个系统的帐号，这个帐号的UID 会有限制(参考/etc/login.defs)
			-s ：后面接一个shell ，若没有指定则预设是/bin/bash 的啦～
			-e ：后面接一个日期，格式为『YYYY-MM-DD』此项目可写入shadow 第八栏位，
			      亦即帐号失效日的设定项目啰；
			-f ：后面接shadow 的第七栏位项目，指定密码是否会失效。0为立刻失效，
			      -1 为永远不失效(密码只会过期而强制于登入时重新设定而已。)


		CentOS建立账号处理流程：
		在/etc/passwd 里面建立一行与帐号相关的资料，包括建立UID/GID/家目录等；
		在/etc/shadow 里面将此帐号的密码相关参数填入，但是尚未有密码；
		在/etc/group 里面加入一个与帐号名称一模一样的群组名称；
		在/home 底下建立一个与帐号同名的目录作为使用者家目录，且权限为700
	
		系统帐号预设都不会主动建立家目录的！
	
		useradd所涉及的文档
		使用者帐号与密码参数方面的档案：/etc/passwd, /etc/shadow
		使用者群组相关方面的档案：/etc/group, /etc/gshadow
		使用者的家目录：/home/帐号名称

- useradd 参考档

	useradd 的预设值可以使用底下的方法呼叫出来：

		useradd -D
		这个资料其实是由/etc/default/useradd呼叫出来的！

		GROUP=100：新建帐号的初始群组使用GID 为100 者
			私有群组机制：系统会建立一个与帐号一样的群组给使用者作为初始群组。代表性的distributions 有RHEL, Fedora, CentOS 等；
			公共群组机制：就是以GROUP=100 这个设定值作为新建帐号的初始群组，因此每个帐号都属于users 这个群组。代表distributions 如SuSE等。
		HOME=/home：使用者家目录的基准目录(basedir)
		INACTIVE=-1：密码过期后是否会失效的设定值
			如果是0代表密码过期立刻失效，如果是-1则是代表密码永远不会失效，如果是数字，如30 ，则代表过期30天后才失效
		EXPIRE=：帐号失效的日期
		SKEL=/etc/skel：使用者家目录参考基准目录
		CREATE_MAIL_SPOOL=yes：建立使用者的mailbox
			var/spool/mail/username

	UID/GID还有密码参数预设值参考档案

		参考档：/etc/login.defs 
		
		MAIL_DIR /var/spool/mail   <==使用者预设邮件信箱放置目录

		PASS_MAX_DAYS 99999     <==/etc/shadow内的第5栏，多久需变更密码日数 
		PASS_MIN_DAYS 0         <==/etc/shadow内的第4栏，多久不可重新设定密码日数 
		PASS_MIN_LEN 5         <==密码最短的字元长度，已被pam模组取代，失去效用！
		PASS_WARN_AGE 7         <==/etc/shadow内的第6栏，过期前会警告的日数
		
		UID_MIN 1000     <==使用者最小的UID，意即小于1000的UID为系统保留 
		UID_MAX 60000     <==使用者能够用的最大UID 
		SYS_UID_MIN 201     <==保留给使用者自行设定的系统帐号最小值UID 
		SYS_UID_MAX 999     <==保留给使用者自行设定的系统帐号最大值UID 
		GID_MIN 1000     <==使用者自订群组的最小GID，小于1000为系统保留 
		GID_MAX 60000     <==使用者自订群组的最大GID 
		SYS_GID_MIN 201     <==保留给使用者自行设定的系统帐号最小值GID 
		SYS_GID_MAX 999     <==保留给使用者自行设定的系统帐号最大值GID
		
		CREATE_HOME yes       <==在不加-M及-m时，是否主动建立使用者家目录？
		UMASK 077       <==使用者家目录建立的umask ，因此权限会是700 
		USERGROUPS_ENAB yes       <==使用userdel删除时，是否会删除初始群组 
		ENCRYPT_METHOD SHA512     <==密码加密的机制使用的是sha512这一个机制！

	使用useradd 这支程式在建立Linux 上的帐号时，至少会参考：

		/etc/default/useradd
		/etc/login.defs
		/etc/skel/*
	
- passwd
	
		passwd [--stdin] [帐号名称]   <==所有人均可使用来改自己的密码 
		passwd [-l] [-u] [--stdin] [-S] [-n日数] [-x日数] [-w日数] [-i日数]帐号 <==root功能
		选项与参数：
			--stdin ：可以透过来自前一个管线的资料，作为密码输入，对shell script 有帮助！
			-l ：是Lock 的意思，会将/etc/shadow 第二栏最前面加上! 使密码失效；
			-u ：与-l 相对，是Unlock 的意思！
			-S ：列出密码相关参数，亦即shadow 档案内的大部分资讯。
			-n ：后面接天数，shadow 的第4 栏位，多久不可修改密码天数
			-x ：后面接天数，shadow 的第5 栏位，多久内必须要更动密码
			-w ：后面接天数，shadow 的第6 栏位，密码过期前的警告天数
			-i ：后面接天数，shadow 的第7 栏位，密码失效天数
		
			新的distributions是使用较严格的PAM模组来管理密码，这个管理的机制写在/etc/pam.d/passwd当中。
			而该档案与密码有关的测试模组就是使用：pam_cracklib.so，这个模组会检验密码相关的资讯，并且取代/etc/login.defs内的PASS_MIN_LEN的设定

- chage

		chage [-ldEImMW]帐号名
		选项与参数：
			-l ：列出该帐号的详细密码参数；
			-d ：后面接日期，修改shadow 第三栏位(最近一次更改密码的日期)，格式YYYY-MM-DD
			-E ：后面接日期，修改shadow 第八栏位(帐号失效日)，格式YYYY-MM-DD
			-I ：后面接天数，修改shadow 第七栏位(密码失效日期)
			-m ：后面接天数，修改shadow 第四栏位(密码最短保留天数)
			-M ：后面接天数，修改shadow 第五栏位(密码多久需要进行变更)
			-W ：后面接天数，修改shadow 第六栏位(密码过期前警告日期)

	chage有一个功能很不错喔！如果你想要让『使用者在第一次登入时，强制她们一定要更改密码后才能够使用系统资源』，可以利用如下的方法来处理的！

		useradd agetest
		echo "agetest" | passwd --stdin agetest
		chage -d 0 agetest
		chage -l agetest | head -n 3

- usermod
	
		usermod [-cdegGlsuLU] username 
		选项与参数：
			-c ：后面接帐号的说明，即/etc/passwd 第五栏的说明栏，可以加入一些帐号的说明。
			-d ：后面接帐号的家目录，即修改/etc/passwd 的第六栏；
			-e ：后面接日期，格式是YYYY-MM-DD 也就是在/etc/shadow 内的第八个栏位资料啦！
			-f ：后面接天数，为shadow 的第七栏位。
			-g ：后面接初始群组，修改/etc/passwd 的第四个栏位，亦即是GID 的栏位！
			-G ：后面接次要群组，修改这个使用者能够支援的群组，修改的是/etc/group 啰～
			-a ：与-G 合用，可『增加次要群组的支援』而非『设定』喔！
			-l ：后面接帐号名称。亦即是修改帐号名称， /etc/passwd 的第一栏！
			-s ：后面接Shell 的实际档案，例如/bin/bash 或/bin/csh 等等。
			-u ：后面接UID 数字啦！即/etc/passwd 第三栏的资料；
			-L ：暂时将使用者的密码冻结，让他无法登入。其实仅改/etc/shadow 的密码栏。
			-U ：将/etc/shadow 密码栏的! 拿掉，解冻啦！

- userdel

		使用者的资料有：
		使用者帐号/密码相关参数：/etc/passwd, /etc/shadow
		使用者群组相关参数：/etc/group, /etc/gshadow
		使用者个人档案资料： /home/username, /var/spool/mail/username..

		userdel [-r] username 
		选项与参数：
			-r ：连同使用者的家目录也一起删除
		
	
### 13.2.2 使用者功能 ###

	id [username]

	finger [-s] username   # 大部分都是在/etc/passwd这个档案里面的资讯啦
	选项与参数：
		-s ：仅列出使用者的帐号、全名、终端机代号与登入时间等等；
		-m ：列出与后面接的帐号相同者，而不是利用部分比对(包括全名部分)
	Login：为使用者帐号，亦即/etc/passwd 内的第一栏位；
	Name：为全名，亦即/etc/passwd 内的第五栏位(或称为注解)；
	Directory：就是家目录了；
	Shell：就是使用的Shell 档案所在；
	Never logged in.：figner 还会调查使用者登入主机的情况喔！
	No mail.：调查/var/spool/mail 当中的信箱资料；
	No Plan.：调查~vbird1/.plan 档案，并将该档案取出来说明！

	chfn [-foph] [帐号名]   # change finger  类似于个人资料
	选项与参数：
		-f ：后面接完整的大名；
		-o ：您办公室的房间号码；
		-p ：办公室的电话号码；
		-h ：家里的电话号码！

	chsh [-ls]	# change shell
	选项与参数：
		-l ：列出目前系统上面可用的shell ，其实就是/etc/shells 的内容！
		-s ：设定修改自己的Shell 啰
	
### 13.2.3 新增与移除群组 ###	

- groupadd
		
		groupadd [-g gid] [-r]群组名称
		选项与参数：
			-g ：后面接某个特定的GID ，用来直接给予某个GID ～
			-r ：建立系统群组啦！与/etc/login.defs 内的GID_MIN 有关。

- groupmod
	
		groupmod [-g gid] [-n group_name]群组名
		选项与参数：
			-g ：修改既有的GID 数字；
			-n ：修改既有的群组名称	

- groupdel

		groupdel [groupname]

- gpasswd：群组管理员功能
		
		#关于系统管理员(root)做的动作：
		gpasswd groupname
		gpasswd [-A user1,...] [-M user3,...] groupname
		gpasswd [-rR] groupname
		选项与参数：
			    ：若没有任何参数时，表示给予groupname 一个密码(/etc/gshadow)
			-A ：将groupname 的主控权交由后面的使用者管理(该群组的管理员)
			-M ：将某些帐号加入这个群组当中！
			-r ：将groupname 的密码移除
			-R ：让groupname 的密码栏失效

		#关于群组管理员(Group administrator)做的动作：
		gpasswd [-ad] user groupname
		选项与参数：
			-a ：将某位使用者加入到groupname 这个群组当中！
			-d ：将某位使用者移除出groupname 这个群组当中。

### 13.2.4 帐号管理实例 ###

	pro1，pro2，pro3是一个项目组的开发人员，/srv/projecta为开发目录
	/srv/projecta的目录权限为：
	drwxrws--- . 2 root projecta 6 Jul 20 23:32 /srv/projecta

	假设myuser1是该项目的助理，他可以查看/srv/project目录的内容，但是不能修改，解决办法为：
		1、将myuser1加入projecta这个群组，但myuser1可以删除了
		2、将/srv/projecta的权限改为2775，其它用户也可以查看了

	传统的Linux 权限无法设定了，此时需要ACL

### 13.2.5 使用外部身份认证系统 ###

Windows 底下有个很有名的身份验证系统，称为`Active Directory (AD)`的东西

CentOS 提供一只名为`authconfig-tui` 的指令可以进行外部身份验证，这个指令的执行结果如下：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/authconfig-tui.jpg)


## 13.3 主机的细部权限规划：ACL 的使用 ##

### 13.3.1 什么是ACL 与如何支援启动ACL ###

ACL 是`Access Control List` 的缩写，主要的目的是在提供传统的 owner,group,others 的read,write,execute 权限之外的细部权限设定。ACL 可以针对`单一使用者，单一档案或目录`来进行r,w,x 的权限规范，对于需要特殊权限的使用状况非常有帮助。

ACL主要针对以下几个项目：
	
	使用者(user)：可以针对使用者来设定权限；
	群组(group)：针对群组为对象来设定其权限；
	预设属性(mask)：还可以针对在该目录下在建立新档案/目录时，规范新资料的预设权限；

查看是否启动ACL
	
	dmesg | grep -i acl

### 13.3.2 ACL 的设定技巧： getfacl, setfacl ###

- setfacl 指令用法介绍及最简单的『 u:帐号:权限』设定

		setfacl [-bkRd] [{-m|-x} acl参数]目标档名
		选项与参数：
			-m ：设定后续的acl 参数给档案使用，不可与-x 合用；
			-x ：删除后续的acl 参数，不可与-m 合用；
			-b ：移除『所有的』 ACL 设定参数；
			-k ：移除『预设的』 ACL 参数，关于所谓的『预设』参数于后续范例中介绍；
			-R ：递回设定acl ，亦即包括次目录都会被设定起来；
			-d ：设定『预设acl 参数』的意思！只对目录有效，在该目录新建的资料会引用此预设值
		setfacl -m u:vbird1:rx acl_test1
		如果一个档案设定了ACL 参数后，他的权限部分就会多出一个+ 号了！

- getfacl 指令用法

		getfacl filename
		选项与参数：
			getfacl 的选项几乎与setfacl 相同！所以鸟哥这里就免去了选项的说明啊！
	
		getfacl acl_test1 
		# file: acl_test1    <==说明档名而已！
		# owner: root        <==说明此档案的拥有者，亦即ls -l看到的第三使用者栏位 
		# group: root        <==此档案的所属群组，亦即ls -l看到的第四群组栏位 
		user::rwx            <==使用者列表栏是空的，代表档案拥有者的权限
		user:vbird1:rx      <==针对vbird1的权限设定为rx ，与拥有者并不同！
		group::r--           <==针对档案群组的权限设定仅有r 
		mask::rx            <==此档案预设的有效权限(mask) 
		other::r--           <==其他人拥有的权限啰！
		显示的资料前面加上# 的，代表这个档案的预设属性，包括档名、档案拥有者与档案所属群组。底下出现的user, group, mask, other 则是属于不同使用者、群组与有效权限(mask)的设定值。

- 特定的单一群组的权限设定：『 g:群组名:权限』

		setfacl -m g:mygroup1:rx acl_test

- 针对有效权限设定：『 m:权限』

	mask有点像是『有效权限』的意思！他的意义是： 使用者或群组所设定的权限必须要存在于mask的权限设定范围内才会生效，此即『有效权限(effective permission)』

		setfacl -m m:r acl_test
		getfacl acl_test
		# file: acl_test1
		# owner: root
		# group: root
		user::rwx
		user:vbird1:rx #effective:r--  <==vbird1+mask均存在者，仅有r而已，x不会生效
		group::r--
		group:mygroup1:rx #effective:r--
		mask::r--
		other::r--
		vbird1 与mask 的集合发现仅有r 存在，因此vbird1 仅具有r 的权限而已，并不存在x 权限！这就是mask 的功能了！
		我们可以透过使用mask 来规范最大允许的权限，就能够避免不小心开放某些权限给其他使用者或群组了。

- 使用预设权限设定目录未来档案的ACL 权限继承『 d:[u|g]:[user|group]:权限』
	
		setfacl -m d:u:myuser1:rx /srv/projecta 
		
		取消myuser1 的设定(连同预设值)
		setfacl -x u:myuser1 /srv/projecta
		setfacl -x d:u:myuser1 /srv/projecta		
	
		pro3 在该目录下无任何权限
		setfacl -mu:pro3:- /srv/projecta
		当设定一个用户/群组没有任何权限的ACL 语法中，在权限的栏位不可留白，而是应该加上一个减号(-) 才是正确的作法


## 13.4 使用者身份切换 ##

一般使用者转变身份成为root的两种方式：
	
	以『 su - 』直接将身份变成root即可，但是这个指令却需要root的密码，也就是说，如果你要以su变成root的话，你的一般使用者就必须要有root的密码才行；

	以『 sudo 指令』执行root 的指令串，由于sudo 需要事先设定妥当，且sudo 需要输入使用者自己的密码， 因此多人共管同一部主机时， sudo 要比su 来的好喔！至少root 密码不会流出去！

### 13.4.1 su ###

	su [-lm] [-c指令] [username]
	选项与参数：
		- ：单纯使用- 如『 su - 』代表使用login-shell 的变数档案读取方式来登入系统；
		      若使用者名称没有加上去，则代表切换为root 的身份。
		-l ：与- 类似，但后面需要加欲切换的使用者帐号！也是login-shell 的方式。
		-m ：-m 与-p 是一样的，表示『使用目前的环境设定，而不读取新使用者的设定档』
		-c ：仅进行一次指令，所以-c 后面可以加上指令喔！	

	单纯使用『 su 』切换成为root的身份，读取的变数设定方式为non-login shell的方式，这种方式很多原本的变数不会被改变，
	尤其是我们之前谈过很多次的PATH这个变数，由于没有改变成为root的环境，因此很多root惯用的指令就只能使用绝对路径来执行咯。

	su总结：
	若要完整的切换到新使用者的环境，必须要使用『 su - username 』或『 su -l username 』， 才会连同PATH/USER/MAIL 等变数都转成新使用者的环境；
	如果仅想要执行一次root 的指令，可以利用『 su - -c "指令串" 』的方式来处理；
	使用root 切换成为任何使用者时，并不需要输入新使用者的密码；

	虽然使用su很方便啦，不过缺点是，当我的主机是多人共管的环境时，如果大家都要使用su来切换成为root的身份，那么不就每个人都得要知道root的密码，这样密码太多人知道可能会流出去。

### 13.4.2 sudo ###

sudo的执行则仅需要自己的密码即可！甚至可以设定不需要密码即可执行sudo呢！

并非所有人都能够执行sudo ，而是仅有规范到`/etc/sudoers`内的用户才能够执行sudo这个指令。

- sudo 的指令用法

		sudo [-b] [-u新使用者帐号]
		选项与参数：
			-b ：将后续的指令放到背景中让系统自行执行，而不与目前的shell 产生影响
			-u ：后面可以接欲切换的使用者，若无此项则代表切换身份为root 。
		
		[root@study ~]# sudo -u sshd touch /tmp/mysshd
		我们的root 使用sshd 的权限去进行某项任务！要注意，因为我们无法使用『 su - sshd 』去切换系统帐号(因为系统帐号的shell 是/sbin/nologin)， 这个时候sudo 真是他X 的好用了！

		sh -c '指令串'
		
		sudo的执行流程：
		1、当使用者执行sudo 时，系统于/etc/sudoers 档案中搜寻该使用者是否有执行sudo 的权限；
		2、若使用者具有可执行sudo 的权限后，便让使用者『输入使用者自己的密码』来确认；
		3、若密码输入成功，便开始进行sudo 后续接的指令(但root 执行sudo 时，不需要输入密码)；
		4、若欲切换的身份与执行者身份相同，那也不需要输入密码。

		能否使用sudo必须要看/etc/sudoers的设定值，而可使用sudo者是透过输入使用者自己的密码来执行后续的指令串

- visudo 与/etc/sudoers

		除了root之外的其他帐号，若想要使用sudo执行属于root的权限指令，则root需要先使用visudo去修改/etc/sudoers ，让该帐号能够使用全部或部分的root指令功能。
		I. 单一使用者可进行root 所有指令，与sudoers 档案语法
		root ALL=(ALL) ALL   <==找到这一行，大约在98行左右
		vbird1 ALL=(ALL) ALL   <==这一行是你要新增的！

		使用者帐号 登入者的来源主机名称=(可切换的身份) 可下达的指令
		root ALL=(ALL) ALL    <==这是预设值

		『使用者帐号』：系统的哪个帐号可以使用sudo 这个指令的意思；
		『登入者的来源主机名称』：当这个帐号由哪部主机连线到本Linux 主机，意思是这个帐号可能是由哪一部网路主机连线过来的， 这个设定值可以指定用户端电脑(信任的来源的意思)。预设值root 可来自任何一部网路主机
		『(可切换的身份)』：这个帐号可以切换成什么身份来下达后续的指令，预设root 可以切换成任何人；
		『可下达的指令』：可用该身份下达什么指令？这个指令请务必使用绝对路径撰写。预设root可以切换任何身份且进行任何指令之意。
		
		II. 利用wheel 群组以及免密码的功能处理visudo
		%wheel ALL=(ALL) ALL  <==大约在106行左右，请将这行的#拿掉！
		#在最左边加上% ，代表后面接的是一个『群组』之意！改完请储存后离开

		不需要密码即可使用sudo：
		%wheel ALL=(ALL) NOPASSWD: ALL  <==大约在109行左右，请将#拿掉！

		III. 有限制的指令操作：
		myuser1 ALL=(root) /usr/bin/passwd   <==最后指令务必用绝对路径 
		如果使用上面的设定，myuser1可以使用 sudo passwd修改root的密码，解决方案如下：
		myuser1 ALL=(root) !/usr/bin/passwd, /usr/bin/passwd [A-Za-z]*, !/usr/bin/ passwd root     # !代表不可执行

		IV. 透过别名建置visudo：
		visudo 的别名可以是『指令别名、帐号别名、主机别名』等
		User_Alias ADMPW = pro1, pro2, pro3, myuser1, myuser2
		Cmnd_Alias ADMPWCOM = !/usr/bin/passwd, /usr/bin/passwd [A-Za-z]*, !/usr/bin/passwd root
		ADMPW ALL=(root) ADMPWCOM
		我透过User_Alias 建立出一个新帐号，这个帐号名称一定要使用大写字元来处理，包括Cmnd_Alias(命令别名)、Host_Alias(来源主机名称别名)都需要使用大写字元的！

		V. sudo 的时间间隔问题：
		如果我使用同一个帐号在短时间内重复操作sudo 来运作指令的话， 在第二次执行sudo 时，并不需要输入自己的密码！sudo 还是会正确的运作喔！不过如果两次sudo操作的间隔超过5分钟，那就得要重新输入一次你的密码了。

		VI. sudo 搭配su 的使用方式：
		使用sudo 搭配su ， 一口气将身份转为root ，而且还用使用者自己的密码：
		User_Alias ADMINS = pro1, pro2, pro3, myuser1
		ADMINS ALL=(root) /bin/su -
		上述的pro1, pro2, pro3, myuser1这四个人，只要输入『 sudo su - 』并且输入『自己的密码』后，立刻变成root的身份！不但root密码不会外流，使用者的管理也变的非常方便！
		
## 13.5.1 使用者的特殊shell 与PAM 模组 ##

系统帐号是不需要登入的。我们所谓的『`无法登入`』指的仅是：『`这个使用者无法使用bash 或其他shell 来登入系统`』而已， 并不是说​​这个帐号就无法使用其他的系统资源喔！举例来说，各个系统帐号，列印工作由lp 这个帐号在管理， WWW 服务由apache 这个帐号在管理， 他们都可以进行系统程序的工作，但是『`就是无法登入主机取得互动的shell`』而已啦！

可以建立『`/etc/nologin.txt`』这个档案，并且在这个档案内说明不能登入的原因，那么下次当这个使用者想要登入系统时，萤幕上出现的就会是/etc/nologin.txt这个档案的内容，而不是预设的内容了！		

### 13.5.2 PAM 模组简介 ###

PAM可以说是一套应用程式介面(Application Programming Interface, API)，他提供了一连串的验证机制，只要使用者将验证阶段的需求告知PAM后， PAM就能够回报使用者验证的结果(成功或失败)。

PAM 模组与其他程式的相关性:  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/pam-1.gif)

PAM用来进行验证的资料称为模组(Modules)，每个PAM模组的功能都不太相同。

### 13.5.3 PAM 模组设定语法 ###

PAM 藉由一个`与程式相同档名的设定档`来进行一连串的认证分析需求。我们同样以passwd 这个指令的呼叫PAM 来说明好了。当你执行passwd 后，这支程式呼叫PAM 的流程是：

	1、使用者开始执行/usr/bin/passwd 这支程式，并输入密码；
	2、passwd 呼叫PAM 模组进行验证；
	3、PAM 模组会到/etc/pam.d/ 找寻与程式(passwd) 同名的设定档；
	4、依据/etc/pam.d/passwd 内的设定，引用相关的PAM 模组逐步进行验证分析；
	5、将验证结果(成功、失败以及其他讯息) 回传给passwd 这支程式；
	6、passwd 这支程式会根据PAM 回传的结果决定下一个动作(重新输入新密码或者通过验证！)
	
	[root@centos ~]# cat /etc/pam.d/passwd
	#%PAM-1.0     # 版本宣告  其它任何以#开头的都是注解
	验证类型(type)	控制标准(flag)	PAM的模块于该模块的参数
	auth       		include      	system-auth
	account    		include      	system-auth
	password   		substack     	system-auth
	-password   	optional    	pam_gnome_keyring.so use_authtok
	password   		substack     	postlogin
	
	include：代表的是『请呼叫后面的档案来作为这个类别的验证』，所以，上述的每一行都要重复呼叫/etc/pam.d/system-auth 那个档案来进行验证的意思！

	第一个栏位：验证类别(Type)：
		auth：
			是authentication (认证)的缩写，所以这种类别主要用来检验使用者的身份验证，
			这种类别通常是需要密码来检验的，所以后续接的模组是用来检验使用者的身份。
		account：
			account (帐号)则大部分是在进行authorization (授权)，这种类别则主要在检验使用者是否具有正确的使用权限，
			举例来说，当你使用一个过期的密码来登入时，当然就无法正确的登入了。
		session:
			session是会议期间的意思，所以session管理的就是使用者在这次登入(或使用这个指令)期间，PAM所给予的环境设定。
			这个类别通常用在记录使用者登入与登出时的资讯！例如，如果你常常使用su或者是sudo指令的话，
			那么应该可以在/var/log/secure里面发现很多关于pam的说明，而且记载的资料是『session open, session close』的资讯！
		password:
			password就是密码嘛！所以这种类别主要在提供验证的修订工作，举例来说，就是修改/变更密码啦！
		
		这四个验证的类型通常是有顺序的，不过也有例外就是了。会有顺序的原因是，(1)我们总是得要先验证身份(auth)后， 
		(2)系统才能够藉由使用者的身份给予适当的授权与权限设定(account)，而且(3)登入与登出期间的环境才需要设定，
		也才需要记录登入与登出的资讯(session)。如果在运作期间需要密码修订时，(4)才给予password的类别。这样说起来，自然是需要有点顺序吧！
	
	第二个栏位：验证的控制旗标(control flag):
		简单的说，他就是『验证通过的标准』啦！这个栏位在管控该验证的放行方式，主要也分为四种控制方式：
		required:
			此验证若成功则带有success (成功)的标志，若失败则带有failure的标志，但不论成功或失败都会继续后续的验证流程。
			由于后续的验证流程可以继续进行，因此相当有利于资料的登录(log) ，这也是PAM最常使用required的原因。	
		requisite:
			若验证失败则立刻回报原程式failure的标志，并终止后续的验证流程。若验证成功则带有success的标志并继续后续的验证流程。
			这个项目与required最大的差异，就在于失败的时候还要不要继续验证下去？由于requisite是失败就终止，因此失败时所产生的PAM资讯就无法透过后续的模组来记录了。
		sufficient:
			若验证成功则立刻回传success给原程式，并终止后续的验证流程；若验证失败则带有failure标志并继续后续的验证流程。这玩意儿与requisits刚好相反！
		optional:
			这个模组控制项目大多是在显示讯息而已，并不是用在验证方面的。

PAM 控制旗标所造成的回报流程:  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/pam-2.gif)

### 13.5.4 常用模组简介 ###

	/etc/pam.d/*：每个程式个别的PAM 设定档；
	/lib64/security/*：PAM 模组档案的实际放置目录；
	/etc/security/*：其他PAM 环境的设定档；
	/usr/share/doc/pam-*/：详细的PAM 说明文件。

	常见模块：
	pam_securetty.so：限制系统管理员(root)只能够从安全的(secure)终端机登入。安全的终端机设定写在/etc/securetty这个档案中。
	pam_nologin.so：这个模组可以限制一般使用者是否能够登入主机之用。当/etc/nologin这个档案存在时，则所有一般使用者均无法再登入系统了！注意喔！这与/etc/nologin.txt并不相同！
	pam_selinux.so：SELinux是个针对程序来进行细部管理权限的功能。
	pam_loginuid.so：验证使用者的uid
	pam_env.so：用来设定环境变数的一个模组，如果你有需要额外的环境变数设定，可以参考/etc/security/pam_env.conf这个档案的详细说明。
	pam_unix.so：可以用在认证、授权、会议管理、密码更新阶段
	pam_pwquality.so：可以用来检验密码的强度！同时提供了/etc/security/pwquality.conf这个档案可以额外指定预设值！
	pam_limits.so：更多细部的设定可以参考： /etc/security/limits.conf内的说明。

为什么root 无法以telnet 直接登入系统，但是却能够使用ssh 直接登入？
	
一般来说， telnet会引用login的PAM模组，而login的验证阶段会有/etc/securetty的限制！由于远端连线属于pts/n (n为数字)的动态终端机介面装置名称，并没有写入到/etc/securetty ，因此root无法以telnet登入远端主机。至于ssh使用的是/etc/pam.d/sshd这个模组，你可以查阅一下该模组，由于该模组的验证阶段并没有加入pam_securetty ，因此就没有/etc/securetty的限制！故可以从远端直接连线到伺服器端。	
	
### 13.5.5 其他相关档案 ###

limits.conf：

	[root@study ~]# vim /etc/security/limits.conf
	vbird1 soft fsize 90000
	vbird1 hard fsize 100000
	# 帐号限制依据限制项目限制值
	# 第一栏位为帐号，或者是群组！若为群组则前面需要加上@ ，例如@projecta
	# 第二栏位为限制的依据，是严格(hard)，还是仅为警告(soft)；
	# 第三栏位为相关限制，此例中限制档案容量，
	# 第四栏位为限制的值，在此例中单位为KB。
	# 若以vbird1 登入后，进行如下的操作则会有相关的限制出现！	

	[vbird1@study ~]$ ulimit -a 
	....(前面省略).... 
	file size (blocks, -f) 90000 
	....(后面省略)....
	
	[vbird1@study ~]$ dd if=/dev/zero of=test bs=1M count=110
	File size limit exceeded
	[vbird1@study ~]$ ll --block-size=K test 
	-rw-rw-r--. 1 vbird1 vbird1 90000K Jul 22 01:33 test
	 #果然有限制到了
	
	范例二：限制pro1这个群组，每次仅能有一个使用者登入系统(maxlogins) 
	[root@study ~]# vim /etc/security/limits.conf 
	@pro1 hard maxlogins 1
	 #如果要使用群组功能的话，这个功能似乎对初始群组才有效喔！而如果你尝试多个pro1的登入时，
	# 第二个以后就无法登入了。而且在/var/log/secure 档案中还会出现如下的资讯：
	# pam_limits(login:session): Too many logins (max 1) for pro1

/var/log/secure, /var/log/messages

	PAM模组都会将资料记载在/var/log/secure当中

## 13.6 Linux 主机上的使用者讯息传递 ##

### 13.6.1 查询使用者： w, who, last, lastlog ###	

	last：查询使用者的最近登录情况
	lastlog:查询使用者最近登录的时间，lastlog会读取/var/log/lastlog

### 13.6.2 使用者对谈： write, mesg, wall ###

	write 使用者帐号 [使用者所在终端介面]
	[root@study ~]# write vbird1 pts/2 
	Hello, there:
	Please don't do anything wrong...   <==这两行是root写的资讯！
	#结束时，请按下[ctrl]-d来结束输入。此时在vbird1的画面中，会出现：

	mesg [y|n]
	是否接受信息，对于root的信息，如果是n，还是要接收

	对所有系统上面的使用者传送简讯(广播)
	
	wall "I will shutdown my linux server..."

### 13.6.3 使用者邮件信箱： mail ###

	mail -s "邮件标题" username@localhost
	[root@study ~]# mail -s "nice to meet you" vbird1     # 如果是本机，可以不用写@localhost
	Hello, DM Tsai
	Nice to meet you in the network.
	You are so nice. byebye!
	.     <==这里很重要喔，结束时，最后一行输入小数点.即可！
	EOT	
	[root@study ~]#   <==出现提示字元，表示输入完毕了！

mail快捷键：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/13/mail-hot-key.jpg)	


## 13.7 CentOS 7 环境下大量建置帐号的方法 ##

	pwck:
	pwck 这个指令在检查/etc/passwd 这个帐号设定档内的资讯，与实际的家目录是否存在等资讯， 
	还可以比对/etc/passwd /etc/shadow 的资讯是否一致，另外，如果/etc/ passwd 内的资料栏位错误时，会提示使用者修订。

	pwconv:
	这个指令主要的目的是在『将/etc/passwd 内的帐号与密码，移动到/etc/shadow 当中！』
	
	pwunconv:
	将/etc/shadow 内的密码栏资料写回/etc/passwd 当中， 并且删除/etc/shadow 档案。

	chpasswd:
	『读入未加密前的密码，并且经过加密后，​​ 将加密后的密码写入/etc/shadow 当中。』
	这个指令很常被使用在大量建置帐号的情况中喔！他可以由Standard input 读入资料，每笔资料的格式是『 username:password 』。

	CentOS使用SHA512加密，可以去/etc/login.defs查看
	
### 13.7.2 大量建置帐号范本(适用passwd --stdin 选项) ###


## 13.8 重点回顾 ##

暂时让用户不能登录的几种方法：
	
	1、将/etc/passwd 的shell 栏位写成/sbin/nologin ，即可让该帐号暂时无法登入主机；
	2、将/etc/shadow 内的密码栏位，增加一个* 号在最前面，这样该帐号亦无法登入！
	3、将/etc/shadow 的第八个栏位关于帐号取消日期的那个，设定小于目前日期的数字，那么他就无法登入系统了！
	
		


		

		
		





