# 6.1 目录与路径 #

## 6.1.1 相对路径与绝对路径 ##

	绝对路径：路径的写法『一定由根目录/写起』
	相对路径：路径的写法『不是由/写起』 

建议在写程序（shell scripts）来管理系统的条件下，建议使用绝对路径的写法。

## 6.1.2 目录的相关操作 ##

	. 代表此层目录
	.. 代表上一层目录
	- 代表前一个工作目录
	~ 代表『目前使用者身份』所在的家目录
	~account 代表account 这个使用者的家目录(account是个帐号名称)
	根目录的上一层(..)与根目录自己(.)是同一个目录

	cd：变换目录
	pwd：显示目前的目录
	mkdir：建立一个新的目录
	rmdir：删除一个空的目录

	cd [相对路径或绝对路径]
	
	pwd [-P]
	选项与参数：
		-P ：显示出确实的路径，而非使用连结(link) 路径。

	mkdir [-mp]目录名称
	选项与参数：
		-m ：设定档案的权限喔！直接设定，不需要看预设权限(umask) 的脸色～
		-p ：帮助你直接将所需要的目录(包含上层目录)递回建立起来！
	
	rmdir [-p]目录名称（只能删除空的目录）
	选项与参数：
		-p ：连同『上层』『空的』目录也一起删除

	
## 6.1.3 关于执行档路径的变数： $PATH ##

	不同身份使用者预设的PATH不同，预设能够随意执行的指令也不同(如root与dmtsai)；
	PATH是可以修改的；
	使用绝对路径或相对路径直接指定某个指令的档名来执行，会比搜寻PATH来的正确；
	指令应该要放置到正确的目录下，执行才会比较方便；
	本目录(.)最好不要放到PATH当中。
	root的PATH：/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
	
# 6.2 档案与目录管理 #

## 6.2.1 档案与目录的检视： ls ##

	ls [-aAdfFhilnrRSt]档名或目录名称.. 
	ls [--color={never,auto,always}]档名或目录名称.. 
	ls [--full-time]档名或目录名称.. 
	选项与参数：
		-a ：全部的档案，连同隐藏档(开头为.的档案)一起列出来(常用)
		-A ：全部的档案，连同隐藏档，但不包括. 与.. 这两个目录
		-d ：仅列出目录本身，而不是列出目录内的档案资料(常用)
		-f ：直接列出结果，而不进行排序(ls 预设会以档名排序！)
		-F ：根据档案、目录等资讯，给予附加资料结构，例如：
		      *:代表可执行档； /:代表目录； =:代表socket 档案； |:代表FIFO 档案；
		-h ：将档案容量以人类较易读的方式(例如GB, KB 等等)列出来；
		-i ：列出inode 号码，inode 的意义下一章将会介绍；
		-l ：长资料串列出，包含档案的属性与权限等等资料；(常用)
		-n ：列出UID 与GID 而非使用者与群组的名称(UID与GID会在帐号管理提到！)
		-r ：将排序结果反向输出，例如：原本档名由小到大，反向则为由大到小；
		-R ：连同子目录内容一起列出来，等于该目录下的所有档案都会显示出来；
		-S ：以档案容量大小排序，而不是用档名排序；
		-t ：依时间排序，而不是用档名。
		--color=never ：不要依据档案特性给予颜色显示；
		--color=always ：显示颜色
		--color=auto ：让系统自行依据设定来判断是否给予颜色
		--full-time ：以完整时间模式(包含年、月、日、时、分) 输出
		--time={atime,ctime} ：输出access 时间或改变权限属性时间(ctime) 
		                       而非内容变更时间(modification time)

## 6.2.2 复制、删除与移动： cp, rm, mv ##

	cp [-adfilprsu]来源档(source)目标档(destination) 
	cp [options] source1 source2 source3 .... directory
	选项与参数：
		-a ：相当于-dr --preserve=all的意思，至于dr请参考下列说明；(常用)
		-d ：若来源档为连结档的属性(link file)，则复制连结档属性而非档案本身；
		-f ：为强制(force)的意思，若目标档案已经存在且无法开启，则移除后再尝试一次；
		-i ：若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
		-l ：进行硬式连结(hard link)的连结档建立，而非复制档案本身；
		-p ：连同档案的属性(权限、用户、时间)一起复制过去，而非使用预设属性(备份常用)；
		-r ：递回持续复制，用于目录的复制行为；(常用)
		-s ：复制成为符号连结档(symbolic link)，亦即『捷径』档案；
		-u ：destination 比source 旧才更新destination，或destination 不存在的情况下才复制。
		--preserve=all ：除了-p 的权限相关参数外，还加入SELinux 的属性, links, xattr 等也复制了。
		最后需要注意的，如果来源档有两个以上，则最后一个目的档一定要是『目录』才行！

	在预设的条件中， cp的来源档与目的档的权限是不同的，目的档的拥有者通常会是指令操作者本身。
	

	[root@study ~]# cd /tmp 
	[root@study tmp]# cp /var/log/wtmp .
	[root@study tmp]# ls -l /var/log/wtmp wtmp 
	-rw-rw-r-- . 1 root utmp 28416 Jun 11 18: 56 /var/log/wtmp
	-rw-r--r-- . 1 root root 28416 Jun 11 19:01 wtmp
	# 注意上面的特殊字体，在不加任何选项的情况下，档案的某些属性/权限会改变；
	# 这是个很重要的特性！要注意喔！还有，连档案建立的时间也不一样了！
	# 那如果你想要将档案的所有特性都一起复制过来该怎办？可以加上-a 喔！如下所示：

	[root@study tmp]# cp -a /var/log/wtmp wtmp_2 
	[root@study tmp]# ls -l /var/log/wtmp wtmp_2
	-rw-rw-r--. 1 root utmp 28416 Jun 11 18:56 /var/log/wtmp
	-rw-rw-r--. 1 root utmp 28416 Jun 11 18:56 wtmp_2
	# 了了吧！整个资料特性完全一模一样ㄟ！真是不赖～这就是-a 的特性！
	
	[dmtsai@study ~]$ cp -a /var/log/wtmp /tmp/dmtsai_wtmp 
	[dmtsai@study ~]$ ls -l /var/log/wtmp /tmp/dmtsai_wtmp 
	-rw-rw-r--. 1 dmtsai dmtsai 28416 6月11 18:56 /tmp/dmtsai_wtmp
	-rw-rw-r--. 1 root utmp   28416 6月11 18:56 /var/log/wtmp
	由于dmtsai 的身份并不能随意修改档案的拥有者与群组，因此虽然能够复制wtmp的相关权限与时间等
	属性， 但是与拥有者、群组相关的，原本dmtsai 身份无法进行的动作，即使加上-a 选项，也是无法
	达成完整复制权限的！

cp复制测试图：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/06/cp-test.jpg)

从上图可以看出不加`-a`选项的时候，目的档的权限=来源档-umask。

	rm [-fir]档案或目录
	选项与参数：
		-f ：就是force 的意思，忽略不存在的档案，不会出现警告讯息；
		-i ：互动模式，在删除前会询问使用者是否动作
		-r ：递回删除啊！最常用在目录的删除了！这是非常危险的选项！！！
	root身份，预设已经加入了-i 的选项。
	\rm -r /tmp/etc 
		在指令前加上反斜线，可以忽略掉alias的指定选项喔！

	mv [-fiu] source destination
	mv [options] source1 source2 source3 .... directory
	选项与参数：
		-f ：force 强制的意思，如果目标档案已经存在，不会询问而直接覆盖；
		-i ：若目标档案(destination) 已经存在时，就会询问是否覆盖！
		-u ：若目标档案已经存在，且source 比较新，才会更新(update)

## 6.2.3 取得路径的档案名称与目录名称 ##
	basename /etc/sysconfig/network 
		network
	dirname /etc/sysconfig/network
		/etc/sysconfig

# 6.3 档案内容查阅 #

	cat 由第一行开始显示档案内容
	tac 从最后一行开始显示，可以看出tac 是cat 的倒着写！
	nl 显示的时候，顺道输出行号！
	more 一页一页的显示档案内容
	less 与more 类似，但是比more 更好的是，他可以往前翻页！
	head 只看头几行
	tail 只看尾巴几行
	od 以二进位的方式读取档案内容！

## 6.3.1 直接检视档案内容 ##

	cat [-AbEnTv]  （Concatenate）
	选项与参数：
		-A ：相当于-vET 的整合选项，可列出一些特殊字符而不是空白而已；
		-b ：列出行号，仅针对非空白行做行号显示，空白行不标行号！
		-E ：将结尾的断行字元$ 显示出来；
		-n ：列印出行号，连同空白行也会有行号，与-b 的选项不同；
		-T ：将[tab] 按键以^I 显示出来；
		-v ：列出一些看不出来的特殊字符

	tac /etc/issue  （反向显示）

	nl [-bnw]档案	
	选项与参数：
		-b ：指定行号指定的方式，主要有两种：
		      -ba ：表示不论是否为空行，也同样列出行号(类似cat -n)；
		      -bt ：如果有空行，空的那一行不要列出行号(预设值)；
		-n ：列出行号表示的方法，主要有三种：
		      -n ln ：行号在萤幕的最左方显示；
		      -n rn ：行号在自己栏位的最右方显示，且不加0 ；
		      -n rz ：行号在自己栏位的最右方显示，且加0 ；
		-w ：行号栏位的占用的字元数。

## 6.3.2 可翻页检视 ##

	more (一页一页翻动)
		空白键(space)：代表向下翻一页；
		Enter ：代表向下翻『一行』；
		/字串 ：代表在这个显示的内容当中，向下搜寻『字串』这个关键字；
		:f ：立刻显示出档名以及目前显示的行数；
		q ：代表立刻离开more ，不再显示该档案内容。
		b 或[ctrl]-b ：代表往回翻页，不过这动作只对档案有用，对管线无用。
	
	less (一页一页翻动)
		空白键 ：向下翻动一页；
		[pagedown]：向下翻动一页；
		[pageup] ：向上翻动一页；
		/字串 ：向下搜寻『字串』的功能；
		?字串 ：向上搜寻『字串』的功能；
		n ：重复前一个搜寻(与/ 或? 有关！)
		N ：反向的重复前一个搜寻(与/ 或? 有关！)
		g ：前进到这个资料的第一行去；
		G ：前进到这个资料的最后一行去(注意大小写)；
		q ：离开less 这个程式；

## 6.3.3 资料撷取 ##

	head [-n number]档案
	选项与参数：
		-n ：后面接数字，代表显示几行的意思
	head -n 20 /etc/man_db.conf  		man_db.conf一共131行
		显示前20行（1-20）
	head -n -100 /etc/man_db.conf
		后面100行的资料都不列印，只列印/etc/man_db.conf的前面几行（1-31）

	tail [-n number]档案
	选项与参数：
		-n ：后面接数字，代表显示几行的意思
		-f ：表示持续侦测后面所接的档名，要等到按下[ctrl]-c才会结束tail的侦测

	tail -n 20 /etc/man_db.conf
		显示最后的20行(112-131)
	tail -n +100 /etc/man_db.conf （从100行开始）
		列出100行以后的资料(100-131)
	tail -f /var/log/messages
		持续侦测/var/log/messages的内容 

## 6.3.4 非纯文字档： od ##

	od [-t TYPE]档案
	选项或参数：
		-t ：后面可以接各种『类型(TYPE)』的输出，例如：
		a ：利用预设的字元来输出；
		c ：使用ASCII 字元来输出
		d[size] ：利用十进位(decimal)来输出资料，每个整数占用size bytes ；
		f[size] ：利用浮点数值(floating)来输出资料，每个数占用size bytes ；
		o[size] ：利用八进位(octal)来输出资料，每个整数占用size bytes ；
		x[size] ：利用十六进位(hexadecimal)来输出资料，每个整数占用size bytes ；
	
	将/etc/issue这个档案的内容以8进位列出储存值与ASCII的对照表
		od -t oCc /etc/issue
	如果对纯文字档使用这个指令，你甚至可以发现到ASCII 与字元的对照表！
		echo abc | od -t dCc

## 6.3.5 修改档案时间或建置新档： touch ##

	modification time (mtime)：
		当该档案的『内容资料』变更时，就会更新这个时间！内容资料指的是档案的内容，而不是档案的属性或权限喔！
	
	status time (ctime)：
		当该档案的『状态(status)』改变时，就会更新这个时间，举例来说，像是权限与属性被更改了，都会更新这个时间啊。 
	
	access time (atime)：
		当『该档案的内容被取用』时，就会更新这个读取时间(access)。举例来说，我们使用cat去读取/etc/man_db.conf ，就会更新该档案的atime了。	

	在预设的情况下，ls显示出来的是该档案的mtime ，也就是这个档案的内容上次被更动的时间。

	touch [-acdmt]档案
	选项与参数：
		-a ：仅修订access time；
		-c ：仅修改档案的时间，若该档案不存在则不建立新档案；
		-d ：后面可以接欲修订的日期而不用目前的日期，也可以使用--date="日期或时间"
		-m ：仅修改mtime ；
		-t ：后面可以接欲修订的时间而不用目前的时间，格式为[YYYYMMDDhhmm

使用touch只能修改atime和mtime，不能修改ctime，ctime由系统自动更改。

# 6.4 档案与目录的预设权限与隐藏权限 #

## 6.4.1 档案预设权限：umask ##

umask就是指定『目前使用者在建立档案或目录时候的权限预设值需要减掉的权限！』

	umask
	umask -S
	umask 002

	若使用者建立为『档案』则预设『没有可执行( x )权限』，亦即只有rw这两个项目，也就是最大为666分，预设权限如下：
	-rw-rw-rw-
	
	若使用者建立为『目录』，则由于x与是否可以进入此目录有关，因此预设为所有权限均开放，亦即为777分，预设权限如下：
	drwxrwxrwx

	root的umask为022，所以root
		建立档案时：(-rw-rw-rw-) - (-----w--w-) ==> -rw-r--r--
		建立目录时：(drwxrwxrwx) - (d----w--w-) ==> drwxr-xr-x
	
关于预设umask的设定可以参考`/etc/bashrc`这个档案的内容，不过，不建议修改该档案，可以参考第十章bash shell提到的环境参数设定档 `(~/.bashrc)`的说明！

## 6.4.2 档案隐藏属性 ##

不过要先强调的是，底下的chattr指令只能在Ext2/Ext3/Ext4的Linux传统档案系统上面完整生效，其他的档案系统可能就无法完整的支援这个指令。

	chattr [+-=][ASacdistu]档案或目录名称
	选项与参数：
		+ ：增加某一个特殊参数，其他原本存在参数则不动。
		- ：移除某一个特殊参数，其他原本存在参数则不动。
		= ：设定一定，且仅有后面接的参数
		
		A ：当设定了A 这个属性时，若你有存取此档案(或目录)时，他的存取时间atime 将不会被修
			改，可避免I/O 较慢的机器过度的存取磁碟。(目前建议使用档案系统挂载参数处理这个项目)
		S ：一般档案是非同步写入磁碟的(原理请参考前一章sync的说明)，如果加上S这个属性时，
		    当你进行任何档案的修改，该更动会『同步』写入磁碟中。
		a ：当设定a 之后，这个档案将只能增加资料，而不能删除也不能修改资料，只有root才能设定这属性
		c ：这个属性设定之后，将会自动的将此档案『压缩』，在读取的时候将会自动解压缩，
		     但是在储存的时候，将会先进行压缩后再储存(看来对于大档案似乎蛮有用的！)
		d ：当dump 程序被执行的时候，设定d 属性将可使该档案(或目录)不会被dump 备份
		i ：这个i 可就很厉害了！他可以让一个档案『不能被删除、改名、设定连结也无法写入或新增资
			料！』对于系统安全性有相当大的助益！只有root 能设定此属性
		s ：当档案设定了s 属性时，如果这个档案被删除，他将会被完全的移除出这个硬碟空间，所以如果误删了，完全无法救回来了喔！
		u ：与s 相反的，当使用u 来设定档案时，如果该档案被删除了，则资料内容其实还存在磁碟中，可以使用来救援该档案喔！
		
		注意1：属性设定常见的是a 与i 的设定值，而且很多设定值必须要身为root 才能设定
		注意2：xfs 档案系统仅支援AadiS 而已

	lsattr [-adR]档案或目录
	选项与参数：
		-a ：将隐藏档的属性也秀出来；
		-d ：如果接的是目录，仅列出目录本身的属性而非目录内的档名；
		-R ：连同子目录的资料也一并列出来！
	
## 6.4.3 档案特殊权限： SUID, SGID, SBIT ##

Set UID：

	当s这个标志出现在档案拥有者的x权限上时，例如刚刚提到的/usr/bin/passwd这个档案的权限状态：
	『-rw s r-xr-x』，此时就被称为Set UID，简称为SUID的特殊权限。

	SUID 权限仅对二进位程式(binary program)有效；
	执行者对于该程式需要具有x 的可执行权限；
	本权限仅在执行该程式的过程中有效(run-time)；
	执行者将具有该程式拥有者(owner) 的权限。

	----------. 1 root root 1374 10月  2 20:19 /etc/shadow
	-rwsr-xr-x. 1 root root 27832 6月  10 2014 /bin/passwd
	
	1、dmtsai 对于/usr/bin/passwd 这个程式来说是具有x 权限的，表示dmtsai 能执行passwd；
	2、passwd 的拥有者是root 这个帐号；
	3、dmtsai 执行passwd 的过程中，会『暂时』获得root 的权限；
	4、/etc/shadow 就可以被dmtsai 所执行的passwd 所修改。

	SUID仅可用在binary program上，不能够用在shell script上面！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/06/centos7_suid.gif)


Set GID：

	s 标志在档案拥有者的x 项目为SUID，那s 在群组的x 时则称为Set GID。

	-rwx--s--x. 1 root slocate 40520 4月  11 11:46 /usr/bin/locate  会去读取mlocate.db
	-rw-r-----. 1 root slocate 4017075 10月  2 21:39 /var/lib/mlocate/mlocate.db

	与SUID 不同的是，SGID 可以针对档案或目录来设定！如果是对档案来说， SGID 有如下的功能：
		SGID 对二进位程式有用；
		程式执行者对于该程式来说，需具备x 的权限；
		执行者在执行的过程中将会获得该程式群组的支援！
	当dmtsai这个帐号去执行locate 时，那dmtsai 将会取得slocate 群组的支援， 因此就能够去读取mlocate.db 啦！
	
	当一个目录设定了SGID 的权限后，他将具有如下的功能：
		使用者若对于此目录具有r 与x 的权限时，该使用者能够进入此目录；
		使用者在此目录下的有效群组(effective group)将会变成该目录的群组；
		用途：若使用者在此目录下具有w 的权限(可以新建档案)，则使用者所建立的新档案，该新档案的群组与此目录的群组相同。

Sticky Bit：
		
	这个Sticky Bit, SBIT 目前只针对目录有效，对于档案已经没有效果了。SBIT 对于目录的作用是：
		当使用者对于此目录具有w, x 权限，亦即具有写入的权限时；
		当使用者在该目录下建立档案或目录时，仅有自己与root 才有权力删除该档案
	
	换句话说：当甲这个使用者于A目录是具有群组或其他人的身份，并且拥有该目录w的权限，这表示『甲使用
	者对该目录内任何人建立的目录或档案均可进行"删除/更名/搬移"等动作。』不过，如果将A目录加上了
	SBIT的权限项目时，则甲只能够针对自己建立的档案或目录进行删除/更名/移动等动作，而无法删除他人的档案。

	drwxrwxrwt. 15 root root 4096 10月  2 21:59 /tmp

SUID/SGID/SBIT 权限设定：
	
	4 为SUID		或		u+s
	2 为SGID		或		g+s
	1 为SBIT		或		o+t 

	chmod 7666 test; ls -l test 
	-rw S rw S rw T 1 root root 0 Jun 16 02:53 test
	
	怎么会出现大写的S与T呢？不都是小写的吗？因为s与t都是取代x这个权限的，但是你有没有发现阿，我们
	是下达7666喔！也就是说， user, group以及others都没有x这个可执行的标志(因为666嘛)，所以，这
	个S, T代表的就是『空的』啦！怎么说？SUID是表示『该档案在执行的时候，具有档案拥有者的权限』，但
	是档案拥有者都无法执行了，哪里来的权限给其他人使用？当然就是空的啦！

## 6.4.4 观察档案类型：file ##

	file [file]

# 6.5 指令与档案的搜寻 #

## 6.5.1 指令档名的搜寻 ##

	which [-a] command
	选项或参数：
		-a ：将所有由PATH 目录中可以找到的指令均列出，而不止第一个被找到的指令名称

	这个指令是根据『PATH』这个环境变数所规范的路径，去搜寻『执行档』的档名～所以，重点是找出『执行
	档』而已！且which后面接的是『完整档名』喔！

	type command   也是通过PATH来搜寻

## 6.5.2 档案档名的搜寻 ##
	
	whereis 只找系统中某些特定目录底下的档案而已，locate 则是利用资料库来搜寻档名，当然两者就相当
	的快速， 并且没有实际的搜寻硬碟内的档案系统状态，比较省时间啦！

	whereis [-bmsu]档案或目录名
	选项与参数：
		-l :可以列出whereis 会去查询的几个主要目录而已
		-b :只找binary 格式的档案
		-m :只找在说明档manual 路径下的档案
		-s :只找source 来源档案
		-u :搜寻不在上述三个项目当中的其他特殊档案

	whereis 主要是针对/bin/sbin 底下的执行档， 以及/usr/share/man 底下的man page 档案，跟几个比较特定的目录来处理而已。所以速度当然快的多！

	locate [-ir] keyword 
	选项与参数：
		-i ：忽略大小写的差异；
		-c ：不输出档名，仅计算找到的档案数量
		-l ：仅输出几行的意思，例如输出五行则是-l 5
		-S ：输出locate 所使用的资料库档案的相关资讯，包括该资料库纪录的档案/目录数量等
		-r ：后面可接正规表示法的显示方式
	
	updatedb：根据/etc/updatedb.conf 的设定去搜寻系统硬碟内的档名，并更新/var/lib/mlocate 内的资料库档案；
	locate：依据/var/lib/mlocate 内的资料库记载，找出使用者输入的关键字档名。

	
	find [PATH] [option] [action]
	选项与参数：
	1. 与时间有关的选项：共有-atime, -ctime 与-mtime ，以-mtime 说明
	   -mtime n ：n 为数字，意义为在n 天之前的『一天之内』被更动过内容的档案；
	   -mtime +n ：列出在n 天之前(不含n 天本身)被更动过内容的档案档名；
	   -mtime -n ：列出在n 天之内(含n 天本身)被更动过内容的档案档名。
	   -newer file ：file 为一个存在的档案，列出比file 还要新的档案档名

find相关时间参数：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/06/find_time.gif)

	图中最右边为目前的时间，越往左边则代表越早之前的时间轴啦。由图可以清楚的知道：
	
	+4代表大于等于5天前的档名：ex> find /var -mtime +4
	-4代表小于等于4天内的档案档名：ex> find /var -mtime -4
	4则是代表4-5那一天的档案档名：ex> find /var -mtime 4
	
	2. 与使用者或群组名称有关的参数：
	   -uid n ：n 为数字，这个数字是使用者的帐号ID，亦即UID ，这个UID 是记录在
	            /etc/passwd 里面与帐号名称对应的数字。这方面我们会在第四篇介绍。
	   -gid n ：n 为数字，这个数字是群组名称的ID，亦即GID，这个GID 记录在
	            /etc/group，相关的介绍我们会第四篇说明～
	   -user name ：name 为使用者帐号名称喔！例如dmtsai
	   -group name：name 为群组名称喔，例如users ；
	   -nouser ：寻找档案的拥有者不存在/etc/passwd 的人！
	   -nogroup ：寻找档案的拥有群组不存在于/etc/group 的档案！
	                当你自行安装软体时，很可能该软体的属性当中并没有档案拥有者，
	                这是可能的！在这个时候，就可以使用-nouser 与-nogroup 搜寻。

	3. 与档案权限及名称有关的参数：
	   -name filename：搜寻档案名称为filename 的档案；
	   -size [+-]SIZE：搜寻比SIZE 还要大(+)或小(-)的档案。这个SIZE 的规格有：
	                   c: 代表byte， k: 代表1024bytes。所以，要找比50KB
	                   还要大的档案，就是『 -size +50k 』
	   -type TYPE ：搜寻档案的类型为TYPE 的，类型主要有：一般正规档案(f), 装置档案(b, c),
	                   目录(d), 连结档(l), socket (s), 及FIFO (p) 等属性。
	   -perm mode ：搜寻档案权限『刚好等于』 mode 的档案，这个mode 为类似chmod
	                 的属性值，举例来说， -rwsr-xr-x 的属性为4755 ！
	   -perm -mode ：搜寻档案权限『必须要全部囊括mode 的权限』的档案，举例来说，
	                 我们要搜寻-rwxr--r-- ，亦即0744 的档案，使用-perm -0744，
	                 当一个档案的权限为-rwsr-xr-x ，亦即4755 时，也会被列出来，
	                 因为-rwsr-xr-x 的属性已经囊括了-rwxr--r-- 的属性了。
	   -perm /mode ：搜寻档案权限『包含任一mode 的权限』的档案，举例来说，我们搜寻
	                 -rwxr-xr-x ，亦即-perm /755 时，但一个档案属性为-rw-------
	                 也会被列出来，因为他有-rw.... 的属性存在！

	find本来就会搜寻次目录，这个特色也要特别注意喔！

	4. 额外可进行的动作：
	   -exec command ：command 为其他指令，-exec 后面可再接额外的指令来处理搜寻到的结果。
	   -print ：将结果列印到萤幕上，这个动作是预设动作！
	
	find /usr/bin /usr/sbin -perm /7000 -exec ls -l {} \; 

find相关额外动作：  
![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/06/centos7_find_exec.gif)

	该范例中特殊的地方有{} 以及\; 还有-exec 这个关键字，这些东西的意义为：

	{} 代表的是『由find 找到的内容』，如上图所示，find 的结果会被放置到{} 位置中；
	-exec 一直到\; 是关键字，代表find 额外动作的开始(-exec) 到结束(\;) ，在这中间的就是find 指令内的额外动作。在本例中就是『 ls -l {} 』啰！
	因为『 ; 』在bash 环境下是有特殊意义的，因此利用反斜线来跳脱。
	


习题：

	找出/etc 底下，档案大小介于50K 到60K 之间的档案，并且将权限完整的列出(ls -l)：
	find /etc -size +50k -a -size -60k -exec ls -l {} \; 
	注意到-a ，那个-a是and的意思，为符合两者才算成功

	找出/etc 底下，档案容量大于50K 且档案所属人不是root 的档名，且将权限完整的列出(ls -l)；
	find /etc -size +50k -a ! -user root -exec ls -ld {} \; 
	find /etc -size +50k -a ! -user root -type f -exec ls -l {} \; 
	上面两式均可！注意到! ，那个!代表的是反向选择，亦即『不是后面的项目』之意！

	找出/etc 底下，容量大于1500K 以及容量等于0 的档案：
	find /etc -size +1500k -o -size 0 
	相对于-a ，那个-o就是或(or)的意思啰！

	
