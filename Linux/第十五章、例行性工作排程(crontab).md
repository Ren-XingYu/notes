# 第十五章、例行性工作排程(crontab) #

## 15.1 什么是例行性工作排程 ##

Linux 的例行性工作是如何进行排程的呢？所谓的排程就是将这些工作安排执行的流程之意！

### 15.1.1 Linux 工作排程的种类： at, cron ###

**at**：at是个可以处理仅执行一次就结束排程的指令，不过要执行at时，必须要有atd这个服务的支援才行。在某些新版的distributions中，atd可能预设并没有启动，那么at这个指令就会失效呢！不过我们的CentOS预设是启动的！

**crontab**：crontab这个指令所设定的工作将会循环的一直进行下去！可循环的时间为分钟、小时、每周、每月或每年等。crontab除了可以使用指令执行外，亦可编辑/etc/crontab来支援。至于让crontab可以生效的服务则是crond这个服务喔！

### 15.1.2 CentOS Linux 系统上常见的例行性工作 ###

	进行登录档的轮替(log rotate)
	登录档分析logwatch的任务
	建立locate的资料库
	man page查询资料库的建立
	RPM软体登录档的建立
	移除暂存档(tmpwatch)
	与网路服务有关的分析行为

## 15.2 仅执行一次的工作排程 ##

### 15.2.1 atd 的启动与at 运作的方式 ###

	systemctl start atd
	systemctl enable atd
	systemctl status atd

既然是工作排程，那么应该会有产生工作的方式，并且将这些工作排进行程表中啰！事实上，我们使用at这个指令来产生所要运作的工作，并将这个工作以文字档的方式写入`/var/spool/at/`目录内，该工作便能等待atd这个服务的取用与执行了。

at的使用限制：

1. 先找寻`/etc/at.allow`这个档案，写在这个档案中的使用者才能使用at ，没有在这个档案中的使用者则不能使用at (即使没有写在at.deny当中)；
2. 如果`/etc/at.allow`不存在，就寻找`/etc/at.deny`这个档案，若写在这个at.deny的使用者则不能使用at ，而没有在这个at.deny档案中的使用者，就可以使用at咯；
3. 如果两个档案都不存在，那么只有root 可以使用at 这个指令。

### 15.2.2 实际运作单一工作排程 ###

	at [-mldv] TIME 
	at -c工作号码
	选项与参数：
		-m ：当at 的工作完成后，即使没有输出讯息，亦以email 通知使用者该工作已完成。
		-l ：at -l 相当于atq，列出目前系统上面的所有该使用者的at 排程；
		-d ：at -d 相当于atrm ，可以取消一个在at 排程中的工作；
		-v ：可以使用较明显的时间格式列出at 排程中的工作列表；
		-c ：可以列出后面接的该项工作的实际指令内容。
		
		TIME：时间格式，这里可以定义出『什么时候要进行at 这项工作』的时间，格式有：
		  HH:MM ex> 04:00
			在今日的HH:MM 时刻进行，若该时刻已超过，则明天的HH:MM 进行此工作。
		  HH:MM YYYY-MM-DD ex> 04:00 2015-07-30
			强制规定在某年某月的某一天的特殊时刻进行该工作！
		  HH:MM[am|pm] [Month] [Date] ex> 04pm July 30
			也是一样，强制在某年某月某日的某时刻进行！
		  HH:MM[am|pm] + number [minutes|hours|days|weeks]
			ex> now + 5 minutes ex> 04pm + 3 days
			就是说，在某个时间点『再加几个时间后』才进行。

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/at-example.jpg)

建议你最好使用绝对路径来下达你的指令，比较不会有问题喔！由于指令的下达与PATH变数有关，同时与当时的工作目录也有关连(如果有牵涉到档案的话)，因此使用绝对路径来下达指令，会是比较一劳永逸的方法。at在运作时，会跑到当时下达at指令的那个工作目录。

**at的执行与终端机环境无关，所有**`standard output/standard error output`**都会传送到执行者的mailbox去啦！**所以在终端机当然看不到任何资讯。可以透过终端机装置来处理。 `echo "Hello" > /dev/tty1`

要注意的是，**如果在at shell 内的指令并没有任何的讯息输出，那么at 预设不会发email 给执行者的**。如果你想要让at 无论如何都发一封email 告知你是否执行了指令，那么可以使用『 **at -m 时间格式**』来下达指令喔！at 就会传送一个讯息给执行者，而不论该指令执行有无讯息输出了！

由于at工作排程的使用上，系统会将该项at工作独立出你的bash环境中，直接交给系统的atd程式来接管，因此，当你下达了at的工作之后就可以立刻离线了，剩下的工作就完全交给Linux管理即可！所以啰，如果有长时间的网路工作时，嘿嘿！使用at可以让你免除网路断线后的困扰喔！

**at工作管理**

	atq     # 查询主机上的工作排程
	atrm (jobnumber)     # 移除某一项排程

如果你是在一个非常忙碌的系统下运作at ，能不能指定你的工作在系统较闲的时候才进行呢？可以的，那就使用`batch`指令吧！

**batch**：系统有空时才进行背景任务

其实batch是利用at来进行指令的下达啦！只是加入一些控制参数而已。这个batch神奇的地方在于：他会在CPU的工作负载小于0.8的时候，才进行你所下达的工作任务啦！那什么是工作负载0.8呢？这个工作负载的意思是： CPU在单一时间点所负责的工作数量。不是CPU的使用率喔！举例来说，如果我有一只程式他需要一直使用CPU的运算功能，那么此时CPU的使用率可能到达100% ，但是CPU的工作负载则是趋近于『 1 』，因为CPU仅负责一个工作嘛！如果同时执行这样的程式两支呢？CPU的使用率还是100% ，但是工作负载则变成2了！

在CentOS 7 底下的batch 已经不再支援时间参数了，因此batch 可以拿来作为判断是否要立刻执行背景程式的依据！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/batch.jpg)

使用uptime 可以观察到`1, 5, 15` 分钟的『平均工作负载』量，因为是平均值，所以当我们如上表删除掉四个工作后，工作负载不会立即降低， 需要一小段时间让这个1分钟平均值慢慢回复到接近0 啊！当小于0.8 之后的『整分钟时间』时，atd 就会将batch 的工作执行掉了！	

## 15.3 循环执行的例行性工作排程 ##

相对于at是仅执行一次的工作，循环执行的例行性工作排程则是由`cron (crond)`这个系统服务来控制的。使用者自己也可以进行例行性工作排程，Linux提供使用者控制例行性工作排程的指令`(crontab)`。

### 15.3.1 使用者的设定 ###

- **/etc/cron.allow**

	将可以使用crontab的帐号写入其中，若不在这个档案内的使用者则不可使用crontab

- **/etc/cron.deny**

	将不可以使用crontab的帐号写入其中，若未记录到这个档案当中的使用者，就可以使用crontab

以优先顺序来说， /etc/cron.allow 比/etc/cron.deny 要优先

当使用者使用crontab这个指令来建立工作排程之后，该项工作就会被纪录到`/var/spool/cron/`里面去了，而且是以帐号来作为判别的喔！举例来说， dmtsai使用crontab后，他的工作会被纪录到/var/spool/cron/dmtsai里头去！但请注意，不要使用vi直接编辑该档案，因为可能由于输入语法错误，会导致无法执行cron喔！另外， cron执行的每一项工作都会被纪录到/var/log/cron这个登录档中，所以啰，如果你的Linux不知道有否被植入木马时，也可以搜寻一下/var/log/ cron这个登录档呢！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/crontab.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/contab-format-1.jpg)

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/contab-format-2.jpg)

周的数字为0 或7 时，都代表『星期天』的意思！

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/crontab-list-and-remove.jpg)

### 15.3.2 系统的设定档： /etc/crontab, /etc/cron.d/* ###

『 crontab -e 』是针对使用者的cron来设计的，如果是『系统的例行性任务』,只要编辑 `/etc/crontab`这个档案就可以啦!

基本上，cron这个服务的最低侦测限制是『**分钟**』，所以『 `cron会每分钟去读取一次/etc/crontab与/var/spool/cron里面的资料内容 `』，因此，只要你编辑完/ etc/crontab这个档案，并且将他储存之后，那么cron的设定就自动的会来执行了！

在Linux 底下的crontab 会自动的帮我们每分钟重新读取一次/etc/crontab 的例行工作事项，但是某些原因或者是其他的Unix 系统中，由于crontab 是读到记忆体当中的，所以在你修改完/etc/crontab 之后，可能并不会马上执行， 这个时候请重新启动crond 这个服务吧！『systemctl restart crond』

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/etc-crontab-structure.jpg)

- MAILTO=root

	当/etc/crontab这个档案中的例行性工作的指令发生错误时，或者是该工作的执行结果有STDOUT/STDERR时，会将错误讯息或者是萤幕显示的讯息传给谁？预设当然是由系统直接寄发一封mail给root啦！

- PATH=....

	输入执行档的搜寻路径！使用预设的路径设定就已经很足够了！

- 『分时日月周身份指令』七个栏位的设定

	多了一个身份。这与使用者的crontab -e不相同。由于使用者自己的crontab并不需要指定身份，但/etc/crontab里面当然要指定身份啦！

crond 服务读取设定档的位置

	/etc/crontab
	/etc/cron.d/*
		0hourly
			01 * * * * root run-parts /etc/cron.hourly
			每个整点的一分会去执行『 run-parts /etc/cron.hourly 』这个指令
			/etc/bin/run-parts，run parts是一个shell script
			run-parts脚本会在大约5分钟内随机选一个时间来执行/etc/cron.hourly目录内的所有执行档！因此，放在/etc/cron.hourly/的档案，必须是能被直接执行的指令脚本，而不是分、时、日、月、周的设定值喔！
			也就是说，除了自己指定分、时、日、月、周加上指令路径的crond 设定档之外，你也可以直接将指令放置到(或连结到)/etc/cron.hourly/ 目录下，则该指令就会被crond 在每小时的1 分开始后的5 分钟内，随机取一个时间点来执行啰！你无须手动去指定分、时、日、月、周就是了。
		raid-check
		sysstat
	/var/spool/cron/*

	/etc/cron.hourly
		0anacron
			/usr/sbin/anacron -s
			anacron会读取/etc/anacrontab配置文件和/var/spool/anacron时间记录文件
	/etc/cron.daily
	/etc/cron.monthly
	/etc/weekly

如果你想要自己开发新的软体，该软体要拥有自己的crontab 定时指令时，就可以将『分、时、日、月、周、身份、指令』的设定档放置到`/etc/cron.d /` 目录下！在此目录下的档案是『crontab 的**设定档脚本**』。

### 15.3.3 一些注意事项 ###

周与日月不可同时并存：『你可以分别以周或者是日月为单位作为循环，但你不可使用「几月几号且为星期几」的模式工作』

## 15.4 可唤醒停机期间的工作任务 ##

### 15.4.1 什么是anacron ###

anacron 并不是用来取代crontab 的，anacron 存在的目的就在于处理非24 小时一直启动的Linux 系统的crontab 的执行！以及因为某些原因导致的超过时间而没有被执行的排程工作。

其实anacron 也是每个小时被crond 执行一次，然后anacron 再去检测相关的排程任务有没有被执行，如果有超过期限的工作在， 就执行该排程任务，执行完毕或无须执行任何排程时，anacron 就停止了。

由于anacron 预设会以一天、七天、一个月为期去侦测系统未进行的crontab 任务，因此对于某些特殊的使用环境非常有帮助。

那么anacron 又是怎么知道我们的系统啥时关机的呢？这就得要使用anacron 读取的时间记录档（`/var/spool/anacron/*`(timestamps)） 了！anacron 会去分析现在的时间与时间记录档所记载的上次执行anacron 的时间，两者比较后若发现有差异， 那就是在某些时刻没有进行crontab 啰！此时anacron 就会开始执行未进行的crontab 任务了！

### 15.4.2 anacron 与/etc/anacrontab ###

anacron 其实是一支**程式**并非一个服务！这支程式在CentOS 当中已经进入crontab 的排程喔！同时anacron 会**每个小时**被主动执行一次喔！

	anacron [-sfn] [job]..
	anacron -u [job]..
	选项与参数：
		-s ：开始一连续的执行各项工作(job)，会依据时间记录档的资料判断是否进行；
		-f ：强制进行，而不去判断时间记录档的时间戳记；
		-n ：立刻进行未进行的任务，而不延迟(delay) 等待时间；
		-u ：仅更新时间记录档的时间戳记，不进行任何工作。
		job ：由/etc/anacrontab 定义的各项工作名称。

在我们的CentOS 中，anacron 的进行其实是在每个小时都会被抓出来执行一次， 但是为了担心anacron 误判时间参数，因此/etc/cron.hourly/ 里面的anacron 才会在档名之前加个0 (0anacron)，让anacron 最先进行！就是为了让时间戳记先更新！以避免anacron 误判crontab 尚未进行任何工作的意思。

	anacron的设定档为：/etc/anacrontab
	anacron读取的时间记录档为：/var/spool/anacron/*

![](https://cos-1257663582.cos.ap-chengdu.myqcloud.com/Notes/Linux/15/anacrontab.jpg)

我们拿/etc/cron.daily/ 那一行的设定来说明好了。那四个栏位的意义分别是：

- 天数：anacron 执行当下与时间戳记(/var/spool/anacron/ 内的时间纪录档) 相差的天数，若超过此天数，就准备开始执行，若没有超过此天数，则不予执行后续的指令。
- 延迟时间：若确定超过天数导致要执行排程工作了，那么请延迟执行的时间，因为担心立即启动会有其他资源冲突的问题吧！
- 工作名称定义：这个没啥意义，就只是会在/var/log/cron 里头记载该项任务的名称这样！通常与后续的目录资源名称相同即可。
- 实际要进行的指令串：有没有跟0hourly 很像啊！没错！相同的作法啊！透过run-parts 来处理的

根据上面的设定档内容，我们大概知道anacron 的执行流程应该是这样的(以cron.daily 为例)：

1. 由/etc/anacrontab 分析到cron.daily 这项工作名称的天数为1 天；
2. 由/var/spool/anacron/cron.daily 取出最近一次执行anacron 的时间戳记；
3. 由上个步骤与目前的时间比较，若差异天数为1 天以上(含1 天)，就准备进行指令；
4. 若准备进行指令，根据/etc/anacrontab 的设定，将延迟5 分钟+ 3 小时(看START_HOURS_RANGE 的设定)；
5. 延迟时间过后，开始执行后续指令，亦即『 run-parts /etc/cron.daily 』这串指令；
6. 执行完毕后， anacron 程式结束。

如此一来，放置在/etc/cron.daily/内的任务就会在一天后一定会被执行的！因为anacron是每个小时被执行一次嘛！所以，现在你知道为什么隔了一阵子才将CentOS开机，开机过后约1小时左右系统会有一小段时间的忙碌！而且硬碟会跑个不停！那就是因为anacron正在执行过去/etc/cron.daily/, /etc/cron.weekly/, /etc/cron.monthly/里头的未进行的各项工作排程啦！

最后，我们来总结一下本章谈到的许多设定档与目录的关系吧！这样我们才能了解crond 与anacron 的关系：

1. crond 会主动去读取/etc/crontab, /var/spool/cron/*, /etc/cron.d/* 等**设定档**，并依据『分、时、日、月、周』的时间设定去各项工作排程；
2. 根据/etc/cron.d/0hourly 的设定，主动去/etc/cron.hourly/ 目录下，执行所有在该目录下的**执行档**；
3. 因为/etc/cron.hourly/0anacron 这个指令档的缘故，主动的每小时执行anacron ，并呼叫/etc/anacrontab 的**设定档**；
4. 根据/etc/anacrontab 的设定，依据每天、每周、每月去分析/etc/cron.daily/, /etc/cron.weekly/, /etc/cron.monthly/ 内的执行档，以进行固定周期需要执行的指令。

也就是说，如果你每个周日的需要执行的动作是放置于/etc/crontab 的话，那么该动作只要过期了就过期了，并不会被抓回来重新执行。但如果是放置在/etc/cron.weekly/ 目录下，那么该工作就会定期，几乎一定会在一周内执行一次～如果你关机超过一周，那么一开机后的数个小时内，该工作就会主动的被执行喔！



